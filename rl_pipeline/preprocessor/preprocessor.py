class Preprocessor(object):

    def __init__(self, preprocessor_param=None):
        self.preprocessor_param = preprocessor_param
        
    def update(self, X):
        '''
        this is needed for some preprocessors such as running_average
        Args:
        X: NumPy array, shape = (N, dim)

        '''        
        raise NotImplementError("")

    def get_params(self):
        '''
        returns the parameters necessary to restore the preprocessor (as a dictionary)
        '''
        raise NotImplementError("")

    def save_params(self, save_path):
        '''
        save the parameters as a dictionary
        '''
        raise NotImplementError("")
        
        
    def restore_preprocessor(self, restore_path):
        '''
        restore the preprocessor 
        '''
        raise NotImplementError("")
        
    def get_scaled_x(x, **kwargs):
        raise NotImplementError("")
        