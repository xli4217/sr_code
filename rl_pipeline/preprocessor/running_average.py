import numpy as np
from rl_pipeline.preprocessor.preprocessor import Preprocessor
import cloudpickle
import copy

default_config = {
    # dimension of obs
    'dim': 3,
    'shift': True,
    'scale': True
}

# http://www.johndcook.com/blog/standard_deviation/
# code adapted from https://github.com/modestyachts/ARS/blob/master/code/
class RunningAverage(Preprocessor):
            
    def __init__(self, preprocessor_param=None):
    
        super(RunningAverage, self).__init__(preprocessor_param)

        self.dim = preprocessor_param['dim']
        self.shift = preprocessor_param['shift']
        self.scale = preprocessor_param['scale']
        
        self._n = 0
        self._M = np.zeros(self.dim,  dtype = np.float64)
        self._S = np.zeros(self.dim,  dtype = np.float64)
        self._M2 = np.zeros(self.dim,  dtype = np.float64)

    def update_with_sample(self, x):
        x = np.asarray(x)
        if x.ndim == 1:
            assert x.shape == self._M.shape, ("x.shape = {}, self.shape = {}"
                                              .format(x.shape, self._M.shape))
            n1 = self._n
            self._n += 1
            if self._n == 1:
                self._M[...] = x
            else:
                delta = x - self._M
                deltaM2 = np.square(x) - self._M2
                self._M[...] += delta / self._n
                self._S[...] += delta * delta * n1 / self._n
                
        if x.ndim == 2:
            # assert x[0,:].shape == self._M.shape, ("x.shape = {}, self.shape = {}"
            #                                        .format(x.shape, self._M.shape))
            # n1 = self._n
            # if n1 == 0:
            #     self._M = np.mean(x, axis=0)
            # else:
            #     delta = x - self._M
            #     deltaM2 = np.square(x) - self._M2
            #     n_vec_km1 = n1 + np.arange(x.shape[0])
            #     n_vec_k = n_vec_km1 + 1
            #     self._M += np.sum(np.divide(delta, n_vec_k.reshape(-1,1)), axis=0)
            #     self._S += np.sum(np.multiply(delta * delta, (n_vec_km1 / n_vec_k).reshape(-1,1)), axis=0)
                
            # self._n += x.shape[0]
            for i in range(x.shape[0]):
                n1 = self._n
                self._n += 1
                if self._n == 1:
                    self._M[...] = x[i,:]
                else:
                    delta = x[i,:] - self._M
                    deltaM2 = np.square(x[i,:]) - self._M2
                    self._M[...] += delta / self._n
                    self._S[...] += delta * delta * n1 / self._n
       
            
                
    def update_with_other_distribution(self, other):
        n1 = self._n
        n2 = other['n']
        n = n1 + n2
        delta = self._M - other['M']
        delta2 = delta * delta
        M = (n1 * self._M + n2 * other['M']) / n
        S = self._S + other['S'] + delta2 * n1 * n2 / n
        self._n = n
        self._M = M
        self._S = S

        
    def update(self, X):
        if isinstance(X, np.ndarray):
            self.update_with_sample(X)
        elif isinstance(X, dict):
            self.update_with_other_distribution(X)
        else:
            raise ValueError("input type not supported")
            
            
    def get_params(self):
        return( {"n": self._n, "M": self._M, "S": self._S, "M2": self._M2} )

    def restore_params(self, params):
        self._n = params['n']
        self._M = params['M']
        self._S = params['S']
        self._M2 = params['M2']
      
        
    def save_params(self, save_path):
        dump = cloudpickle.dumps(self.get_params())
        with open(save_path, "wb") as f:
            f.write(dump)

    def restore_preprocessor(self, restore_path):
        with open(restore_path, 'rb') as f:
            params = cloudpickle.load(f)
            print(params)
        self.restore_params(params)
        print("restored state preprocessor from: {}".format(restore_path))
        
    def get_scaled_x(self, x):
        if self._n == 0:
            return x
            
        scaled_x = np.array(x)
        if self.shift:
            scaled_x = x - self.mean
        if self.scale:
            scaled_x = scaled_x / (self.std + 1e-8)

        return scaled_x


    @property
    def n(self):
        return self._n

    @property
    def mean(self):
        return self._M

    @property
    def var(self):
        return self._S / (self._n - 1) if self._n > 1 else np.square(self._M)

    @property
    def std(self):
        std = np.sqrt(self.var)
        # Set values for std less than 1e-7 to +inf to avoid 
        # dividing by zero. State elements with zero variance
        # are set to zero as a result. 
        std[std < 1e-7] = float("inf") 
        return std

    @property
    def shape(self):
        return self._M.shape


if __name__ == "__main__":
    config = {"dim": 17, "shift": True, "scale": True}
    preprocessor1 = RunningAverage(config)
    preprocessor2 = RunningAverage(config)

    x = np.random.rand(30, 17)

    for i in range(x.shape[0]):
        preprocessor1.update_with_sample(x[i,:])

    preprocessor2.update_with_sample(x[0,:])
    preprocessor2.update_with_sample(x[1:,:])    

    # print(preprocessor1._M - preprocessor2._M)
    # print(preprocessor1._S - preprocessor2._S)
    print(preprocessor2._M - np.mean(x, axis=0))
    print(preprocessor2.var - np.var(x, axis=0))