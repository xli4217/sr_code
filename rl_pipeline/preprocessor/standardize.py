import numpy as np
from rl_pipeline.preprocessor.preprocessor import Preprocessor
import cloudpickle

class Standardize(Preprocessor):
    """
    Standardize input data
    x_standardized = (x - mean)/std
    """

    def __init__(self, dim, preprocessor_param):
        """
        Args:
            obs_dim: dimension of axis=1
        """

        super(Standardize, self).__init__(dim, preprocessor_param)
        
        
    def update(self, X):
        pass
        
    def get_params(self):
        pass

    def save_params(self, save_path):
        pass
        
    def restore_preprocessor(self, restore_path):
        pass
        
    def get_scaled_x(self, x):
        if x.size > 1:
            return (x - np.mean(x)) / np.std(x)
        else:
            return x
      