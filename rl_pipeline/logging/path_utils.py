import os
import shutil
import numpy as np

RLFPS_PATH = os.path.dirname(os.path.realpath(__file__+"/../../"))

def remove_test_dirs(example_path):
    '''
    remove all log and/or data related to test

    example_path: folder where the example is located relative to the root RLFPS path
    '''

    data_path = RLFPS_PATH + "/rllab/data/local/experiment"

    if os.path.isdir(data_path + "/test"):
        shutil.rmtree(data_path+"/test")

    
    abs_example_path = RLFPS_PATH + example_path
    
    # -- remove current test log path and create new one
    tensorboard_log_path = abs_example_path + "/log"

    if os.path.isdir(tensorboard_log_path + "/test"):
        shutil.rmtree(tensorboard_log_path + "/test")

    os.mkdir(tensorboard_log_path + "/test")

    # files_to_rm = [f for f in os.listdir(example_path + '/log/test')]
    # for f in files_to_rm:
    #     os.remove(os.path.join(example_path, 'log/test', f))


    # -- remove current rllab test data path and create new one
    if os.path.isdir(abs_example_path + "/test"):
        shutil.rmtree(abs_example_path+"/test")

    os.mkdir(abs_example_path + "/test")


def make_hyperparam_str(hyperparam_names, hyperparam_values):
    '''
    example inputs:
    hyperparam_names=['learning_rate', 'momentum'], hyperparam_values=[0.1, 0.9]

    example output:
    output = "learning_rate_0.1_momentum_0.9"
    '''
    hyperparam_str = ""
    first = True
    for name, value in zip(hyperparam_names, hyperparam_values):
        if first:
            hyperparam_str += name + "_" + str(value)
            first = False
        else:
            hyperparam_str += "_"  + name + "_" + str(value)

    return hyperparam_str


