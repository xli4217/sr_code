from rl_pipeline.logging.logger_base import LoggerBase
from tensorboardX import SummaryWriter
import torch
from future.utils import viewitems
import os
import signal

class TensorboardXLogger(LoggerBase):

    def __init__(self,
                 csv_data_dir,
                 log_dir,
                 model_dir,
                 transitions_dir,
                 config_dir,
                 info_dir,
                 logging_params):

        super(TensorboardXLogger, self).__init__(csv_data_dir,
                                                 log_dir,
                                                 model_dir,
                                                 transitions_dir,
                                                 config_dir,
                                                 info_dir,
                                                 logging_params)

        self.writers = {}
        self.log_dir = log_dir
        self.name = 'TensorboardXLogger'

    def tfx_add_writer(self, writer_name):
        self.writers[writer_name] = SummaryWriter(log_dir=os.path.join(self.log_dir, writer_name))
        
    def tfx_writer_add_graph(self, writer_name,  model, input_dim):
        dummy_input = torch.randn(input_dim)
        self.writers[writer_name].add_graph(model, dummy_input)

    def tfx_writer_flush(self, writer_name):
        self.writers[writer_name].flush()
        
    def tfx_close(self):
        for k, v in viewitems(self.writers):
            v.close()
        
    def tfx_writer_add_summary(self, writer_name, summary_dict, step, record_distributions=False):
        """
        Args:
          summary_dict: takes the form
        {
            "scalar": {
                'training_loss': value,
                'validation_loss': value,
                # metrics in a dict are plotted in the same graph
                'loss/group':{
                    'training_loss': value,
                    'validation_loss': value
                }
            },
            "histogram": {
                "name1": param1,
                "name2": param2
            }
            "text": {
                 "tag": str
             },
            "hparam": {
                 "param_dict": {},
                 "metrci_dict": {}
             }
        }  

        """
   

        for summary_type, summary in viewitems(summary_dict):
            if summary_type == "scalar":
                # write scaler Summary
                for key, value in viewitems(summary):
                    if isinstance(value, dict):
                        group_dict = {}
                        for k, v in viewitems(value):
                            group_dict[k] = v

                        self.writers[writer_name].add_scalars(key, group_dict, step)
                    else:
                        self.writers[writer_name].add_scalar(key, value, step)

            if record_distributions:
                if summary_type == "histogram":
                    for k, v in viewitems(summary):
                        self.writers[writer_name].add_histogram(k, v, step)


            if summary_type == 'text':
                for key, value in viewitems(summary):
                    if not isinstance(value, str):
                        value = str(value)
                    self.writers[writer_name].add_text(key, value, step)

            # if summary_type == 'hparam':
                # self.writers[writer_name].add_hparams(summary['hparam_dict'], summary['metric_dict'])