import rl_pipeline.logging.logger as logger
import os
import cloudpickle
from future.utils import viewitems
import logging

class LoggerBase(object):
    def __init__(self,
                 csv_data_dir,
                 log_dir,
                 model_dir,
                 transitions_dir,
                 config_dir,
                 info_dir,
                 logging_params):
        
        self.csv_data_dir = csv_data_dir
        self.log_dir = log_dir
        self.model_dir = model_dir
        self.transitions_dir = transitions_dir
        self.config_dir = config_dir
        self.info_dir = info_dir
        
        self.logging_params = logging_params

        self.logger = logger
        self.logger.add_tabular_output(os.path.join(csv_data_dir, "data.csv"))
        
        # text logger
        self.txt_logger = logging.getLogger('')
        hdlr = logging.FileHandler(os.path.join(self.log_dir, 'log.txt'))
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        hdlr.setFormatter(formatter)
        self.txt_logger.addHandler(hdlr) 
        self.txt_logger.setLevel(logging.INFO)

    def log_text(self, txt, level='info'):
        if level == 'info':
            self.txt_logger.info(txt)
        elif level == 'debug':
            self.txt_logger.debug(txt)
        elif level == 'warning':
            self.txt_logger.warning(txt)
        elif level == 'error':
            self.txt_logger.error(txt)
        else:
            raise ValueError('level not supported')
        
    def csv_record_tabular(self, name, value):
        self.logger.record_tabular(name, value)

    def csv_record_tabular_dict(self, log_dict):
        for key, value in viewitems(log_dict):
            self.logger.record_tabular(key, value)
        
    def csv_dump_tabular(self):
        self.logger.dump_tabular(with_prefix=False)

    def save_content(self, content, path, itr=None, interval=None, file_type="csv"):
        '''
        this saves content as pkl or csv to path 
        if "interval" is provided, saves at each "interval"
        '''
        if not os.path.exists(os.path.dirname(path)):
            os.makedirs(os.path.dirname(path))
 
        if file_type == "pkl":
            if not interval:
                res = cloudpickle.dumps(content)
                with open(os.path.join(path),'wb') as f:
                    f.write(res)
            else:
                if itr % interval == 0:
                    res = cloudpickle.dumps(content)
                    with open(os.path.join(path),'wb') as f:
                        f.write(res)
                    
        if file_type == "csv":
            import pandas as pd
            if not interval:
                content.to_csv(path)
            else:
                if itr % interval == 0:
                    content.to_csv(path)
        
    def save_model(self, model, model_name, itr=None, interval=None, directory=None, **kwargs):
        if isinstance(model, dict):
            for model_n, model in viewitems(model):
                # name = model_name + "_" + model_n
                if interval:
                    if itr % interval == 0:
                        if not directory:
                            model.save(self.model_dir, model_n)
                        else:
                            model.save(directory, model_n)
                else:
                    if not directory:
                        model.save(self.model_dir, model_n)
                    else:
                        model.save(directory, model_n)

        else:
            if interval:
                if itr % interval == 0:
                    if not directory:
                        model.save(self.model_dir, model_n)
                    else:
                        model.save(directory, model_n)

            else:
                if not directory:
                    model.save(self.model_dir, model_n)
                else:
                    model.save(directory, model_n)
                    
        
    def save_transitions(self, batch=None, model=None, filename=None, itr=None, interval=None, **kwargs):
        '''
        Saves batch (a list of dicts with keywords "Observations", "Actions", "Rewards") and the model (policy or value) that
        generated the batch to file with name "filename" every "interval" iterations
        '''
        if interval:
            if itr % interval == 0:
                # create the transitions folder
                transitions_dir = os.path.join(self.transitions_dir, filename)
                if not os.path.exists(transitions_dir):
                    os.makedirs(transitions_dir)

                if batch is not None:
                    self.save_content(batch, os.path.join(transitions_dir, "batch.pkl"), itr, file_type="pkl") # save batch
                if model is not None:
                    if isinstance(model, dict):
                        model_name = "model_dict"
                    else:
                        model_name = model.scope
                
                    self.save_model(model, model_name=model_name, directory=transitions_dir)
        else:
            # create the transitions folder
            transitions_dir = os.path.join(self.transitions_dir, filename)
            if not os.path.exists(transitions_dir):
                os.makedirs(transitions_dir)

            if batch is not None:
                self.save_content(batch, os.path.join(transitions_dir, "batch.pkl"), itr, file_type="pkl") # save batch
            if model is not None:
                self.save_model(model, model_name=model.config['scope'], directory=transitions_dir)
            
    def save_config(self, config):
        '''
        this saves the configuration of the current experiment as a pkl file
        '''
        res = cloudpickle.dumps(config)
        with open(os.path.join(self.config_dir,"config.pkl"),'wb') as f:
            f.write(res)


    def close(self):
        self.logger.remove_tabular_output(os.path.join(self.csv_data_dir, "data.csv"))