import numpy as np
from rl_pipeline.configuration.configuration import Configuration
import time
import ray

default_config = {
    'seed': 0,
    'update_parameters': True,
    'nb_remote_runners': 0,
    'ray': False,
    "max_itr": 10,
    'evaluation_logging': False,
    'evaluation_batch_size': 0,
    'evaluation_logging_interval': 0,
    'BaseRunner': {
        'type': None,
        'config': {}
    },
    'Evaluator': {
        'type': None,
        'config': {}
    },
}


class SynchronousRunner(object):
    '''
    class for synchronous updates, i.e. data collection and agent update happen sequentially (often the case in actor-critic methods)
    '''
    def __init__(self, config={}):
        self.SynchronousRunner_config = Configuration(default_config)
        self.SynchronousRunner_config.update(config)
        
        self.update_parameters = self.SynchronousRunner_config.get('update_parameters')
        base_runner_type = self.SynchronousRunner_config.get(['BaseRunner', 'type'])
        base_runner_config = self.SynchronousRunner_config.get(['BaseRunner', 'config'])
        if self.SynchronousRunner_config.get('nb_remote_runners') == 0:
            self.local_runner = base_runner_type(config=base_runner_config, role="111", seed=self.SynchronousRunner_config.get('seed'))
        if self.SynchronousRunner_config.get('nb_remote_runners') > 0 and self.SynchronousRunner_config.get('ray'):
            ray.init()
            # define remote runners
            RemoteRunner = ray.remote(base_runner_type)
            if self.SynchronousRunner_config.get('ports') is None: 
                self.remote_runners = [RemoteRunner.remote(config=base_runner_config, use_ray=True, role="100", seed=base_runner_config['seed']+7*i, suffix=str(i)) for i in range(self.SynchronousRunner_config.get('nb_remote_runners'))]       
            else:
                self.remote_runners = [RemoteRunner.remote(config=base_runner_config, seed=base_runner_config['seed']+7*i, use_ray=True, role="100", port_num=self.SynchronousRunner_config.get('ports')[i], suffix=str(i)) for i in range(self.SynchronousRunner_config.get('nb_remote_runners'))]        

            time.sleep(5)
                
            # define local runners
            self.local_runner = base_runner_type(base_runner_config, use_ray=True, role="011", port_num=self.SynchronousRunner_config.get('ports')[-1])
                
        self.logger = self.local_runner.logger
        self.evaluation = self.SynchronousRunner_config.get(['Evaluator', 'type'])(self.SynchronousRunner_config.get(['Evaluator', 'config']))
        # TODO: this is a hack, fix
        self.null_evaluation_log_dict = None
        
    def run(self):
        for itr in range(self.SynchronousRunner_config.get('max_itr')):
            if self.SynchronousRunner_config.get('nb_remote_runners') == 0:
                total_unscaled_batch, total_scaled_batch, other = self.local_runner.get_batch_rollout(deterministic=False)
                if self.update_parameters:
                    self.local_runner.agent.update(total_unscaled_batch, total_scaled_batch, other, itr)
                
            if self.SynchronousRunner_config.get('nb_remote_runners') > 0 and self.SynchronousRunner_config.get('ray'):
                # synchrononize remote runners with local runner
                print("synchronizing remote with local ...")
                local_runner_weights = self.local_runner.get_trainable_weights()
                weights_id = ray.put(local_runner_weights)
                set_remote_weights = [remote_runner.set_trainable_weights.remote(weights_id) for remote_runner in self.remote_runners]
                ray.wait(set_remote_weights, self.SynchronousRunner_config.get('nb_remote_runners'))
                print("synchronizing complete")

                # batch_list is [(unscaled_batch1, scaled_batch1, other), ...]
                get_batch_list = [runner.get_batch_rollout.remote() for runner in self.remote_runners]
                
                # this is to make sure all remote runners terminate before moving on
                ready_batch_list_id, remaining_batch_list_id = ray.wait(get_batch_list, self.SynchronousRunner_config.get('nb_remote_runners'))
                batch_list = ray.get(ready_batch_list_id)
                total_unscaled_batch, total_scaled_batch, total_other = [], [], []
                for batches in batch_list:
                    total_unscaled_batch.append(batches[0])
                    total_scaled_batch.append(batches[1])
                    total_other.append(batches[2])
                total_unscaled_batch = sum(total_unscaled_batch, [])
                total_scaled_batch = sum(total_scaled_batch, [])
                total_other = sum(total_other, [])
                self.local_runner.agent.update(total_unscaled_batch, total_scaled_batch, total_other, itr)

                # update local preprocessors
                print("updating local preprocessors")
                state_preprocessor_params, reward_preprocessor_params = self.local_runner.update_preprocessors(total_unscaled_batch)

                if (state_preprocessor_params is not None or reward_preprocessor_params is not None) and self.update_parameters:
                    # synchronize remote preprocessors
                    print("synchronizing remote preprocessors")
                    remote_preprocessor_sync = [remote_runner.restore_preprocessors.remote(state_preprocessor_params, reward_preprocessor_params) for remote_runner in self.remote_runners]
                    ray.wait(remote_preprocessor_sync, self.SynchronousRunner_config.get('nb_remote_runners'))


            # Log training returns
            training_log_dict = self.evaluation.get_diagnostics(mode="training", batch=total_unscaled_batch)
            self.logger.csv_record_tabular_dict(training_log_dict)    
                
            # get evaluation batch and log performances
            if self.SynchronousRunner_config.get('evaluation_logging'):
                if itr % self.SynchronousRunner_config.get('evaluation_logging_interval') == 0:
                    unscaled_batch, _, _ = self.local_runner.get_batch_rollout(batch_size=self.SynchronousRunner_config.get('evaluation_batch_size'), deterministic=True)
                    evaluation_log_dict = self.evaluation.get_diagnostics(mode="evaluation", batch=unscaled_batch)
                    if not self.null_evaluation_log_dict:
                        self.null_evaluation_log_dict = {x: None for x in evaluation_log_dict}
                else:
                    evaluation_log_dict = self.null_evaluation_log_dict
                    
                self.logger.csv_record_tabular_dict(evaluation_log_dict)    
                
            self.logger.csv_record_tabular("iteration", itr)
            self.logger.csv_dump_tabular()
        
    def close(self):
        self.local_runner.close()
        if self.SynchronousRunner_config.get('nb_remote_runners') > 0:
            [remote_runner.close.remote() for remote_runner in self.remote_runners]