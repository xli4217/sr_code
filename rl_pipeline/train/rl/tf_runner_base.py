import numpy as np
import tensorflow as tf

class TFRunnerBase(object):
    """
    Base runner with hyperparameter tuning capabilities
    """

    def __init__(self, config, remote=False, seed=0, **kwargs):
        self.config = config
        self.remote=remote # whether the current instance is a remote runner or not (used mainly for logging, only local runner logs)

        if not seed:
            np.random.seed(self.config['seed'])
            tf.set_random_seed(self.config['seed'])
        else:
            np.random.seed(seed)
            tf.set_random_seed(seed)

        self.seed = self.config['seed']
        self.remote = remote
        self.configuration = config['configuration']
        self.create_agent()
        if self.agent:
            if self.agent.logger:
                self.agent.logger.save_config(config)

        self.var = None
        
        
    def create_agent(self):
        '''
        Instantiate the necessary components for a training run, this include but not limited to

        logger, environment, policy, baseline, preprocessor, sampler, agent, etc
        '''
        self.agent = None
        self.sampler = None
        self.state_preprocessor = None
        self.reward_preprocessor = None

    
    def get_trainable_weights(self):
        if self.var:
            return self.var.get_weights()
        else:
            raise ValueError("not in ray mode")

        
    def set_trainable_weights(self, weights):
        if self.var:
            self.var.set_weights(weights)
        else:
            raise ValueError("not in ray mode")

    def get_batch_rollout(self, batch_size=None, deterministic=False):
        if self.sampler:
            unscaled_batch, scaled_batch = self.sampler.get_batch(batch_size, deterministic)
            return unscaled_batch, scaled_batch, []
        else:
            raise ValueError("no sampler")
            
        
    def update_preprocessors(self, unscaled_batch):
        for traj in unscaled_batch:
            if self.state_preprocessor:
                self.state_preprocessor.update(traj['Observations'])
        
            if self.reward_preprocessor:
                self.reward_preprocessor.update(traj['Rewards'])

        state_preprocessor_params = None
        reward_preprocessor_params = None
        if self.state_preprocessor:
            state_preprocessor_params = self.state_preprocessor.get_params()
        if self.reward_preprocessor:
            reward_preprocessor_params = self.reward_preprocessor.get_params()
        return state_preprocessor_params, reward_preprocessor_params


    def restore_preprocessors(self, state_preprocessor_params=None, reward_preprocessor_params=None):
        if self.state_preprocessor:
            self.state_preprocessor.restore_params(state_preprocessor_params)
        if self.reward_preprocessor:
            self.reward_preprocessor.restore_params(reward_preprocessor_params)

    def close(self):
        raise NotImplementError("")


