from rl_pipeline.configuration.configuration import Configuration
from rl_pipeline.configuration.experiment_dirs import ExperimentDirs

import numpy
import os
import fire
from future.utils import viewitems

import time
import copy
import cloudpickle


default_config = {
    # this is used to provide directories for the logger
    'ExpDir': None,
    'seed': 0,
    'env_constructed': False,
    'cmd_log': "",
    'base_config': {},
    # continue learning from this config {'exp_path': None, 'hyperparam_exp_name': None, 'itr': None},
    'restore_runner_dir': None,
    'Logger': {
        'type': None,
        'config': {}
    },
    'Reset':{
        'type': None,
        'config':{}
    },
    'Environment': {
        'type': None,
        'config': {
            'constructed': False
        }
    },
    'Preprocessors': {
        "state_preprocessor": {"type": None, 'config':{}},
        "reward_preprocessor": {"type": None, 'config':{}}
    },
    'ReplayBuffer': {
        'type': None,
        'config': {}
    },
    'Exploration':{
        'type': None,
        'config':{}
    },
    'Actor':{
        'type': None,
        'config':{}
    },
    'Critic':{
        'type': None,
        'config':{}
    },
    'Agent':{
        'type': None,
        'config':{}
    },
    'Sampler':{
        'type': None,
        'config':{}
    }
}



class RunnerBase(object):

    def __init__(self, config={}, use_ray=False, remote=False, seed=None, port_num=None):
        self.RunnerBase_config = Configuration(default_config)
        self.RunnerBase_config.update(config)

        self.remote = remote
        self.seed = seed
        if self.seed is None:
            self.seed =self.RunnerBase_config.get('seed')

        self.ray = use_ray
        self.port_num = port_num

    def create_logger(self):
        if self.remote and self.ray:
            self.logger = None
        else:
            exp_dir = self.RunnerBase_config.get('ExpDir')
            self.logger = self.RunnerBase_config.get(['Logger', 'type'])(csv_data_dir=exp_dir.csv_data_hyperparam_exp_dir,
                                                                        log_dir=exp_dir.log_hyperparam_exp_dir,
                                                                        model_dir=exp_dir.model_hyperparam_exp_dir,
                                                                        config_dir=exp_dir.config_hyperparam_exp_dir,
                                                                        info_dir=exp_dir.info_hyperparam_exp_dir,
                                                                        transitions_dir=exp_dir.transitions_hyperparam_exp_dir,
                                                                        logging_params=self.RunnerBase_config.get(['Logger', 'config']))
            
            self.logger.save_config(self.RunnerBase_config)
            self.logger.log_text("cmd log: {}".format(self.RunnerBase_config.get('cmd_log')))

            
    def create_reset(self):
        self.reset = None
        if self.RunnerBase_config.get(['Reset', 'type']) is not None:
            self.reset = self.RunnerBase_config.get(['Reset', 'type'])(self.RunnerBase_config.get(['Reset', 'config']))


    def create_env(self):
        # env_config = self.RunnerBase_config.get(['Environment', 'config'])    
        # if not self.RunnerBase_config.get('env_constructed'):
        #     self.env = self.RunnerBase_config.get(['Environment', 'type'])(env_config, seed=self.seed, reset=self.reset, logger=self.logger)
        # else:
        #     self.env = self.RunnerBase_config.get(['Environment', 'type'])
        # self.obs_dim = self.env.state_space['shape'][0]
        # self.action_dim = self.env.action_space['shape'][0]

        env_config = self.RunnerBase_config.get(['Environment', 'config'])    
        if not self.RunnerBase_config.get('env_constructed'):
            self.env = self.RunnerBase_config.get(['Environment', 'type'])(env_config, seed=self.seed, port_num=self.port_num, suffix=self.suffix, reset=self.reset, logger=self.logger)
        else:
            self.env = self.RunnerBase_config.get(['Environment', 'type'])
        self.obs_dim = self.env.state_space['shape'][0]
        self.action_dim = self.env.action_space['shape'][0]
        
    def create_preprocessors(self):
        self.state_preprocessor = None
        self.reward_preprocessor = None
        if self.RunnerBase_config.get(['Preprocessors', 'state_preprocessor', 'type']) is not None:
            config = self.RunnerBase_config.get(['Preprocessors', 'state_preprocessor', 'config'])
            config['dim'] = self.obs_dim
            self.state_preprocessor = self.RunnerBase_config.get(['Preprocessors', 'state_preprocessor', 'type'])(config)
        if self.RunnerBase_config.get(['Preprocessors', 'reward_preprocessor', 'type']) is not None:
            self.reward_preprocessor = self.RunnerBase_config.get(['Preprocessors', 'reward_preprocessor', 'type'])(self.RunnerBase_config.get(['Preprocessors', 'reward_preprocessor', 'config']))

    def create_replay_buffer(self):
        self.replay_buffer = None
        if self.RunnerBase_config.get(['ReplayBuffer', 'type']) is not None and ( (self.ray and not self.remote) or not self.ray):
            self.replay_buffer = self.RunnerBase_config.get(['ReplayBuffer', 'type'])( self.RunnerBase_config.get(['ReplayBuffer', 'config']))
       
       

    def create_exploration(self):
        self.exploration = None
        if self.RunnerBase_config.get(['Exploration', 'type']) is not None:
            explore_config = self.RunnerBase_config.get(['Exploration', 'config'])
            self.exploration = self.RunnerBase_config.get(['Exploration', 'type'])(exploration_params=explore_config, action_space=self.env.action_space, seed=self.seed)
            
    def create_critic(self):
        self.critic = None
        if self.RunnerBase_config.get(['Critic', 'type']) is not None:
            critic_config = self.RunnerBase_config.get(['Critic', 'config'])
            critic_config['obs_dim'] = self.obs_dim
            critic_config['action_dim'] = self.action_dim
            self.critic = self.RunnerBase_config.get(['Critic', 'type'])(config=critic_config)

    def create_actor(self):
        self.policy = None
        if self.RunnerBase_config.get(['Actor', 'type']) is not None:
            policy_config = self.RunnerBase_config.get(['Actor', 'config'])
            policy_config['obs_dim'] = self.obs_dim
            policy_config['action_dim'] = self.action_dim
            self.policy = self.RunnerBase_config.get(['Actor', 'type'])(config=policy_config,
                                                                        exploration_strategy=self.exploration)

    def create_algo_agent(self):
        if not self.remote or not self.ray:
            self.agent = self.RunnerBase_config.get(['Agent', 'type'])(config=self.RunnerBase_config.get(['Agent', 'config']),
                                                                       policy=self.policy,
                                                                       value=self.value,
                                                                       replay_buffer=self.replay_buffer,
                                                                       logger=self.logger)

    def create_sampler(self):
        if self.remote or not self.ray:
            sampler_config = self.RunnerBase_config.get(['Sampler', 'config'])
            if self.remote:
                sampler_config['update_preprocessors'] = False
                sampler_config['save_preprocessors'] = False
      

            self.sampler = self.RunnerBase_config.get(['Sampler', 'type'])(config=sampler_config,
                                                                          env=self.env,
                                                                          policy=self.policy,
                                                                          state_preprocessor=self.state_preprocessor,
                                                                          reward_preprocessor=self.reward_preprocessor,
                                                                          logger=self.logger)



    def restore_from_checkpoint(self):
        actor_critic_restore_dir = os.path.join(restore_config['exp_path'], 'transitions', restore_config['hyperparam_exp_name'], 'itr_'+str(restore_config['itr']))
        replay_buffer_restore_dir = os.path.join(restore_config['exp_path'], 'log', restore_config['hyperparam_exp_name'])
        # restore actor
        self.policy.restore(actor_critic_restore_dir)
        # restore critic
        self.Q.restore(actor_critic_restore_dir)
        # restore replay buffer
        self.replay_buffer.restore(os.path.join(replay_buffer_restore_dir, 'replay_buffer.pkl'))
      
            
    def create_agent(self):
        """
        Instantiate the necessary components for a training run, this include but not limited to

        logger, environment, policy, baseline, preprocessor, sampler, agent, etc

        """
        #### build infrastructure ####
        self.create_logger()
        self.create_reset()
        self.create_env()
        self.create_preprocessors()
        self.create_replay_buffer()
        self.create_exploration()
        
        #### build computation graph ####
        self.create_critic()
        self.create_actor()
        self.create_algo_agent()

        #### create sampler ####
        self.create_sampler()

        #### restore ####
        restore_config = self.RunnerBase_config.get('restore_runner_dir')
        if restore_config is not None:
            self.restore_from_checkpoint()
        
    def get_trainable_weights(self):
        pass

    def set_trainable_weights(self, weights):
        pass
        
    def get_batch_rollout(self, batch_size=None, deterministic=False):
        if self.sampler:
            unscaled_batch, scaled_batch = self.sampler.get_batch(batch_size, deterministic)
            return unscaled_batch, scaled_batch, []
        else:
            raise ValueError("no sampler")


    def update_preprocessors(self, unscaled_batch):
        return None, None

    def restore_preprocessors(self, state_preprocessor_params=None, reward_preprocessor_params=None):
        pass

    def close(self):
        if self.logger is not None:
            self.logger.close()
        self.env.close()
