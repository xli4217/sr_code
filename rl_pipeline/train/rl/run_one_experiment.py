import os
import numpy as np
from rl_pipeline.configuration.configuration import Configuration
from rl_pipeline.configuration.experiment_dirs import ExperimentDirs
from future.utils import viewitems


default_config = {
    'nb_random_seed_exp': 1,
    'ExpDir': None,
    'exp_root_dir': None,
    'exp_name': None,
    'hyperparam_dir': None,  
    'seed': 0,
    'Runner': {
        'type': None,
        'config': {
            'BaseRunner': {
                'type': None,
                'config': {}
            },
            'Evaluator': {
                'type': None,
                'config': {}
            },
            'nb_remote_runners': 0,
            'evaluation_logging': False,
            'evaluation_batch_size': 0,
            'evaluation_logging_interval': 0,
        }
    },
    'HyperparameterTuner': {
        'type': None,
        'config': {
            'nb_steps': 10,
        }
    }
}
   


class RunOneExperiment(object):

    def __init__(self, config={}):
        self.RunOneExperiment_config = Configuration(default_config)
        self.RunOneExperiment_config.update(config)

        if self.RunOneExperiment_config.get('ExpDir') is None:
            experiment_dir = os.path.join(self.RunOneExperiment_config.get('exp_root_dir'), "experiments")
            current_exp_dir = os.path.join(experiment_dir, self.RunOneExperiment_config.get('exp_name'))
            # ---- construct logging directories ----
            self.ExpDir = ExperimentDirs(experiment_dir, current_exp_dir)
        else:
            self.ExpDir = self.RunOneExperiment_config.get('ExpDir')

        #### this is sync_runner of async_runner ####
        self.runner_cls = self.RunOneExperiment_config.get(['ExperimentRunner', 'type'])
        self.runner_config = self.RunOneExperiment_config.get(['ExperimentRunner', 'config'])
            
    def run_one_experiment(self):
        '''
        this function takes care of training and hyperparameter tuning
        '''
        if self.RunOneExperiment_config.get('HyperparameterTuner') is None:
            if self.RunOneExperiment_config.get('nb_random_seed_exp') == 1:
                self.ExpDir.create_hyperparam_exp("seed"+str(self.RunOneExperiment_config.get('seed')))
                # update runner config
                self.runner_config['BaseRunner']['config']['ExpDir'] = self.ExpDir
                self.runner = self.runner_cls(self.runner_config)
                self.runner.run()
                self.runner.close()
            else:
                seed_list = np.random.choice(range(1000), self.RunOneExperiment_config.get('nb_random_seed_exp'), replace=False)
                for i in range(self.RunOneExperiment_config.get('nb_random_seed_exp')):
                    seed = seed_list[i]
                    self.RunOneExperiment_config.config['seed'] = seed
                    self.ExpDir.create_hyperparam_exp("seed"+str(seed))
                    self.runner_config['BaseRunner']['config']['ExpDir'] = self.ExpDir
                    self.runner = self.runner_cls(self.runner_config)
                    self.runner.run()
                    self.runner.close()
                    
        elif self.RunOneExperiment_config.get('HyperparameterTuner') is not None:
            hyperparam_tuner = self.RunOneExperiment_config.get(['HyperparameterTuner', 'type'])(self.RunOneExperiment_config.get(['HyperparameterTuner', 'config']))
            for i in range(self.RunOneExperiment_config.get(['HyperparameterTuner', 'config', 'nb_steps'])):
                hyperparam_dict = hyperparam_tuner.get_next_hyperparam_dict()
                ######## update config #########
                agent_config = self.runner_config['BaseRunner']['config']['Agent']['config']
                
                actor_config = self.runner_config['BaseRunner']['config']['Actor']['config']
                critic_config = self.runner_config['BaseRunner']['config']['Critic']['config']
                replay_buffer_config = self.runner_config['BaseRunner']['config']['ReplayBuffer']['config']
                
                effective_hyperparam_dict = {}
                # update agent config
                for key, value in viewitems(agent_config):
                    # this is for 'base_config' in agent_config
                    if isinstance(value, dict):
                        for k, v in viewitems(value):
                            if k in hyperparam_dict.keys():
                                value[k] = hyperparam_dict[k]
                                effective_hyperparam_dict[k] = hyperparam_dict[k]
                    else:
                        if key in hyperparam_dict.keys():
                            agent_config[key] = hyperparam_dict[key]
                            effective_hyperparam_dict[key] = hyperparam_dict[key]
                        
                # update actor config
                for key, value in viewitems(actor_config):
                    if key in hyperparam_dict.keys():
                        actor_config[key] = hyperparam_dict[key]
                        effective_hyperparam_dict[key] = hyperparam_dict[key]

                # update actor config
                for key, value in viewitems(critic_config):
                    if key in hyperparam_dict.keys():
                        critic_config[key] = hyperparam_dict[key]
                        effective_hyperparam_dict[key] = hyperparam_dict[key]

                        
                # update replay buffer config
                for key, value in viewitems(replay_buffer_config):
                    if key in hyperparam_dict.keys():
                        replay_buffer_config[key] = hyperparam_dict[key]
                        effective_hyperparam_dict[key] = hyperparam_dict[key]
        
              
                hyperparam_str = hyperparam_tuner.make_hyperparam_string(effective_hyperparam_dict)
                self.ExpDir.create_hyperparam_exp(hyperparam_str)
           
                ################################
                self.runner_config['BaseRunner']['config']['ExpDir'] = self.ExpDir
                self.runner = self.runner_cls(self.runner_config)
                self.runner.run()
                # get a batch for evaluation
                print("obtaining evaluation batch")
                unscaled_batch, _, _ = self.runner.local_runner.get_batch_rollout(deterministic=True)
                avg_return = []
                for traj in unscaled_batch:
                    avg_return.append(np.sum(traj['Rewards']))
                print(hyperparam_dict)
                print("performance metric: %f" %np.mean(np.array(avg_return)))
                
                info_dir = os.path.abspath(os.path.join(self.runner.local_runner.agent.logger.info_dir, os.path.pardir))
                hyperparam_tuner.save_hyperparam_performance(new_metric=np.mean(np.array(avg_return)), save_path=os.path.join(info_dir, "hyperparam_log.csv"))
                self.runner.close()

        

if __name__ == "__main__":
    pass
    # from config import experiment_config
    # cls = RunOneExperiment(experiment_config)