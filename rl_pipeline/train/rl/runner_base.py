from rl_pipeline.configuration.configuration import Configuration
from rl_pipeline.configuration.experiment_dirs import ExperimentDirs

import numpy as np
import os
import fire
from future.utils import viewitems

import time
import copy
import cloudpickle


default_config = {
    # this is used to provide directories for the logger
    'ExpDir': None,
    'seed': 0,
    'cmd_log': "",
    'state_space': None,
    'action_space': None,
    'create': {
        'logger': True,
        'reset': True,
        'environment': True,
        'preprocessors': True,
        'replay_buffer': True,
        'exploration': True,
        'actor': True,
        'critic': True,
        'algo_agent': True,
        'sampler': True
    },
    'Logger': {
        'type': None,
        'config': {}
    },
    'Reset':{
        'type': None,
        'config':{}
    },
    'Environment': {
        'type': None,
        'config': {
            'constructed': False
        }
    },
    'Preprocessors': {
        "state_preprocessor": {"type": None, 'config':{}},
        "reward_preprocessor": {"type": None, 'config':{}}
    },
    'ReplayBuffer': {
        'type': None,
        'config': {}
    },
    'Exploration':{
        'type': None,
        'config':{}
    },
    'Actor':{
        'type': None,
        'config':{}
    },
    'Critic':{
        'type': None,
        'config':{}
    },
    'Agent':{
        'type': None,
        'config':{}
    },
    'Sampler':{
        'type': None,
        'config':{}
    }
}



class RunnerBase(object):

    def __init__(self, config={}, use_ray=False, role='111', seed=None, port_num=None, logger=None):
        '''
        role: 1-- collect data
              -1- update policy
              --1 update critic
        '''

        self.RunnerBase_config = Configuration(default_config)
        self.RunnerBase_config.update(config)

        self.role = role
        self.seed = seed
        if self.seed is None:
            self.seed =self.RunnerBase_config.get('seed')

        self.ray = use_ray
        self.port_num = port_num
        self.logger = logger
        
    def create_logger(self):
        if self.role[1] == '0' and self.role[2] == '0':
            self.logger = None
        else:
            if self.logger is None:
                exp_dir = self.RunnerBase_config.get('ExpDir')
                self.logger = self.RunnerBase_config.get(['Logger', 'type'])(csv_data_dir=exp_dir.csv_data_hyperparam_exp_dir,
                                                                             log_dir=exp_dir.log_hyperparam_exp_dir,
                                                                             model_dir=exp_dir.model_hyperparam_exp_dir,
                                                                             config_dir=exp_dir.config_hyperparam_exp_dir,
                                                                             info_dir=exp_dir.info_hyperparam_exp_dir,
                                                                             transitions_dir=exp_dir.transitions_hyperparam_exp_dir,
                                                                             logging_params=self.RunnerBase_config.get(['Logger', 'config']))
            
                self.logger.save_config(self.RunnerBase_config)
                self.logger.log_text("cmd log: {}".format(self.RunnerBase_config.get('cmd_log')))

            
    def create_reset(self):
        self.reset = None
        if self.RunnerBase_config.get(['Reset', 'type']) is not None:
            self.reset = self.RunnerBase_config.get(['Reset', 'type'])(self.RunnerBase_config.get(['Reset', 'config']))


    def create_env(self):
        self.env = None
        if self.role[0] == '1': # if collect data
            env_config = self.RunnerBase_config.get(['Environment', 'config'])    
            self.env = self.RunnerBase_config.get(['Environment', 'type'])(env_config, seed=self.seed, port_num=self.port_num, suffix=self.suffix, reset=self.reset, logger=self.logger)
            self.obs_dim = self.env.state_space['shape'][0]
            self.action_dim = self.env.action_space['shape'][0]
        
    def create_preprocessors(self):
        self.state_preprocessor = None
        self.reward_preprocessor = None

        if self.RunnerBase_config.get(['Preprocessors', 'state_preprocessor', 'type']) is not None:
            config = self.RunnerBase_config.get(['Preprocessors', 'state_preprocessor', 'config'])
            config['dim'] = self.obs_dim
            self.state_preprocessor = self.RunnerBase_config.get(['Preprocessors', 'state_preprocessor', 'type'])(config, seed=self.seed)
            #### load ####
            if self.RunnerBase_config.get(['Preprocessors', 'state_preprocessor', 'config', 'load_path']) is not None:
                self.state_preprocessor.restore(self.RunnerBase_config.get(['Preprocessors', 'state_preprocessor', 'config', 'load_path']))
                
        if self.RunnerBase_config.get(['Preprocessors', 'reward_preprocessor', 'type']) is not None:
            self.reward_preprocessor = self.RunnerBase_config.get(['Preprocessors', 'reward_preprocessor', 'type'])(self.RunnerBase_config.get(['Preprocessors', 'reward_preprocessor', 'config']), seed=self.seed)
            #### load ####
            if self.RunnerBase_config.get(['Preprocessors', 'reward_preprocessor', 'config', 'load_path']) is not None:
                self.reward_preprocessor.restore(self.RunnerBase_config.get(['Preprocessors', 'reward_preprocessor', 'config', 'load_path']))

            
    def create_replay_buffer(self):
        self.replay_buffer = None
        if self.role[1] == '1' or self.role[2] == '1': # if update actor or critic
            if self.RunnerBase_config.get(['ReplayBuffer', 'type']) is not None:
                self.replay_buffer = self.RunnerBase_config.get(['ReplayBuffer', 'type'])( self.RunnerBase_config.get(['ReplayBuffer', 'config']), seed=self.seed)
                if self.RunnerBase_config.get(['ReplayBuffer', 'config', 'load_path']) is not None:
                    self.replay_buffer.restore(self.RunnerBase_config.get(['ReplayBuffer', 'config', 'load_path']))
       

    def create_exploration(self):
        self.exploration = None
        if self.RunnerBase_config.get(['Exploration', 'type']) is not None:
            explore_config = self.RunnerBase_config.get(['Exploration', 'config'])
            self.exploration = self.RunnerBase_config.get(['Exploration', 'type'])(exploration_params=explore_config, action_space=self.RunnerBase_config.get('action_space') ,seed=self.seed)
            
    def create_critic(self):
        self.critic = None
        if self.RunnerBase_config.get(['Critic', 'type']) is not None:
            critic_config = self.RunnerBase_config.get(['Critic', 'config'])
            critic_config['state_space'] = self.RunnerBase_config.get('state_space')
            critic_config['action_space'] = self.RunnerBase_config.get('action_space')
          
            self.critic = self.RunnerBase_config.get(['Critic', 'type'])(config=critic_config, seed=self.seed)
            
            if self.RunnerBase_config.get(['Critic', 'config', 'load_path']) is not None:
                self.critic.restore(self.RunnerBase_config.get(['Critic', 'config', 'load_path']))
                
    def create_actor(self):
        self.actor = None
        if self.RunnerBase_config.get(['Actor', 'type']) is not None:
            actor_config = self.RunnerBase_config.get(['Actor', 'config'])
            # actor_config['state_space'] = self.env.state_space
            # actor_config['action_space'] = self.env.action_space
            actor_config['state_space'] = self.RunnerBase_config.get('state_space')
            actor_config['action_space'] = self.RunnerBase_config.get('action_space')
            self.actor = self.RunnerBase_config.get(['Actor', 'type'])(config=actor_config,
                                                                       exploration_strategy=self.exploration,
                                                                       seed=self.seed)
            
            if self.RunnerBase_config.get(['Actor', 'config', 'load_path']) is not None:
                self.actor.restore(self.RunnerBase_config.get(['Actor', 'config', 'load_path']))
            
    def create_algo_agent(self):
        self.agent = None
        if self.role[1] == '1' or self.role[2] == '1': # if update actor or critic
            self.agent = self.RunnerBase_config.get(['Agent', 'type'])(config=self.RunnerBase_config.get(['Agent', 'config']),
                                                                       seed=self.seed,
                                                                       actor=self.actor,
                                                                       critic=self.critic,
                                                                       replay_buffer=self.replay_buffer,
                                                                       logger=self.logger)

    def create_sampler(self):
        self.sampler = None
        if self.role[0] == '1': # if collect data
            sampler_config = self.RunnerBase_config.get(['Sampler', 'config'])
            # if self.remote:
            #     sampler_config['update_preprocessors'] = False
            #     sampler_config['save_preprocessors'] = False
      
            self.sampler = self.RunnerBase_config.get(['Sampler', 'type'])(config=sampler_config,
                                                                          env=self.env,
                                                                          actor=self.actor,
                                                                          state_preprocessor=self.state_preprocessor,
                                                                          reward_preprocessor=self.reward_preprocessor,
                                                                          logger=self.logger)

            
    def create_agent(self):
        """
        Instantiate the necessary components for a training run, this include but not limited to

        logger, environment, actor, baseline, preprocessor, sampler, agent, etc

        """
        #### build infrastructure ####
        if self.RunnerBase_config.get(['create', 'logger']):
            self.create_logger()
        if self.RunnerBase_config.get(['create', 'reset']):
            self.create_reset()
        if self.RunnerBase_config.get(['create', 'environment']):
            self.create_env()
        if self.RunnerBase_config.get(['create', 'preprocessors']):
            self.create_preprocessors()
        if self.RunnerBase_config.get(['create', 'replay_buffer']):
            self.create_replay_buffer()
        if self.RunnerBase_config.get(['create', 'exploration']):
            self.create_exploration()

        #### build computation graph ####
        if self.RunnerBase_config.get(['create', 'critic']):
            self.create_critic()
        if self.RunnerBase_config.get(['create', 'actor']):
            self.create_actor()
        if self.RunnerBase_config.get(['create', 'algo_agent']):
            self.create_algo_agent()

        #### create sampler ####
        if self.RunnerBase_config.get(['create', 'sampler']):
            self.create_sampler()
        
    def get_trainable_weights(self):
        pass

    def set_trainable_weights(self, weights):
        pass
        
    def get_batch_rollout(self, batch_size=None, deterministic=False):
        if self.sampler:
            unscaled_batch, scaled_batch = self.sampler.get_batch(batch_size, deterministic)
            return unscaled_batch, scaled_batch, []
        else:
            raise ValueError("no sampler")


    def update_preprocessors(self, unscaled_batch):
        return None, None

    def restore_preprocessors(self, state_preprocessor_params=None, reward_preprocessor_params=None):
        pass

    def close(self):
        if self.logger is not None:
            self.logger.close()
        self.env.close()
