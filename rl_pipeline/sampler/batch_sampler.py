import numpy as np
import os
import cloudpickle
from future.utils import viewitems
import time
from rl_pipeline.configuration.configuration import Configuration

default_config = {
    'deterministic': False,
    'rollout_batch_size': 5,
    'max_episode_timesteps': 100,
    'init_batch_size': 0,
    'use_preprocessors': True,
    'update_preprocessors': False,
    'save_preprocessors': False,
    'log_info_keys': [],
    'process_rewards': None
}

class BatchSampler(object):

    def __init__(self,
                 config={},
                 env=None,
                 actor=None,
                 state_preprocessor=None,
                 reward_preprocessor=None,
                 logger=None,
                 **kwargs):

        self.BatchSampler_config = Configuration(default_config)
        self.BatchSampler_config.update(config)
        
        self.env = env # env can be an env object or a list of env objects, if latter, then multiple robots are used to collect batches
        self.policy = actor
        self.batch_size = self.BatchSampler_config.get('rollout_batch_size')
        self.episode_horizon = self.BatchSampler_config.get('max_episode_timesteps')
        self.init_batch_size = self.BatchSampler_config.get('init_batch_size')
        self.logger = logger
        
        self.config_use_preprocessors = self.BatchSampler_config.get('use_preprocessors')
        self.config_update_preprocessors = self.BatchSampler_config.get('update_preprocessors')
        self.config_save_preprocessors = self.BatchSampler_config.get('save_preprocessors')
        
        self.state_preprocessor = state_preprocessor
        self.reward_preprocessor = reward_preprocessor

        self.unscaled_batch = [] # this is the batch before normalization
        self.scaled_batch = [] # this is the batch after normalization

        self.init = False
        self.prev_state = None
        self.prev_action = None

        self.log_info_keys = self.BatchSampler_config.get('log_info_keys')

        # self.env_methods = [x for x, y in cls.__dict__.items() if type(y) == FunctionType]
        
    def init_sampler(self):
        if self.init_batch_size:
            print("Initializing preprocessor ...")
            for _ in range(self.init_batch_size):
                unscaled_traj, scaled_traj = self.run_episode(episode_horizon=self.episode_horizon)
            print("Finished preprocessor initializing")
        else:
            pass
        
    def run_episode(self, episode_horizon, deterministic=None, initial_state=None):
        """ Run single episode with option to animate
        Args:
        
        Returns: two dictionaries with keys "Observations", "Actions", "Rewards"
        Observations: shape = (episode len, obs_dim)
        Actions: shape = (episode len, act_dim)
        Rewards: shape = (episode len,)

        The first dictionary is original trajectory
        The second is normalized trajectory (same as the first if no state or reward preprocessor specified)
        """

        deterministic = deterministic
        if deterministic is None:
            deterministic = self.BatchSampler_config.get('deterministic')
        
        episode_start_time = time.time()
        
        if not episode_horizon:
            episode_horizon = self.episode_horizon

        self.env.reset()
        Observations, Actions, Rewards, Done, Info = [], [], [], [], {} # original trajectory
        n_Observations, n_Rewards = [], [] # normalized trajectory
        done = False
        timestep = 0
        while not done and timestep < episode_horizon:
            # populate info for logging
            env_info = self.env.get_info()
            for key, value in viewitems(env_info):
                if not isinstance(value, dict) and key in self.log_info_keys:
                    if key in Info.keys():
                        Info[key] = np.vstack([Info[key], np.array(value)])
                    else:
                        Info[key] = np.array(value)


            #### Assumes never done on the first step ####
            obs = self.env.get_state(all_info=env_info)
            # if timestep == 0 and initial_state is not None:
            #     obs = initial_state
                        
            Observations.append(obs)

            if self.state_preprocessor and self.config_use_preprocessors:
                n_obs = self.state_preprocessor.get_scaled_x(obs)
            else:
                n_obs = obs

            n_Observations.append(n_obs)

            #t1 = time.time()
            action = self.policy.get_action(n_obs.astype(np.float32), deterministic=deterministic)
            #t2 = time.time() - t1
            #print("dta: ", t2)
        
            
            Actions.append(action.flatten())

            if timestep > 0:
                reward = self.env.get_reward(state=self.prev_state,
                                             action=self.prev_action,
                                             next_state=obs,
                                             all_info=env_info)
                    
                Rewards.append(reward)
                if self.reward_preprocessor and self.config_use_preprocessors:
                    n_reward = self.reward_preprocessor.get_scaled_x(np.array(reward)) # usually just scale reward, mean should not be shifted
                else:
                    n_reward = reward

                n_Rewards.append(n_reward)

                # assume can not be done on the first step !!!!!!!
                done = self.env.is_done(next_state=obs, all_info=env_info)
                Done.append(done)        

                # print("state:", obs)
                # print("action:", action)
                # print("reward:", reward),
                # print("done:", done)
                # print("-----------")
                
            # Step environment
            #t1 = time.time()
            self.env.step(action)
            #t2 = time.time() - t1
            #print("dt: ", t2)

            self.prev_state = obs
            # action actually sent to the system, used to determine reward
            self.prev_action = self.env.action_dict['actions'] 
            
            timestep += 1
            
        
        env_info = self.env.get_info()
        # --- terminal state ---
        terminal_obs = self.env.get_state(all_info=env_info)
        if self.state_preprocessor and self.config_use_preprocessors:
            n_terimnal_obs = self.state_preprocessor.get_scaled_x(terminal_obs)
        else:
            n_terminal_obs = terminal_obs
        # --- terminal reward ---
        reward = self.env.get_reward(self.prev_state, self.prev_action, terminal_obs, all_info=env_info)
        Rewards.append(reward)
        if self.reward_preprocessor and self.config_use_preprocessors:
            n_reward = self.reward_preprocessor.get_scaled_x(np.array(reward)) # usually just scale reward, mean should not be shifted
        else:
            n_reward = reward
        n_Rewards.append(n_reward)
        # --- terminal done ---
        Done.append(done)
        
        # make sure dimensions are right
        assert(all([np.array(Observations).shape[0] == np.array(Actions).shape[0],
                    np.array(Observations).shape[0] == np.array(Rewards).reshape(-1,1).shape[0],
                    np.array(Observations).shape[0] == np.array(Done).shape[0]]))

     
        episode_time = time.time() - episode_start_time
        #print("episode_time: {} s".format(str(episode_time)))
        
        other = {'episode_time': episode_time}

        for key in Info.keys():
            Info[key] = np.array(Info[key])
        Info['episode_time'] = np.array([episode_time])

        #### process rewards (like in HER) ####
        if self.BatchSampler_config.get('process_rewards') is not None:
            Rewards = self.BatchSampler_config.get('process_rewards')(np.array(Observations), np.array(Actions))
        
        unscaled_traj = {"Observations": np.array(Observations), "Actions": np.array(Actions), "Rewards": np.array(Rewards).reshape(-1,1), "Done": np.array(Done).reshape(-1,1), "Terminal_state": np.array(terminal_obs), "Info": Info}
        scaled_traj = {"Observations": np.array(n_Observations), "Actions": np.array(Actions), "Rewards": np.array(n_Rewards).reshape(-1,1), "Done": np.array(Done).reshape(-1,1), "Terminal_state": np.array(n_obs), "Info": Info}
        
        return unscaled_traj, scaled_traj, other

    def update_preprocessors(self, traj):
        # update preprocessers
        if self.state_preprocessor:
            self.state_preprocessor.update(traj['Observations'])

        if self.reward_preprocessor:
            self.reward_preprocessor.update(traj['Rewards'])
            

    def save_preprocessors(self):
        # save preprocessor params for restoration
        if self.logger:
            if self.state_preprocessor:
                self.state_preprocessor.save_params(os.path.join(self.logger.info_dir, "state_preprocessor_params.pkl"))
            if self.reward_preprocessor:
                self.reward_preprocessor.save_params(os.path.join(self.logger.info_dir, "reward_preprocessor_params.pkl"))
                
    def get_batch(self, batch_size=None, episode_horizon=None, deterministic=False):
        '''
        returns two list of dictionarys [{"Observations":obs, "Actions":act, "Rewards": reward}, ...] where
        obs has dimension steps_per_episode x obs_dim
        act has dimention steps_per_episode-1 x act_dim
        reward has dimension steps_per_episode-1 x 1

        the first list is original (unscaled) trajectories, second is scaled
        '''

        # init sampler if not already
        if not self.init:
            self.init_sampler()
            self.init = True
            
        unscaled_batch = []
        scaled_batch = []
        episode_time = []
        
        if not batch_size:
            batch_size = self.batch_size
        if not episode_horizon:
            episode_horizon = self.episode_horizon
            
        for i in range(batch_size):
            # print("traj: {}".format(i))
            
            trajs = None
            while trajs is None:
                trajs = self.run_episode(episode_horizon, deterministic)
                
            unscaled_traj, scaled_traj, other = trajs
            unscaled_batch.append(unscaled_traj)
            scaled_batch.append(scaled_traj)
            episode_time.append(other['episode_time'])

        # update preprocessosrs
        if self.config_update_preprocessors and self.config_use_preprocessors:
            for unscaled_traj in unscaled_batch:
                self.update_preprocessors(unscaled_traj)
        if self.config_save_preprocessors and self.config_use_preprocessors:
            self.save_preprocessors()
    
        print("batch time: {} s".format(str(np.sum(np.array(episode_time)))))
        return unscaled_batch, scaled_batch


    def save_batch(self, batch, path):
        '''
        This function saves batch Observations, Actions as a pkl file
        '''
        res = cloudpickle.dumps(batch)
        with open(path, "wb") as f:
            f.write(res)
   
        
if __name__ == "__main__":
    pass