from .exploration_base import ExplorationBase
from rl_pipeline.configuration.configuration import Configuration
import numpy as np
import time
import random


default_config = {
    'base_config': {},
    'seed': 0,
    'mu': 0,
    'theta': 0.15,
    'sigma': 0.3
}

class OUExploration(ExplorationBase):
    """
    This strategy implements the Ornstein-Uhlenbeck process, which adds
    time-correlated noise to the actions taken by the deterministic policy.
    The OU process satisfies the following stochastic differential equation:
    dxt = theta*(mu - xt)*dt + sigma*dWt
    where Wt denotes the Wiener process
    """
    def __init__(self, config={}, action_space=None, seed=0, **kwargs):
        self.OUExploration = Configuration(default_config)
        self.OUExploration.update(config)

        super(OUExploration, self).__init__(self.OUExploration.get('base_config'), action_space, seed)
        
        if not seed:
            self.seed = self.OUExploration.get('seed')
        else:
            self.seed = seed

        np.random.seed(self.seed)

        self.action_space = action_space
        self.action_dim = action_space['shape'][0]
        
        self.mu = self.OUExploration.get('mu')
        self.theta = self.OUExploration.get('theta')
        self.sigma = self.OUExploration.get('sigma')
        self.reset()
        
    def reset(self):
        self.state = np.ones(self.action_dim) * self.mu
        
    def get_action_with_exploration(self, action, total_timestep=0):
        x = self.state
        dx = self.theta * (self.mu - x) + self.sigma * np.random.randn(len(x))
        self.state = x + dx
        
        noise = self.state
        action_with_noise = action + noise
        return action_with_noise
