class ExplorationBase(object):

    def __init__(self, exploration_params=None, action_space=None, seed=None, **kwargs):
        self.exploration_params = exploration_params

        # this is in the form {"type":"int", "shape": (1, ), "upper_bound":[4], "lower_bound":[0]}
        self.action_space = action_space 
        
    def get_action_with_exploration(self, action, total_timestep):
        '''
        Augment the action with exploration and return.
        'total_timestep' is the time since start of training
        '''
        raise NotImplementError("")