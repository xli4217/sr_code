from .exploration_base import ExplorationBase
from rl_pipeline.schedule.schedule import LinearSchedule
import numpy as np
import time
import random

class EpsilonLinearDecay(ExplorationBase):
    
    def __init__(self, exploration_params=None, action_space=None, seed=None, **kwargs):
        super(EpsilonLinearDecay, self).__init__(exploration_params, action_space, seed)
        self.total_decay_steps = self.exploration_params['total_decay_steps']
        self.epsilon = self.exploration_params['initial_epsilon']
        self.final_epsilon = self.exploration_params['final_epsilon']
        if not seed:
            self.seed = self.exploration_params['seed']
        else:
            self.seed = seed
        self.equal_sampling_cutoff_index = self.exploration_params['equal_sampling_cutoff_index']
        self.schedule = LinearSchedule(self.total_decay_steps, final_p=self.final_epsilon, initial_p=self.epsilon)
        self.action_space = action_space
        self.steps_of_uniform_random = self.exploration_params['steps_of_uniform_random']
        np.random.seed(self.seed)
        
    def get_action_with_exploration(self, action, total_timestep):
        '''
        Augment the action with exploration and return.
        'total_timestep' is the time since start of training
        '''
        assert isinstance(action, np.ndarray)
        
        # this is in the form {"type":"int", "shape": (1, ), "upper_bound":[4], "lower_bound":[0]}
        self.epsilon = self.schedule.value(total_timestep)
        if np.random.rand() >= self.epsilon:
            return action
        else:
            if self.action_space['type'] == "int":
                if not self.equal_sampling_cutoff_index:
                    return np.random.randint(low=self.action_space['lower_bound'][0], high=self.action_space['upper_bound'][0]+1)
                else:
                    if np.random.rand() >= 0.5:
                        return np.random.randint(low=self.action_space['lower_bound'][0], high=self.equal_sampling_cutoff_index)
                    else:
                        return np.random.randint(low=self.equal_sampling_cutoff_index, high=self.action_space['upper_bound'][0]+1)
            elif self.action_space['type'] == "float":
                # numpy version produces same rand vector for different agents when ray is used
                if total_timestep < self.steps_of_uniform_random:
                    sampled_action = [random.uniform(low, high) for low, high in zip(self.action_space['lower_bound'], self.action_space['upper_bound'])]
                else:
                    sampled_action = (action + np.random.normal(0, self.epsilon*np.array(self.action_space['upper_bound']), size=self.action_space['shape'][0])).clip(self.action_space['lower_bound'], self.action_space['upper_bound'])
                
                return sampled_action
                
            else:
                raise ValueError("Action type not supported.")

                