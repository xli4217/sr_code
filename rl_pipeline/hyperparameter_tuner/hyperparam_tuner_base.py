from rl_pipeline.utils.utils import make_hyperparam_str

class HyperparamTunerBase(object):

    '''
    Base class for a hyperparameter tuner
    '''
    def __init__(self, hyperparam_tuning_params, **kwargs):
        self.hyperparam_tuning_params = hyperparam_tuning_params
        self.params_dict = hyperparam_tuning_params['params_dict']
        
    def get_next_hyperparam_dict(self, metric=None, **kwargs):
        '''
        returns new hyperparam dict as {"lr": 0.001, "nb_layers": 3, ...} according to 'metric' (if needed)
        'metric' is not needed for methods such as random search or grid search
        '''

        raise NotImplementError("")

    def save_hyperparam_performance(self):
        '''
        saves the performance metric and corresponding hyperparameters as a table
        '''
        raise NotImplementError("")

    def make_hyperparam_string(self, hyperparam_dict):
        return make_hyperparam_str(hyperparam_dict)


    def save_hyperparm_tuner(self, save_dir):
        '''
        saves the current state of the hyperparameter tuner
        '''
        pass

    def restore_hyperparam_tuner(self, restore_dir):
        '''
        restores the hyperparameter tuner
        '''
        pass