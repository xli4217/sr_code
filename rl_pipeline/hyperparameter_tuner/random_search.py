import numpy as np
import pandas as pd
import os
from future.utils import viewitems

from rl_pipeline.hyperparameter_tuner.hyperparam_tuner_base import HyperparamTunerBase

class RandomSearch(HyperparamTunerBase):
    '''
    Base class for a hyperparameter tuner
    '''
    def __init__(self, hyperparam_tuning_params, **kwargs):
        
        super(RandomSearch, self).__init__(hyperparam_tuning_params)
        self.hyperparam_tuning_log = {"metric": [], "update_itr":[]}
        self.current_hyperparam_dict = {}
        self.metric = None
        self.update_itr = 0
        
    def get_next_hyperparam_dict(self, **kwargs):
        '''
        returns new hyperparam dict as {"lr": 0.001, "nb_layers": 3, ...} using random search
        '''
        new_hyperparam_dict = {}
        for key, value in viewitems(self.params_dict):
            if value['type'] == "float":
                new_hyperparam_dict[key] = np.random.uniform(value['range'][0],value['range'][1])
            if value['type'] == "int":
                new_hyperparam_dict[key] = np.random.randint(value['range'][0],value['range'][1])
                
        self.current_hyperparam_dict = new_hyperparam_dict

        return new_hyperparam_dict

    def save_hyperparam_performance(self, new_metric, save_path):
        '''
        updates and saves the performance metric and corresponding hyperparameters as a table
        '''
        self.metric = new_metric
        # update hyperparam_log
        for key, value in viewitems(self.current_hyperparam_dict):
            if key not in self.hyperparam_tuning_log.keys():
                self.hyperparam_tuning_log[key] = [value]
            else:
                self.hyperparam_tuning_log[key].append(value)

        self.hyperparam_tuning_log["metric"].append(self.metric)
        self.hyperparam_tuning_log["update_itr"].append(self.update_itr)
        self.update_itr += 1
        
        pd.DataFrame(data=self.hyperparam_tuning_log).to_csv(save_path)