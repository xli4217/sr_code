import numpy as np
import pandas as pd
import os
from future.utils import viewitems
import skopt

from rl_pipeline.hyperparameter_tuner.hyperparam_tuner_base import HyperparamTunerBase


default_config = {
    "performance_metric": "average_return",
    "nb_steps": 10,
    "params_dict":{
        # "prior" can be "uniform" or "log-uniform", "transform" can be "identity" or "normalize"
        "batch_size": {"type": "float", "range": [32, 64], "prior": "uniform", "transform": "identity"},
    },
    # this is optimizer specific arguments
    "optimizer": {
        "base_estimator": "GP",
        "n_initial_points": 3,
        "random_state": 5,
    }
}

class SkoptTuner(HyperparamTunerBase):

    def __init__(self, config, **kwargs):
        super(SkoptTuner, self).__init__(config)
        self.hyperparam_tuning_log = {"metric": [], "update_itr":[]}
        self.current_hyperparam_dict = {}
        self.metric = None
        self.config = config
        self.current_skopt_suggestion = None
        self.update_itr = 0
        
        # create spaces
        self.space = []
        for key, value in viewitems(self.config['params_dict']):
            if value['type'] == "float":
                space = skopt.space.Real(low=value['range'][0],
                                         high=value['range'][1],
                                         prior=value['prior'],
                                         transform=value['transform'],
                                         name=key)
                self.space.append(space)
            if value['type'] == "integer":
                space = skopt.space.Integer(low=value['range'][0],
                                            high=value['range'][1],
                                            transform=value['transform'],
                                            name=key)
                self.space.append(space)
            if value['type'] == "categorical":
                space = skopt.space.Categorical(categories=value['range'],
                                                prior=None,
                                                transform='identity',
                                                name=key)
                self.space.append(space)

        self.opt = skopt.Optimizer(dimensions=self.space,
                                   base_estimator=self.config['optimizer']['base_estimator'],
                                   n_initial_points=self.config['optimizer']['n_initial_points'],
                                   random_state=self.config['optimizer']['random_state'])
        
    def get_next_hyperparam_dict(self, **kwargs):
        new_hyperparam_dict = {}
        skopt_suggestion = self.opt.ask(n_points=1, strategy="cl_min")
        self.current_skopt_suggestion = skopt_suggestion
        
        hyperparam_keys = list(self.config['params_dict'].keys())
        for i in range(len(hyperparam_keys)):
            new_hyperparam_dict[hyperparam_keys[i]] = skopt_suggestion[0][i]

        self.current_hyperparam_dict = new_hyperparam_dict
        return new_hyperparam_dict
            
    def save_hyperparam_performance(self, new_metric, save_path):
        self.metric = new_metric
        # update hyperparam_log
        for key, value in viewitems(self.current_hyperparam_dict):
            if key not in self.hyperparam_tuning_log.keys():
                self.hyperparam_tuning_log[key] = [value]
            else:
                self.hyperparam_tuning_log[key].append(value)

        self.hyperparam_tuning_log["metric"].append(self.metric)
        self.hyperparam_tuning_log["update_itr"].append(self.update_itr)
        self.update_itr += 1
        
        pd.DataFrame(data=self.hyperparam_tuning_log).to_csv(save_path)

        # tell skopt the metric for current suggestion so to its model
        # negative sign is added because skopt aims to minimize the metric
        self.opt.tell(self.current_skopt_suggestion, [-new_metric], fit=True)
        
    def save_hyperparm_tuner(self, save_dir):
        pass

    def restore_hyperparam_tuner(self, restore_dir):
        pass
        
if __name__ == "__main__":
    config = { "performance_metric": "average_return",
               "nb_steps": 3,
               "params_dict":{
                   # "prior" can be "uniform" or "log-uniform", "transform" can be "identity" or "normalize"
                   "gamma": {"type": "float", "range": [0.9, 0.99], "prior": "uniform", "transform": "identity"},
                   "lam": {"type": "float", "range": [0.9,0.99], "prior": "uniform", "transform": "identity"}
               },
               # this is optimizer specific arguments
               "optimizer": {
                   "base_estimator": "GP",
                   "n_initial_points": 5,
                   "random_state": 5,
               }
           }

    skopt_tuner = SkoptTuner(config)
    print(skopt_tuner.get_next_hyperparam_dict())