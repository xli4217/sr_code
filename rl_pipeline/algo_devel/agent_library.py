import os
import numpy as np

from rl_pipeline.configuration.configuration import Configuration

default_config = {}

class AgentLibrary(object):

    def __init__(self, config={}):
        self.AgentLibrary_config = default_config
        self.AgentLibrary_config.update(config)

    def get_agent(self,
                  agent_name,
                  agent_config,
                  actor_config=None,
                  critic_config=None,
                  exploration_config=None):

        if agent_name == "qlearning":
            agent_funct = self.get_qlearning_agent
        elif agent_name == 'sac':
            agent_func = self.get_sac_agent
        elif agent_name == 'ddpg':
            agent_func = self.get_ddpg_agent
        elif agent_name == 'ppo':
            agent_func = self.get_ppo_agent
        else:
            raise ValueError('agent not supported')

        return agent_func(agent_name,
                          agent_config,
                          actor_config,
                          critic_config,
                          exploration_config)
            
    def get_qlearning_agent(self,
                            agent_name,
                            agent_config,
                            actor_config=None,
                            critic_config=None,
                            exploration_config=None):

        

        from rl_pipeline.algo_devel.q_learning.qlearning_runner import QLearningRunner
        from rl_pipeline.algo_devel.q_learning.agent.q_learning_agent import QLearningAgent
        from rl_pipeline.algo_devel.q_learning.policy.grid_value import GridValue
        from rl_pipeline.logging.tensorboardX_logger import TensorboardXLogger

        Exploration = {
            'type': EpsilonLinearDecay,
            'config': {
                'total_decay_steps': 3e5,
                'initial_epsilon': 1.0,
                'final_epsilon': 0.1,
                'seed': 0,
                'equal_sampling_cutoff_index': None,
                'action_space': None
                }
        }
        
        Actor = {
            'type': GridValue,
            'config':{
                'scope': "Q",
                'obs_dim': None,
                'action_dim': None
            }
        }
        
        Critic = {
            'type': None,
            'config':{}
        }
        
        
        Agent = {
            'type': QLearningAgent,
            'config':{
                'scope': 'qlearning_agent',
                'lr': 0.1,
                'gamma': 0.99,
                'log_component': {
                    'state_dist': True,
                    'action_dist': True,
                    'reward_dist': False,
                    'done_dist': False
                }
            }
        }
        
        return Exploration, Actor, Critic, Agent
        
    def get_sac_agent(self,
                      agent_name,
                      agent_config,
                      actor_config=None,
                      critic_config=None,
                      exploration_config=None):
        
        from rl_pipeline.algo_devel.sac.pytorch.sac_runner import SACRunner
        from rl_pipeline.algo_devel.sac.pytorch.agent.sac_agent import SACAgent
        from rl_pipeline.algo_devel.sac.pytorch.policy.mlp_gaussian_policy import MlpGaussianPolicy
        from rl_pipeline.algo_devel.sac.pytorch.value.mlp_value import MlpQ, MlpV
        from rl_pipeline.logging.tensorboardX_logger import TensorboardXLogger
        
        
        Exploration = {
            'type': None,
            'config': {}
        }
        
        Actor = {
            'type': MlpGaussianPolicy,
            'config':{
                'scope': 'policy',
                'obs_dim': None,
                'action_dim': None,
                'hidden_sizes': (300, 200, 100),
                # if this is not None, policy is restored
                'restore_policy_dir': None,
                'restore_policy_name': 'policy',
                #### specific ####
                'LOG_SIG_MAX': 2,
                'LOG_SIG_MIN': -20,
                'epsilon': 1e-6
            }
        }
        
        Critic_Q = {
            'type': MlpQ,
            'config':{
                'scope': "Q",
                'obs_dim': None,
                'action_dim': None
            }
        }
        
        Critic_V = {
            'type': MlpV,
            'config':{
                'scope': "V",
                'obs_dim': None,
                'action_dim': None
            }
        }
        
        Agent = {
            'type': SACAgent,
            'config':{
                'scope': 'sac_agent',
                'lr': agent_config['lr'],
                'gamma': agent_config['gamma'],
                'clip_gradient_by_norm': False,
                'batch_size': agent_config['batch_size'],
                'nb_optimization_steps_per_itr': agent_config['nb_optimization_steps_per_itr'],
                'target_q_update_interval':  agent_config['target_q_update_interval'],
                'no_update_until':  agent_config['no_update_until'],
                'soft_target_update':  agent_config['soft_target_update'],
                'optimizer': 'adam',
                'save_replay_buffer':  agent_config['save_replay_buffer'],
                'update_critic':  agent_config['update_critic'],
                'log_components': agent_config['log_components'],
                #### Specific ####
                # temperature
                'alpha': 'auto',
                'reward_scale': 10,
            } 
        }

        return Exploration, Actor, Critic_Q, Critic_V, Agent

    def get_ddpg_agent(self,
                       agent_name,
                       agent_config,
                       actor_config=None,
                       critic_config=None,
                       exploration_config=None):
        from rl_pipeline.algo_devel.ddpg.pytorch.ddpg_runner import DDPGRunner
        from rl_pipeline.algo_devel.ddpg.pytorch.agent.ddpg_agent import DDPGAgent
        from rl_pipeline.algo_devel.ddpg.pytorch.policy.simple_pytorch_mlp import PytorchMlp
        from rl_pipeline.algo_devel.ddpg.pytorch.value.mlp_q import PytorchMlpQ
        from rl_pipeline.algo_devel.ddpg.pytorch.value.td3_q import TD3Q
        from rl_pipeline.exploration.epsilon_linear_decay import EpsilonLinearDecay

        Exploration = {
                'type': EpsilonLinearDecay,
                'config': {
                    'total_decay_steps': agent_config['total_decay_steps'],
                    'initial_epsilon': agent_config['initial_epsilon'],
                    'final_epsilon': agent_config['final_epsilon'],
                    'seed': agent_config['seed'],
                    'steps_of_uniform_random': agent_config['steps_of_uniform_random'],
                    'equal_sampling_cutoff_index': None,
                    'action_space': None
                }
        }
    
        
        Actor = {
            'type': PytorchMlp,
            'config':{
                'scope': 'policy',
                'seed': agent_config['seed'],
                'obs_dim': None,
                'action_dim': None,
                'restore_dir': None,
                'restore_model_name': None,
            }
        }

        
        if agent_config['td3']:
            Critic = {
                'type': TD3Q,
                'config':{
                    'scope': 'Q',
                    'seed': agent_config['seed'],
                    'obs_dim': None,
                    'action_dim': None
                }
            }
    
        else:
            Critic = {
                'type': PytorchMlpQ,
                'config':{
                    'scope': "Q",
                    'seed': agent_config['seed'],
                    'obs_dim': None,
                    'action_dim': 1
                }
            }

           
            
        Agent = {
            'type': DDPGAgent,
            'config':{
                'base_config':{
                    'debug': True,
                    'actor_lr': agent_config['actor_lr'],
                    'critic_lr': agent_config['critic_lr'],
                    'seed': agent_config['seed'],
                    'gamma': agent_config['gamma'],
                    'minibatch_size': agent_config['minibatch_size'],
                    'optimizer': 'adam',
                    'log_components': agent_config['log_components']
                },
                'debug': True,
                'gpu': agent_config['gpu'],
                'scope': 'ddpg_agent',
                'demo': False,
                'demo_batch_size': 32,
                # policy to demo loss ratio
                'demo_lambda': 0.3,
                'save_replay_buffer': agent_config['save_replay_buffer'],
                'clip_gradient_by_norm': False,
                'nb_optimization_steps_per_itr': agent_config['nb_optimization_steps_per_itr'],
                'target_q_update_interval': agent_config['target_q_update_interval'],
                'no_update_until': agent_config['no_update_until'],
                'soft_target_update': agent_config['soft_target_update'],
                #### TD3 config ####
                'td3': agent_config['td3'],
                'policy_frequency': agent_config['policy_frequency'],
                'policy_noise': agent_config['policy_noise'],
                'noise_clip': agent_config['noise_clip'],
                #### composition config ####
                'update_critic': agent_config['update_critic']
            }
        }

        return Exploration, Actor, Critic, Agent

    def get_ppo_agent(self,
                      agent_name,
                      agent_config,
                      actor_config=None,
                      critic_config=None,
                      exploration_config=None):

        from rl_pipeline.algo_devel.ppo.pytorch.ppo_runner import PPORunner
        from rl_pipeline.algo_devel.ppo.pytorch.agent.ppo_agent import PPOAgent
        from rl_pipeline.algo_devel.ppo.pytorch.policy.mlp_policy import PytorchMlp
        from rl_pipeline.algo_devel.ppo.pytorch.value.mlp_value import PytorchMlpValue
        from rl_pipeline.logging.tensorboardX_logger import TensorboardXLogger
        
        Exploration = {
            'type': None,
            'config': {}
        }

        Actor = {
            'type': PytorchMlp,
            'config':{
                'scope': 'policy',
                'gpu': agent_config['gpu'],
                'obs_dim': None,
                'action_dim': None,
            }
        }

        Critic = {
            'type': PytorchMlpValue,
            'config':{
                'scope': "V",
                'gpu': agent_config['gpu'],
                'obs_dim': None,
                'action_dim': 1
            }
        }

        Agent = {
            'type': PPOAgent,
            'config':{
                'base_config':{
                    'debug': True,
                    'actor_lr': agent_config['actor_lr'],
                    'critic_lr': agent_config['critic_lr'],
                    'gamma': agent_config['gamma'],
                    'minibatch_size': agent_config['minibatch_size'],
                    'optimizer': 'adam',
                    'log_components': agent_config['log_components']
                },
                'scope': 'ppo_agent',
                'gpu': agent_config['gpu'],
                # if this is True, two replay buffers are maintained, policy loss is appended with a behavioral cloning loss
                'demo': False,
                'demo_batch_size': 32,
                # policy to demo loss ratio
                'demo_lambda': 0.1,
                'policy_value_opt_combine': True,
                # currently, policy and value optimization has the same minibatch size and trains for the same epoches
                'nb_training_epoches':agent_config['nb_training_epoches'],
                'eps': 0.2,
                'use_clipped_value_loss': None,
                'clip_param': 1.,
                'value_loss_coef': agent_config['value_loss_coef'],
                'entropy_coef': agent_config['entropy_coef'],
                'max_grad_norm': 1.,
                'kl_target': agent_config['kl_target'],
                # for GAE - 0 is total bootstrap, low variance high bias, 1 is n step return, high variance low bias
                'lam': agent_config['lam'],
            }
        }

        return Exploration, Actor, Critic, Agent