from rl_pipeline.configuration.configuration import Configuration
from rl_pipeline.agent.batch_agent_base import BatchAgentBase

import os
import numpy as np
import torch
import torch.nn as nn
from torch import autograd

import scipy.signal
from sklearn.utils import shuffle
import time

from future.utils import viewitems


default_config = {
    'scope': 'ppo_agent',
    'gpu': True,
    # if this is True, two replay buffers are maintained, policy loss is appended with a behavioral cloning loss
    'demo': False,
    'demo_batch_size': 32,
    # policy to demo loss ratio
    'demo_lambda': 0.1,
    'policy_value_opt_combine': True,
    # currently, policy and value optimization has the same minibatch size and trains for the same epoches
    'nb_training_epoches':1,
    'eps': 0.2,
    'use_clipped_value_loss': None,
    'clip_param': 1.,
    'value_loss_coef': 0.2,
    'entropy_coef': 0.1,
    'max_grad_norm': None,
    'kl_target': 0.03,
    # for GAE - 0 is total bootstrap, low variance high bias, 1 is n step return, high variance low bias
    'lam': 0,
    'base_config': {}
}

class PPOAgent(BatchAgentBase):

    def __init__(self,
                 config={},
                 policy=None,
                 old_policy=None,
                 value=None,
                 value_target=None,
                 replay_buffer=None,
                 logger=None,
                 **kwargs):
        
        self.PPOAgent_config = Configuration(default_config)
        self.PPOAgent_config.update(config)

        super(PPOAgent, self).__init__(config=self.PPOAgent_config.get('base_config'), logger=logger)
        
        self.name = self.PPOAgent_config.get('scope')

        self.gpu = self.PPOAgent_config.get('gpu')
        if self.gpu:
            if not torch.cuda.is_available():
                print('no cuda device available')
                self.gpu = False
                self.cuda_device_name = None
            else:
                self.cuda_device_name = torch.cuda.get_device_name(0)
                torch.cuda.empty_cache()
                # torch.cuda.memory_allocated()
                # toch.cuda.memory_cached()
        self.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

        
        self.policy = policy
        self.old_policy = old_policy
        self.old_policy.load_state_dict(self.policy.state_dict())
        
        self.v = value
        self.logger = logger
        if self.logger is not None:
            if self.name not in self.logger.writers.keys():
                self.logger.tfx_add_writer(self.name)

        self.nb_updates = 0

        self.eps = self.PPOAgent_config.get('eps')

        self.mse_loss_func = nn.MSELoss()

        self.build_optimizers()
        
    

    def actor_loss(self, adv, state, action):
        # all input arguments are pytorch tensor
        assert (all([
            isinstance(adv, torch.Tensor),
            isinstance(state, torch.Tensor),
            isinstance(action, torch.Tensor),
            len(adv.size()) == 2,
            len(state.size()) == 2,
            len(action.size()) == 2,
            adv.size()[0] == state.size()[0],
            action.size()[0] == state.size()[0]]))
        
        adv = adv.view(adv.numel())
            
        old_action_log_probs, _ = self.old_policy.get_action_log_probs_and_entropy(state, action)
 
        action_log_probs, entropy = self.policy.get_action_log_probs_and_entropy(state, action)
            
        ratio = torch.exp(action_log_probs - old_action_log_probs)

        clipfrac = (torch.abs(ratio - 1.0) > self.eps).double()
            
        surr_loss1 = ratio * adv
        surr_loss2 = torch.clamp(ratio, 1.0 - self.eps, 1 + self.eps) * adv

        policy_mean_entropy = entropy.mean()

        entropy_coef = float(self.PPOAgent_config.get('entropy_coef'))
        actor_loss = -torch.min(surr_loss1, surr_loss2).mean() - entropy_coef * policy_mean_entropy


        # old_action_prob = torch.exp(old_action_log_probs)
        # action_prob = torch.exp(action_log_probs)
        
        ## discrete KL
        # kl = (old_action_prob * (old_action_prob / action_prob).log()).sum()
        # kl = torch.nn.functional.kl_div(old_action_prob, action_prob)

        ## analytical KL
        kl = torch.distributions.kl.kl_divergence(self.policy.dist.dist, self.old_policy.dist.dist).mean()

        info_dict = {
                "scalar": {
                    'actor_loss': actor_loss,
                    'mean_entropy': policy_mean_entropy,
                    'kl_divergence': kl
                },
                "histogram": {
                    'old_action_log_probs': old_action_log_probs,
                    'action_log_probs': action_log_probs,
                    'ratio': ratio,
                    'surr_loss1': surr_loss1,
                    'surr_loss2': surr_loss2,
                    'clipfrac': clipfrac
                }
            }
        return actor_loss, info_dict

    def critic_loss(self, state, disc_sum_rew):

        assert all([
            isinstance(state, torch.Tensor),
            isinstance(disc_sum_rew, torch.Tensor),
            state.size()[0] > 1,
            state.size()[0] == disc_sum_rew.size()[0]])
            
        v = self.v.forward(state)
        v = v.view(v.numel())
        disc_sum_rew = disc_sum_rew.view(disc_sum_rew.numel())

        assert v.size() == disc_sum_rew.size()
            
        # vl = self.mse_loss_func(disc_sum_rew, v)
        #### MSE Value Loss ####
        critic_loss = torch.sum((disc_sum_rew - v)**2)/disc_sum_rew.size()[0]
        if self.PPOAgent_config.get('use_clipped_value_loss') is not None:
            critic_loss = torch.clamp(critic_loss, min=self.PPOAgent_config.get('use_clipped_value_loss')[0], max=self.PPOAgent_config.get('use_clipped_value_loss')[1])

        diff = disc_sum_rew - v
        std_diff = torch.std(diff)
        std_disc_sum_rew = torch.std(disc_sum_rew)
        explained_var = 1 - (std_diff / std_disc_sum_rew)

        info_dict = {
            "scalar": {
                'value_loss': critic_loss,
                'explained_variance': explained_var
            },
            "histogram": {}
        }

        assert len(critic_loss.size()) == 0
            
        return critic_loss, info_dict
    
    def build_optimizers(self):
        # build optimizers
        if self.optimizer == 'adam':
            if not self.PPOAgent_config.get('policy_value_opt_combine'):
                self.critic_opt = torch.optim.Adam(self.v.parameters(), self.critic_lr)
                self.actor_opt =torch.optim.Adam(self.policy.parameters(), self.actor_lr)
            else:
                self.actor_opt = torch.optim.Adam(list(self.policy.parameters()) + list(self.v.parameters()), self.actor_lr)
                self.critic_lr = None
        else:
            raise ValueError('optimizer not supported')
                
        
    def discount(self, x, gamma):
        """ Calculate discounted forward sum of a sequence at each point """
        assert isinstance(x, np.ndarray)
        
        x = x.flatten()
        res = scipy.signal.lfilter([1.0], [1.0, -gamma], x[::-1])[::-1]

        assert res.ndim == 1
        assert res.shape == x.shape
        
        return res


    def add_value(self, batch):
        """ Adds estimated value to all time steps of all trajectories
        Args:
        batch: as returned by sampler.get_batch()
 
        
        Returns:
        updated batch (mutates batch dictionary to add 'Values')
        """
        for traj in batch:
            obs = traj['Observations']
            values = self.v.get_prediction(obs)
            traj['Values'] = values.reshape(-1,1)
            assert traj['Values'].ndim == 2
            
        return batch


    def add_disc_sum_rew(self, batch, gamma):
        """ Adds discounted sum of rewards to all time steps of all trajectories
        Args:
        batch: as returned by sampler.get_batch()
        gamma: discount

        Returns:
        updated batch (mutates the batch dictionary to add 'Disc_sum_rewards')
        """
        for traj in batch:
            rewards = traj['Rewards']
            sum_rew  = np.array(self.discount(rewards, 1.0)).reshape(-1,1)
            disc_sum_rew = np.array(self.discount(rewards, gamma)).reshape(-1,1)

            # print('r:', rewards)
            # print('R:', disc_sum_rew)

            traj['Disc_sum_rew'] = disc_sum_rew
            traj['Sum_rew'] = sum_rew
            
            assert traj['Disc_sum_rew'].ndim == 2
            assert traj['Sum_rew'].ndim == 2
            
        return batch
    
        
    def gae(self, batch, gamma, lam):
        """ Add generalized advantage estimator.
        https://arxiv.org/pdf/1506.02438.pdf
        Args:
        batch: as returned by sampler.get_batch(), must include 'Values' key from add_value().
        gamma: reward discount
        lam: lambda (see paper).
        lam=0 : use TD residuals
        lam=1 : A =  Sum Discounted Rewards - V_hat(s)
        
        Returns:
        updated batch (mutates batch dictionary to add 'Advantages')
        """
    
        for traj in batch:                                                    
            rewards = traj['Rewards'].flatten()
            # temporal differences
            values = traj['Values'].flatten() 
            tds = rewards - values + np.append(values[1:] * gamma, 0) # the last observation is not recorded, this may cause problem
            advantages = self.discount(tds, gamma * lam)
            traj['Advantages'] = advantages.reshape(-1, 1)
            assert traj['Advantages'].ndim == 2
            
        return batch

    def build_training_set(self, batch):
        """
        Args:
        batch: batch after processing by add_disc_sum_rew(),
        add_value(), and add_gae()
        
        Returns: dictionary with keys "Observations", "Actions", "Advantages", "Disc_sum_rew"
        """

        # observations has one more row than other quantities, make everyone same length
        Observations = np.concatenate([traj['Observations'] for traj in batch]) 
        Actions = np.concatenate([traj['Actions'] for traj in batch])
        Disc_sum_rew = np.concatenate([traj['Disc_sum_rew'] for traj in batch])
        Advantages = np.concatenate([traj['Advantages'] for traj in batch])
        Sum_rew = np.concatenate([traj['Sum_rew'] for traj in batch])
        Values = np.concatenate([traj['Values'] for traj in batch])
        Done = np.concatenate([traj['Done'] for traj in batch])
        Rewards = np.concatenate([traj['Rewards'] for traj in batch])

        Discounted_returns = []
        Episode_length = []
        for traj in batch:
            Discounted_returns.append(self.discount(traj['Rewards'].flatten(), self.gamma)[0])
            Episode_length.append(traj['Rewards'].flatten().size)
            
        # normalize advantages
        Advantages = (Advantages - Advantages.mean()) / (Advantages.std() + 1e-6)
        
        train_set = {"Observations": Observations,
                     "Actions": Actions,
                     "Rewards": Rewards,
                     "Disc_sum_rew": Disc_sum_rew,
                     "Advantages": Advantages,
                     "Sum_rew": Sum_rew,
                     "Values": Values,
                     "Done": Done,
                     'Discounted_returns': np.array(Discounted_returns),
                     'Episode_length': np.array(Episode_length)
                 } 

           
        return train_set


    def shuffle_training_set(self, training_set):
        index = np.arange(training_set['Advantages'].size)
        shuffled_index = shuffle(index)
        for key in list(training_set.keys()):
            if training_set[key].shape[0] == len(shuffled_index):
                if training_set[key].ndim == 2:
                    training_set[key] = training_set[key][shuffled_index,:]
                else:
                    training_set[key] = training_set[key][shuffled_index]
        return training_set

    def get_training_subset(self, training_set, start, end):
        subset = {}
        for key in list(training_set.keys()):
            if training_set[key].ndim == 2:
                subset[key] = training_set[key][start:end, :]
            else:
                subset[key] = training_set[key][start:end]
        return subset
        
    def update_one_step(self, training_subset_dict):
        state = torch.tensor(training_subset_dict['Observations'].astype(np.float32))
        action = torch.tensor(training_subset_dict['Actions'].astype(np.float32))
        disc_sum_rew = torch.tensor(training_subset_dict['Disc_sum_rew'].astype(np.float32))
        adv = torch.tensor(training_subset_dict['Advantages'].astype(np.float32))
        done = torch.tensor(training_subset_dict['Done'].astype(np.float32))

        critic_loss, critic_info_dict = self.critic_loss(state, disc_sum_rew)
        actor_loss, actor_info_dict = self.actor_loss(adv, state, action) 
        
        if self.PPOAgent_config.get('policy_value_opt_combine'):
            self.actor_opt.zero_grad()

            with autograd.detect_anomaly():
                (critic_loss * self.PPOAgent_config.get('value_loss_coef') + actor_loss).backward()
                
            if self.PPOAgent_config.get('max_grad_norm') is not None:
                nn.utils.clip_grad_norm_(list(self.policy.parameters()) + list(self.v.parameters()), self.PPOAgent_config.get('max_grad_norm'))
                
            self.actor_opt.step()
        else:
            self.critic_opt.zero_grad()
            self.actor_opt.zero_grad()

            with autograd.detect_anomaly():
                critic_loss.backward()
                critic_loss.backward()

            if self.PPOAgent_config.get('max_grad_norm') is not None:
                nn.utils.clip_grad_norm_(self.policy.parameters(), self.PPOAgent_config.get('max_grad_norm'))
                nn.utils.clip_grad_norm_(self.v.parameters(), self.PPOAgent_config.get('max_grad_norm'))

            self.critic_opt.step()
            self.actor_opt.step()    
                
        self.nb_updates += 1

        info_dict_numpy = {"scalar": {}, "histogram": {}}
        for k, v in viewitems(actor_info_dict['scalar']):
            info_dict_numpy['scalar'][k] = v.data.cpu().numpy()
        for k, v in viewitems(critic_info_dict['scalar']):
            info_dict_numpy['scalar'][k] = v.data.cpu().numpy()
     
        for k, v in viewitems(actor_info_dict['histogram']):
            info_dict_numpy['histogram'][k] = v.data.cpu().numpy()
        for k, v in viewitems(critic_info_dict['histogram']):
            info_dict_numpy['histogram'][k] = v.data.cpu().numpy()

        self.update_summary(info_dict_numpy)
            
        return info_dict_numpy
        
    def update(self, unscaled_batch, scaled_batch, other, itr):   
        update_start_time = time.time()
        #### log batches for evaluation ####
        if self.logger is not None:
             # save current batch and models for visualization and reuse (in 'transitions' folder)
             self.logger.save_transitions(unscaled_batch,
                                          {'policy': self.policy, 'V': self.v},
                                          filename="itr_"+str(itr),
                                          itr=itr,
                                          interval=self.logger.logging_params['transitions_params']['interval'])
             

        #### Prepare training set ####
        batch_with_value = self.add_value(scaled_batch)
        batch_with_value_disc_sum_rew = self.add_disc_sum_rew(batch_with_value, self.gamma)
        batch_with_value_disc_sum_rew_gae = self.gae(batch_with_value_disc_sum_rew, self.gamma, self.PPOAgent_config.get('lam'))
        training_set_dict = self.build_training_set(batch_with_value_disc_sum_rew_gae)
        
        timesteps_per_batch = len(training_set_dict['Advantages'])
        minibatch_size = self.minibatch_size
        
        #### Sync old policy with current policy ####
        self.old_policy.load_state_dict(self.policy.state_dict())

        #### Update ####
        for _ in range(self.PPOAgent_config.get('nb_training_epoches')):
            training_set_dict = self.shuffle_training_set(training_set_dict)
            for start in range(0, timesteps_per_batch, minibatch_size):
                training_subset_dict = self.get_training_subset(training_set_dict, start, start+minibatch_size)
                ## depending on the size of tranining_set, the last minibatch might not have enough samples, so we skip update for the last minibatch
                if training_subset_dict['Observations'].shape[0] == minibatch_size:
                    info_dict = self.update_one_step(training_subset_dict)
                    # if new policy is too far from old, stop updating
                    if info_dict['scalar']['kl_divergence'] > self.PPOAgent_config.get('kl_target'):
                        break
                    # build summary_dict
                    summary_dict = {}
                    if self.logger.name != 'TensorboardXLogger':
                        raise ValueError("Logger incompatible")
                    else:                        
                        summary_dict = self.build_tensorboard_summary(scaled_batch, unscaled_batch, self.summary)
                        self.logger.tfx_writer_add_summary(self.name, summary_dict, self.nb_updates, record_distributions=True)
                        self.logger.tfx_writer_flush(self.name)
                        
        update_time = time.time() - update_start_time
        print("update time: {} s".format(str(update_time)))            


if __name__ == "__main__":
    cls = PPOAgent()