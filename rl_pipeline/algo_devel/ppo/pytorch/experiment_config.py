import os
import numpy as np
from future.utils import viewitems

##########################
# Env required callbacks #
##########################
def multiagent_particle_env_configs():
    def get_state(all_info):
        '''
        all_info = {
            'agent': {
                'pos': [x,y],
                'vel': [x,y]
            },
            'obstacles': [
                {'name': 'obstacle_0', 'pos': [x,y]},
                {'name': 'obstacle_1', 'pos': [x,y]},
            ],
            'goals': [
                {'name': 'goal_0', 'pos': [x,y]},
                {'name': 'goal_1', 'pos': [x,y]},
            ]
        }
        '''
        agent_state = list(all_info['agent']['pos']) + list(all_info['agent']['vel'])

        obstacle_rel_state = []
        for obstacle in all_info['obstacles']:
            obstacle_rel_state += list(obstacle['pos'] - all_info['agent']['pos'])

        goal_rel_state = []
        for goal in all_info['goals']:
            goal_rel_state += list(goal['pos'] - all_info['agent']['pos'])

        state = np.array(list(agent_state) + obstacle_rel_state + goal_rel_state)
    
        return state

    def get_reward(state=None, action=None, next_state=None):
        agent_pos = next_state[:2]
        goal_rel_pos = next_state[-2:]
        obstacle_0_rel_pos = next_state[4:6]
        obstacle_1_rel_pos = next_state[6:8]

        dist_to_goal = np.linalg.norm(goal_rel_pos)
        dist_to_obs_0 = np.linalg.norm(obstacle_0_rel_pos)
        dist_to_obs_1 = np.linalg.norm(obstacle_1_rel_pos)

        r = -dist_to_goal + 0.4 * (dist_to_obs_0 + dist_to_obs_1)

        return r
        
    def is_done(state=None, action=None, next_state=None):
        agent_pos = state[:2]
        goal_rel_pos = state[-2:]
        
        dist_to_goal = np.linalg.norm(goal_rel_pos) 
        
        done = False
        if dist_to_goal < 0.07:
            done = True
            print("done!")
        return done
    
    state_space = {'type': 'float', 'shape': (10, ), 'upper_bound': [], 'lower_bound': []}

    action_space = {'type': 'float', 'shape': (2, ), 'upper_bound': [0.2, 0.2], 'lower_bound': [-0.2, -0.2]}

    return get_state, get_reward, is_done, state_space, action_space