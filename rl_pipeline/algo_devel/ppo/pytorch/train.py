import os
import numpy as np
from train_config import construct_rl_experiment_config
from fire import Fire

from rl_pipeline.train.rl.run_one_experiment import RunOneExperiment

'''
This the the entry script
'''

def train():
    experiment_config = Fire(construct_rl_experiment_config)
    run_one_experiment = RunOneExperiment(experiment_config)
    run_one_experiment.run_one_experiment()
    
if __name__ == "__main__":
    train()