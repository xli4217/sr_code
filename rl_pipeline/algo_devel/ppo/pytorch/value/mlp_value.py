import torch
import torch.nn as nn
import torch.nn.functional as F

from rl_pipeline.configuration.configuration import Configuration
import os
import numpy as np

default_config = {
    'scope': 'value',
    # this can be 'V' or 'Q'
    'type': "V",
    'state_space': {'type': 'float', 'shape': (3, ), 'upper_bound': [], 'lower_bound': []},
    'action_space': {'type': 'float', 'shape': (3, ), 'upper_bound': [0.1,0.1,0.1], 'lower_bound': [-0.1,-0.1,-0.1]},
    'load_path': None
}

class PytorchMlpValue(nn.Module):
    def __init__(self,
                 config={},
                 seed=0,
                 trainable=True,
                 logger=None):

        self.config = Configuration(default_config)
        self.config.update(config)

        super(PytorchMlpValue, self).__init__()

        self.obs_dim = self.config.get('state_space')['shape'][0]
        self.action_dim = self.config.get('action_space')['shape'][0]
        
        self.logger = logger

        self.set_seed(seed)
        self.nb_get_prediction_calls = 0

        self.scope = self.config.get('scope')
        
        self.build_graph()
        if self.config.get('load_path') is not None:
            self.restore(self.config.get('load_path'))

        
        if not trainable:
            for param in self.parameters():
                param.requires_grad = False
        
    def build_graph(self):
        if self.config.get('type') == 'Q':
            self.fc1 = nn.Linear(self.obs_dim + self.action_dim, 200)
        elif self.config.get('type') == 'V':
            self.fc1 = nn.Linear(self.obs_dim, 200)
        else:
            raise ValueError("type not supported")
        self.fc2 = nn.Linear(200, 200)
        self.fc3 = nn.Linear(200, 200)

        self.output_layer = nn.Linear(200, 1)
    def save(self, model_dir, model_name="model"):
        save_dir = os.path.join(model_dir, self.config.get('scope'))
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)

        torch.save(self.state_dict(), os.path.join(save_dir, model_name))
            
    def restore(self, restore_path):
        saved_dict = torch.load(restore_path)
        if "state_dict" in saved_dict.keys():
           model_dict = saved_dict['state_dict']
        else:
           model_dict = saved_dict
        self.load_state_dict(model_dict)
        print("restored value function from {} !!!!!!".format(restore_path))
        
    def forward(self, s, a=None):
        """
        Args:
          s: pytorch tensor
          a: pytorch tensor
        """

        if self.config.get('type') == 'Q':
            x = torch.cat((s, a), 1)
        elif self.config.get('type') == 'V':
            x = s
        else:
            raise ValueError('type not supported')
            
        x = F.leaky_relu(self.fc1(x))
        x = F.leaky_relu(self.fc2(x))
        x = F.leaky_relu(self.fc3(x))
        out = self.output_layer(x)
        return out
        
    def get_prediction(self, s, a=None):
        """
        Args:
          s: numpy array
          a: numpy array
        """
        self.nb_get_prediction_calls += 1

        if s.ndim == 1:
            s = np.array([s])
        s = np.array(s, dtype=np.float32)
        s = torch.from_numpy(s)
      
        if self.config.get('type') == 'Q':    
            if a.ndim == 1:
                a = np.array([a])

            a = np.array(a, dtype=np.float32)
            a = torch.from_numpy(a)
            q = self.forward(s, a).data.numpy()
        elif self.config.get('type') == 'V':
            q = self.forward(s).data.numpy()
            
        # returns a numpy array of size n x 1
        return q
            
    def set_seed(self, seed):
        torch.manual_seed(seed)
        np.random.seed(seed)


if __name__ == "__main__":
    config = {
        'scope': 'policy',
        'type': 'V',
        'obs_dim': 3,
        'action_dim': 1
    }

    cls = PytorchMlpValue(config)
    s = np.array([2,2,2])
    a = np.array([1])
    print(cls.get_prediction(s))