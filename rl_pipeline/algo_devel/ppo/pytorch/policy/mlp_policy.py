import torch
import torch.nn as nn
import torch.nn.functional as F
from torch import autograd

from rl_pipeline.configuration.configuration import Configuration
import os
import numpy as np

class AddBias(nn.Module):
    def __init__(self, bias):
        super(AddBias, self).__init__()
        self._bias = nn.Parameter(bias.unsqueeze(1))

    def forward(self, x):
        if x.dim() == 2:
            bias = self._bias.t().view(1, -1)
        else:
            bias = self._bias.t().view(1, -1, 1, 1)

        return x + bias


class DiagGaussian(nn.Module):
    def __init__(self, num_inputs, num_outputs, action_space):
        super(DiagGaussian, self).__init__()

        init_ = lambda m: self.init(m, nn.init.orthogonal_,lambda x: nn.init.constant_(x, 0))

        self.fc_mean = init_(nn.Linear(num_inputs, num_outputs))
        self.logstd = AddBias(torch.zeros(num_outputs))
        
    def init(self, module, weight_init, bias_init, gain=1):
        weight_init(module.weight.data, gain=gain)
        bias_init(module.bias.data)
        return module
        
    def forward(self, x):

        action_mean = self.fc_mean(x)
        
        zeros = torch.zeros(action_mean.size()[-1]).unsqueeze(0)
        if x.is_cuda:
            zeros = zeros.cuda()            
        action_logstd = self.logstd(zeros)
            
        action_std = action_logstd.exp()
        cov = torch.diag(action_std.view(action_std.numel())) 
        
        self.dist = torch.distributions.multivariate_normal.MultivariateNormal(action_mean, cov)
        return self.dist



default_config = {
    'scope': 'policy',
    'gpu': False,
    'state_space': {'type': 'float', 'shape': (3, ), 'upper_bound': [], 'lower_bound': []},
    'action_space': {'type': 'float', 'shape': (3, ), 'upper_bound': [0.1,0.1,0.1], 'lower_bound': [-0.1,-0.1,-0.1]},
    'load_path': None,
}

class PytorchMlp(nn.Module):
    def __init__(self,
                 config={},
                 seed=0,
                 trainable=True,
                 logger=None,
                 exploration_strategy=None):

        self.config = Configuration(default_config)
        self.config.update(config)

        super(PytorchMlp, self).__init__()

        self.gpu = self.config.get('gpu')
        if self.gpu:
            if not torch.cuda.is_available():
                print('no cuda device available')
                self.gpu = False
                self.cuda_device_name = None
                self.device = 'cpu'
            else:
                self.cuda_device_name = torch.cuda.get_device_name(0)
                torch.cuda.empty_cache()
                # torch.cuda.memory_allocated()
                # toch.cuda.memory_cached()
                self.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
        else:
            self.device = 'cpu'
                
        self.explore = exploration_strategy
        self.logger = logger

        self.set_seed(seed)
        self.nb_get_action_calls = 0

        self.scope = self.config.get('scope')

        self.state_space = self.config.get('state_space')
        self.action_space = self.config.get('action_space')

        # self.action_scale = []
        # for i in range(self.action_space['shape'][0]):
        #     self.action_scale.append(max(abs(self.action_space['upper_bound'][i]), abs(self.action_space['lower_bound'][i])))
        # self.action_scale = np.array(self.action_scale, dtype=np.float32)

        
        self.build_graph()
        if self.config.get('load_path') is not None:
            self.restore(self.config.get('load_path'))
       

        self.mean_entropy = None
        self.action_log_probs = None    
        
    def build_graph(self):
        self.fc1 = nn.Linear(self.state_space['shape'][0], 200).to(self.device)
        self.fc2 = nn.Linear(200, 200).to(self.device)
        self.fc3 = nn.Linear(200, 200).to(self.device)
        self.dist = DiagGaussian(200, self.action_space['shape'][0], self.action_space).to(self.device)
        
    def save(self, model_dir, model_name="model"):
        save_dir = os.path.join(model_dir, self.config.get('scope'))
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)

        torch.save(self.state_dict(), os.path.join(save_dir, model_name))
            
    def restore(self, restore_path):
        saved_dict = torch.load(restore_path)
        if "state_dict" in saved_dict.keys():
           model_dict = saved_dict['state_dict']
        else:
           model_dict = saved_dict
        self.load_state_dict(model_dict)
        print("Restored policy from: {}!!!".format(restore_path))
        
    def forward(self, x):
        assert len(x.size()) == 2

        x = F.leaky_relu(self.fc1(x))
        x = F.leaky_relu(self.fc2(x))
        x = F.leaky_relu(self.fc3(x))
        with autograd.detect_anomaly():
            out = self.dist(x)
        return out

    def get_action(self, x, deterministic=False):
        '''
        currently doesn't handle batch x
        '''

        if isinstance(x, np.ndarray):
            dim = x.ndim
        else:
            dim = x.dim()
                
        if dim == 1:
            x = np.array([x])

        self.nb_get_action_calls += 1
        x = np.array(x, dtype=np.float32)
        x = torch.from_numpy(x).to(self.device)
        
        with autograd.detect_anomaly():
            current_dist = self.forward(x)
        
        if deterministic:
            a = current_dist.mean
        else:
            a = current_dist.sample()

        a = a.data.cpu().numpy()
        
        # returns a numpy array of size 1 x action_dim
        return a
        
    def get_action_log_probs_and_entropy(self, state, action):
        '''
        state and action have dim n x state_dim/action_dim,
        inputs are pytorch tensors
        '''
        assert state.size()[0] == action.size()[0]

        current_dist = self.forward(state)
        
        action_log_probs = current_dist.log_prob(action)

        entropy = current_dist.entropy()
        return action_log_probs, entropy
        
    def set_seed(self, seed):
        torch.manual_seed(seed)
        np.random.seed(seed)


if __name__ == "__main__":
    import time

    config = {
        'scope': 'policy',
        'gpu': True,
        'state_space': {'type': 'float', 'shape': (3, ), 'upper_bound': [], 'lower_bound': []},
        'action_space': {'type': 'float', 'shape': (4, ), 'upper_bound': [], 'lower_bound': []},
        'load_path': None,
    }

    cls = PytorchMlp(config)

    s = torch.rand(5,3)
    print(cls.get_action(s))

    # device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    # cls.to(device)
    # print(next(cls.parameters()).is_cuda)
    # for _ in range(10):
    #     t1 = time.time()
    #     s = np.random.rand(3)
    #     a = cls.get_action(s)
    #     t2 = time.time() - t1
    #     print('time ellapse:', t2)
    

    # obs = np.array([2,2])
    # print(cls.get_action(obs))
    # print(cls.entropy)
    # print(cls.action_log_probs)
    # print(cls.current_dist.mean)
    # print(cls.current_dist.stddev)

    # print(type(cls.parameters()))

    