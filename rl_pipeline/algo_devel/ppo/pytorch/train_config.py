import os
import numpy as np

from rl_pipeline.logging.tensorboardX_logger import TensorboardXLogger
from rl_pipeline.sampler.batch_sampler import BatchSampler
from rl_pipeline.train.rl.sync_runner import SynchronousRunner
from rl_pipeline.evaluation.evaluation import Evaluation
from rl_pipeline.hyperparameter_tuner.random_search import RandomSearch
from rl_pipeline.hyperparameter_tuner.skopt_tuner import SkoptTuner

#### algorithm specific ####
from rl_pipeline.algo_devel.ppo.pytorch.ppo_runner import PPORunner
from rl_pipeline.algo_devel.ppo.pytorch.agent.ppo_agent import PPOAgent
from rl_pipeline.algo_devel.ppo.pytorch.policy.mlp_policy import PytorchMlp
from rl_pipeline.algo_devel.ppo.pytorch.value.mlp_value import PytorchMlpValue

##############################################
# Config to construct algorithm runner class #
##############################################


def construct_rl_base_runner_config(env='multiagent_particle'):

    if env == "vrep_baxter":
        #### Vrep Baxter Configs ####
        from examples.icra2019.experiment_config import get_states
        from examples.icra2019.env.experiment_env_vrep import ExperimentEnvVrep

        Environment = {
            'type': ExperimentEnvVrep,
            'config': {
                'mode': 'deploy',
                'obj_sample_region': {'x':[0.7, 1.], 'y':[-0.45, 0.3], 'z':[-0.046, -0.045]},
                'base_env_config': {
                    "control_mode": 'velocity',
                    "state_space": {"type":"float", "shape":(16, ), "upper_bound":[], "lower_bound":[]},
                    "action_space": {"type":"float", "shape":(7, ), "upper_bound":[1.5,1.5,1.5,1.5,3.5,3.5,3.5], "lower_bound":[-1.5,-1.5,-1.5,-1.5,-3.5,-3.5,-3.5]},
                    "get_state": get_states
                }
            }
        }

    elif env == "multiagent_particle":
        #### MultiAgentParticleEnv Configs ####
        from rl_pipeline.env.multiagent_env.multiagent_env import MultiAgentParticleEnv
        from rl_pipeline.env.multiagent_env.scenario import Scenario
        from experiment_config import multiagent_particle_env_configs
        
        
        get_state, get_reward, is_done, state_space, action_space = multiagent_particle_env_configs()
        
        Environment = {
            'type': MultiAgentParticleEnv,
            'config': {
                "seed": 10,
                "state_space": state_space,
                "action_space": action_space,
                "get_state": get_state,
                "get_reward": get_reward,
                "is_done": is_done,
                ####
                # fixed frame view if True, in agent's frame if False
                "shared_viewer": True,
                "headless": True,
                "scenario": {
                    'type': Scenario,
                    'config': {
                        'num_obstacles': 2,
                        'num_goals': 1
                    }
                }
            }
        }
    else:
        raise ValueError("environment not supported")
        
    base_runner_config = {
        'ExpDir': None,
        'seed': 0,
        'env_constructed': False,
        'restore_runner_dir': None,
        'Logger': {
            'type': TensorboardXLogger,
            'config': {
                "csv_data_params": {},
                "log_params": {},
                "model_params": {
                    "interval": 50 # save model every 10 iterations
                },
                "transitions_params":{
                    "interval": 50, # record trajecory batch and the computation graph every this many iterations,
                }
            }
        },
        'Reset':{
            'type': None,
            'config':{}
        },
        'Environment': Environment,
        "Preprocessors":{
            "state_preprocessor": {"type": None, 'config':{"dim": None, "shift": True, "scale": True}},
            "reward_preprocessor": {"type": None, 'config':{"dim": 1, "shift": False, "scale": True}}
        },
        'ReplayBuffer': {
            'type': None,
            'config': {}
        },
        'Exploration':{
            'type': None,
            'config': {}
        },
        'Actor':{
            'type': PytorchMlp,
            'config':{
                'scope': 'policy',
                'obs_dim': None,
                'action_dim': None,
            }
        },
        'Critic':{
            'type': PytorchMlpValue,
            'config':{
                'scope': "V",
                'obs_dim': None,
                'action_dim': 1
            }
        },
        'Agent':{
            'type': PPOAgent,
            'config':{
                'scope': 'ppo_agent',
                # if this is True, two replay buffers are maintained, policy loss is appended with a behavioral cloning loss
                'demo': False,
                'demo_batch_size': 32,
                # policy to demo loss ratio
                'demo_lambda': 0.1,
                'policy_value_opt_combine': True,
                'policy_lr': 0.003,
                'value_lr': 0.003,
                # currently, policy and value optimization has the same minibatch size and trains for the same epoches
                'minibatch_size': 64,
                'nb_training_epoches':2,
                'gamma': 0.98,
                'optimizer': 'adam',
                'eps': 0.2,
                'use_clipped_value_loss': False,
                'clip_param': 1.,
                'value_loss_coef': 0.2,
                'entropy_coef': 0.1,
                'max_grad_norm': 1.,
                # for GAE - 0 is total bootstrap, low variance high bias, 1 is n step return, high variance low bias
                'lam': 0,
                'log_components':{
                    'state_dist': False,
                    'action_dist': False,
                    'reward_dist': False,
                    'done_dist': False,
                    'adv_dist': False,
                    'policy_gradient_dist': False,
                    'value_gradient_dist': False,
                    'policy_weights': False,
                    'value_weights': False,
                    'other_dist': False
                }
            }
        },
        'Sampler':{
            'type': BatchSampler,
            'config': {
                'rollout_batch_size': 5, # in units of episodes
                "max_episode_timesteps": 50,  
                "init_batch_size": 0, # run a few episode of untrained policy to initialize scaler
                'update_preprocessors': False, # this might be false if multiple runners are used to collect sample, their preprocessors might need to be updated with all experiences
                'save_preprocessors': False,
                'log_info_keys': []
            }
            
        }
    }

    return base_runner_config
    
########################
# Construct experiment #
########################
def construct_rl_experiment_config():

    base_runner_config = construct_rl_base_runner_config(env='multiagent_particle')
    
    experiment_config = {
        # this can be 'train' or 'hyperparam_tuning'
        'mode': 'train',
        'nb_random_seed_exp': 1,
        'ExpDir': None,
        'exp_root_dir': os.path.join(os.environ['RLFPS_PATH'], 'rl_pipeline', 'algo_devel', 'ppo', 'pytorch'),
        'exp_name': 'test',
        'seed': 0,
        'Runner':{
            'type': SynchronousRunner,
            'config': {
                'BaseRunner':{
                    'type': PPORunner,
                    'config': base_runner_config
                },
                'Evaluator': {
                    'type': Evaluation,
                    'config': {
                        'gamma': 0.99,
                        'log_state_dimensions': [],
                        'log_action_dimensions': [],
                        'log_info_keys': []
                    }
                },
                'nb_remote_runners': 0,
                "max_itr": 2000,
                'evaluation_logging': False,
                'evaluation_batch_size': 0,
                'evaluation_logging_interval': 0,
            }
        },
        'HyperparameterTuner':{
            'type': SkoptTuner,
            'config':{
                "performance_metric": "average_return",
                "nb_steps": 10,
                "params_dict":{
                    # "prior" can be "uniform" or "log-uniform", "transform" can be "identity" or "normalize"
                    "gamma": {"type": "float", "range": [0.9, 0.99], "prior": "uniform", "transform": "identity"},
                    "lam": {"type": "float", "range": [0.9,0.99], "prior": "uniform", "transform": "identity"}
                },
            # this is optimizer specific arguments
                "optimizer": {
                    "base_estimator": "GP",
                    "n_initial_points": 3,
                    "random_state": 5,
                }
            }
        }
    }

    return experiment_config 