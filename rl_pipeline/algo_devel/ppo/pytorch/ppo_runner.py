from rl_pipeline.configuration.configuration import Configuration
from rl_pipeline.train.rl.runner_base import RunnerBase
from rl_pipeline.configuration.experiment_dirs import ExperimentDirs

import numpy
import os
import fire
from future.utils import viewitems

import time
import ray
import copy
import cloudpickle

default_config = {
    # this is used to provide directories for the logger
    'ExpDir': None,
    'seed': 0,
    'Logger': {
        'type': None,
        'config': {}
    },
    'Reset':{
        'type': None,
        'config':{}
    },
    'Environment': {
        'type': None,
        'config': {
            'constructed': False
        }
    },
    "Preprocessors":{
        "state_preprocessor": {"type": None, 'config':{"dim": None, "shift": True, "scale": True}},
        "reward_preprocessor": {"type": None, 'config':{"dim": 1, "shift": False, "scale": True}}
    },
    'ReplayBuffer': {
        'type': None,
        'config': {}
    },
    'Exploration':{
        'type': None,
        'config':{}
    },
    'Actor':{
        'type': None,
        'config':{}
    },
    'Critic':{
        'type': None,
        'config':{}
    },
    'Agent':{
        'type': None,
        'config':{}
    },
    'Sampler':{
        'type': None,
        'config':{}
    }
}

class PPORunner(RunnerBase):

    def __init__(self, config={}, use_ray=False, role='111', seed=None, port_num=None, suffix=""):

        self.PPORunner_config = Configuration(default_config)
        self.PPORunner_config.update(config)
            
        super(PPORunner, self).__init__(config=config, role=role, seed=seed, port_num=port_num)

        self.role = role
        self.seed = seed
        if self.seed is None:
            self.seed =self.PPORunner_config.get('seed')
        self.port_num = port_num
        self.suffix = suffix
        self.use_ray = use_ray
        
        #### setups ####
        self.create_agent()
        
    def create_actor(self):
        self.actor = None
        policy_config = self.RunnerBase_config.get(['Actor', 'config'])
        policy_config['state_space'] = self.RunnerBase_config.get('state_space')
        policy_config['action_space'] = self.RunnerBase_config.get('action_space')
        self.actor = self.RunnerBase_config.get(['Actor', 'type'])(config=policy_config, seed=self.seed)
        self.old_actor = self.RunnerBase_config.get(['Actor', 'type'])(config=policy_config, trainable=False, seed=self.seed)

    def create_algo_agent(self):
        self.agent = None
        if self.role[1] == '1' or self.role[2] == '1': # if update actor or critic
            self.agent = self.RunnerBase_config.get(['Agent', 'type'])(config=self.RunnerBase_config.get(['Agent', 'config']),
                                                                       policy=self.actor,
                                                                       old_policy=self.old_actor,
                                                                       value=self.critic,
                                                                       logger=self.logger)


    def set_trainable_weights(self, weights):
        #### this is a little weird, sync_runner passes ids, somehow becomes weight dict ####
        # if self.use_ray:
        #     weights = ray.get(weights)
        self.actor.load_state_dict(weights)

    def get_trainable_weights(self):
        return self.actor.state_dict()
        
        
    def close(self):
        if self.logger is not None:
            self.logger.close()
        self.env.close()
        
if __name__ == "__main__":
    from config import base_runner_config
    runner = PPORunner(base_runner_config)
