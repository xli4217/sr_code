import os
import numpy as np

from rl_pipeline.configuration.configuration import Configuration

import cvxopt
from cvxopt import matrix
from cvxopt import solvers

solvers.options['show_progress'] = False

default_config = {

}

class CBF(object):

    def __init__(self, config={}):
        self.CBF_config = Configuration(default_config)
        self.CBF_config.update(config)

        self.name = 'cbf'

        self.init_val = None
        
    def set_initial_value(self, init_val=None):
        self.init_val = init_val
        
    def run(self, Q, p, G, h, A=None, b=None):
        '''
        definition set according to https://cvxopt.org/examples/tutorial/qp.html
        '''
        Q = matrix(Q)
        p = matrix(p)
        G = matrix(G)
        h = matrix(h)
        if A is not None and b is not None:
            A = matrix(A)
            b = matrix(b)
            sol = solvers.qp(Q, p, G, h, A, b, initval=self.init_val)
        else:
            sol = solvers.qp(Q, p, G, h, initval=self.init_val)

        return np.array(sol['x']).flatten()
        
if __name__ == "__main__":
    cls = CBF()

    Q = 2 * np.array([[2, .5], [.5, 1]])
    p = np.array([1., 1.])
    G = np.array([[-1., 0.], [0., -1.]])
    h = np.zeros(2)
    A = np.array([[1., 1.]])
    b = np.array([1.])

    cls.set_initial_value(np.array([2,3]))
    print(cls.run(Q,p,G,h,A,b))