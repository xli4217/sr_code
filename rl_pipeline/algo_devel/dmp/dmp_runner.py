from rl_pipeline.configuration.configuration import Configuration
from rl_pipeline.train.rl.runner_base import RunnerBase
from rl_pipeline.configuration.experiment_dirs import ExperimentDirs

import numpy as np
import os
import fire
from future.utils import viewitems

import time

default_config = {
    'forcing_function': 'rbf'
}

class DMPRunner(object):
    def __init__(self, config={}, use_ray=False, remote=False, seed=None, port_num=19997, suffix=""):
        self.DMPRunner_config = Configuration(default_config)
        self.DMPRunner_config.update(config)
        self.forcing_function  = self.DMPRunner_config.get('forcing_function')
        
        self.create_env()
        self.create_actor()
        
    def create_env(self):
        from rl_pipeline.algo_devel.dmp.experiment_config import ExperimentConfig
        exp_config = ExperimentConfig()
        self.env = exp_config.Environment['type'](exp_config.Environment['config'])
        self.obs_dim = self.env.state_space['shape'][0]
        self.action_space = self.env.action_space
        
    def create_actor(self):
        from rl_pipeline.algo_devel.dmp.dmp_policy.dmp_policy import DMPPolicy

        if self.forcing_function == 'rbf':
            from rl_pipeline.algo_devel.dmp.forcing_function.rbf import RBFPolicy
            forcing_function_type = RBFPolicy
            forcing_function_config = {
                'scope': 'policy',
                'obs_dim': self.obs_dim,
                'action_space': self.action_space,
                'restore_policy_dir': None,
                'restore_policy_name': 'policy',
                ####
                'n_dmps': self.action_space['shape'][0],
                # number of basis functions per DMP (only for rbf forcing fucntions)
                'n_bfs': 5,
                # tunable parameters, control amplitude of basis functions
                # has dimension n_dmps x n_bfs
                'w': 15*np.random.rand(self.action_space['shape'][0], 5),
                'canonical_system': None
            }
        elif self.forcing_function == 'mlp':
            from rl_pipeline.algo_devel.dmp.forcing_function.pytorch_mlp import PytorchMlp
            forcing_function_type = PytorchMlp
            forcing_function_config = {
                'scope': 'policy',
                'obs_dim': self.obs_dim,
                'action_space': self.action_space,
                'restore_policy_dir': None,
                'restore_policy_name': 'policy',
                ####
                'canonical_system': None
            }
        else:
            raise ValueError('forcing function not supported')
            
        actor_config = {
            'scope': 'dmp',
            ####
            # gain on attractor term y dynamics
            'ay': None,
            # gain on attractor term y dynamics
            'by': None,
            # timestep
            'dt': 0.1,
            # time scaling, increase tau to make the system execute faster
            'tau': 0.5,
            'forcing_function': {
                'type': forcing_function_type,
                'config': forcing_function_config
            }
            
        }
        
        self.policy = DMPPolicy(actor_config)
        
    def get_traj(self):
        traj = []
        others = []
        obs_i = self.env.reset()
        obs_i = self.env.get_state()
        traj.append(obs_i)
        self.policy.set_initial_pos(obs_i[:2])
        self.policy.set_goal_pos(np.array([0.5, 0.5]))
        self.policy.reset()
        for i in range(100):
            ddy, dy, y, other = self.policy.get_action(obs_i)
            self.env.step(y)
            obs_i = self.env.get_state()
            traj.append(obs_i)
            others.append(other)
            
        return np.array(traj), others
            
if __name__ == "__main__":
    config = {
        'forcing_function': 'mlp'
    }
    cls = DMPRunner(config=config)
    traj = cls.get_traj()
    #print(traj)
        