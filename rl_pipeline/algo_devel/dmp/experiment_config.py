import numpy as np
from future.utils import viewitems
import os
from rl_pipeline.configuration.configuration import Configuration

default_config = {
    'env_name': 'multiagent_particle'
}

class ExperimentConfig(object):
    def __init__(self, config={}):
        self.ExperimentConfig_config = Configuration(default_config)
        self.ExperimentConfig_config.update(config)

        self.env_name = self.ExperimentConfig_config.get('env_name')
        # used in batch_sampler to post-process rewards
        self.process_rewards = None
        self.Environment = None

        self.construct_mdp_env()


    def construct_mdp_env(self):
        if self.env_name == 'multiagent_particle':
            from rl_pipeline.env.multiagent_env.experiment_config import multiagent_particle_env_config
            
            self.get_state, self.get_reward, self.is_done, self.state_space, self.action_space, env_other = multiagent_particle_env_config()

            self.env_type = env_other['type']
            self.env_config = env_other['config']
            self.env_config['control_mode'] = 'position'
            self.env_config['headless'] = True
    
            self.env_config['action_space']['upper_bound'] = 50 * np.array(self.env_config['action_space']['upper_bound'])
            self.env_config['action_space']['lower_bound'] = 50 * np.array(self.env_config['action_space']['lower_bound'])
            
            self.Environment = {
                'type': self.env_type,
                'config': self.env_config
            }