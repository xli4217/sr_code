import numpy as np
import os
from rl_pipeline.configuration.configuration import Configuration
from rl_pipeline.algo_devel.dmp.dmp_policy.canonical_system import CanonicalSystem
import copy
from future.utils import viewitems


default_config = {
    'scope': 'dmp',
    'obs_dim': None,
    'action_space': None,
    ####
    # gain on attractor term y dynamics
    'ay': None,
    # gain on attractor term y dynamics
    'by': None,
    # timestep
    'dt': 0.01,
    # time scaling, increase tau to make the system execute faster
    'tau': 1.0,
    # indices of states from the state vector used in dmp
    'dmp_state_idx': [0,1],
    'forcing_function': {
        'type': None,
        'config': {
            'scope': 'forcing_function',
            'obs_dim': 2,
            'action_space': None,
            'restore_policy_dir': None,
            'restore_policy_name': 'policy',
        }
    }
}

class DMPPolicy(object):
    '''
    Implemented according to https://studywolf.wordpress.com/2013/11/16/dynamic-movement-primitives-part-1-the-basics/
    '''
    
    def __init__(self,
                 config={},
                 seed=0,
                 trainable=True,
                 logger=None,
                 exploration_strategy=None,
                 forcing_function=None):
        
        self.DMPPolicy_config = Configuration(default_config)
        self.DMPPolicy_config.update(config)

        self.explore = exploration_strategy
        self.logger = logger

        self.set_seed(seed)
        self.nb_get_action_calls = 0

        self.scope = self.DMPPolicy_config.get('scope')

        self.action_space = self.DMPPolicy_config.get(['action_space'])
        self.obs_dim = self.DMPPolicy_config.get(['obs_dim'])
        self.action_dim = self.action_space['shape'][0]
        ####

        self.dmp_state_idx = self.DMPPolicy_config.get('dmp_state_idx')
        self.build_graph()

        self.forcing_function = forcing_function
        if forcing_function is None and self.DMPPolicy_config.get('forcing_function')['type'] is not None:
            forcing_function_config = self.DMPPolicy_config.get('forcing_function')['config']
            forcing_function_config['canonical_system'] = self.cs
            # setup external forcing term
            self.forcing_function = self.DMPPolicy_config.get('forcing_function')['type'](forcing_function_config)

        self.goal = None
        self.y0 = None

        
    def build_graph(self):
        self.n_dmps = self.action_dim
        self.dt = self.DMPPolicy_config.get('dt')
        self.tau = self.DMPPolicy_config.get('tau')
        
        self.by = self.DMPPolicy_config.get('by')

        ay = self.DMPPolicy_config.get('ay')
        if ay is None:
            self.ay = np.ones(self.n_dmps) * 25
        elif isinstance(ay, (int, float)):
            self.ay = np.ones(self.n_dmps) * ay
        else:
            self.ay = ay

        
        by = self.DMPPolicy_config.get('by')
        if by is None:
            self.by = self.ay / 4
        elif isinstance(by, (int, flot)):
            self.by = np.ones(self.n_dmps) * by
        else:
            self.by = by

        self.cs = CanonicalSystem(dt=self.dt)
        self.timesteps = int(self.cs.run_time / self.dt)
        
    def check_offset(self):
        """Check to see if initial position and goal are the same
        if they are, offset slightly so that the forcing term is not 0"""

        for d in range(self.n_dmps):
            if (self.y0[d] == self.goal[d]):
                print("initial position too close to goal, offsetting")
                self.goal[d] += 1e-4

    def gen_front_term(self, cx):
        """Generates the diminishing front term on
        the forcing term.

        x float: the current value of the canonical system
        dmp_num int: the index of the current dmp
        """
        return cx * (self.goal - self.y0)

    def set_initial_pos(self, y0):
        self.y0 = y0

    def set_goal_pos(self, goal):
        self.goal = goal

        
    def reset(self, y=None, dy=None, ddy=None):
        if y is None:
            self.y = copy.copy(self.y0).astype(np.float64)
        else:
            self.y = copy.copy(y).astype(np.float64)
            
        if dy is None:
            self.dy = np.zeros(self.n_dmps).astype(np.float64)
        else:
            self.dy = copy.copy(dy).astype(np.float64)

        if ddy is None:
            self.ddy = np.zeros(self.n_dmps).astype(np.float64)
        else:
            self.ddy = copy.copy(ddy).astype(np.float64)

        self.ddy = np.zeros(self.n_dmps).astype(np.float64)
        self.cs.reset_state()

        if self.goal is not None:
            self.check_offset()

    def reset_cs(self):
        self.cs.reset_state()
            
    def save(self, model_dir, model_name="model"):
        save_dir = os.path.join(model_dir, self.config.get('scope'))
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)

    def restore(self, model_dir, model_name="model"):
        if self.config.get('scope'):
            restore_path = os.path.join(model_dir, self.config.get('scope'), model_name)
            print("!!!!!!!")
            print(restore_path)
        else:
            restore_path = os.path.join(model_dir, model_name)

    def get_action(self, x=None, ext_f=None, decay_ext_f=True, deterministic=False):
        
        self.nb_get_action_calls += 1
        if x is not None:
            x = np.array(x, dtype=np.float32)

        # optional system feedback
        error = 1.0

        error_coupling = 1. / (1. + error)

        cx = self.cs.step(tau=self.tau, error_coupling=error_coupling)

        # generate forcing term
        front_terms = self.gen_front_term(cx)

        if ext_f is not None:
            f = ext_f
            if decay_ext_f:
                f = ext_f * front_terms
        elif ext_f is None and self.forcing_function is not None:
            f = self.forcing_function.get_action(x, cx, deterministic=deterministic) * front_terms
            if decay_ext_f:
                f = front_terms * f
        else:
            f = np.zeros(self.action_dim)

        f = f.flatten()
        point_attractor = self.ay * ( self.by * (self.goal - self.y) - self.dy )

        self.ddy = ( point_attractor + f ) * self.tau

        self.dy += self.ddy * self.dt * error_coupling
        self.y += self.dy * self.dt * error_coupling

        
        forcing_function_info = {}
        if self.forcing_function is not None:
            forcing_function_info = self.forcing_function.get_info()
        
        other = {
            'cx': { 'cx': cx},
            'accel':{
                'term1_x': point_attractor[0],
                'term1_y': point_attractor[1],
                'fx': f[0],
                'fy': f[1],
                'ddy_x': self.ddy[0],
                'ddy_y': self.ddy[1]
            },
            'velocity':{
                'dy_x': self.dy[0],
                'dy_y': self.dy[1]
            },
            'position':{
                'y_x': self.y[0],
                'y_y': self.y[1]
            },
           
            'forcing_function': forcing_function_info
        }
        
        if self.logger:
            self.logger.csv_record_tabular('step', self.nb_get_action_calls)
            for figure_name, figure_content in viewitems(other):
                for k, v in viewitems(figure_content):
                    self.logger.csv_record_tabular(k, v)



        return self.ddy, self.dy, self.y, other
        
        
    def set_seed(self, seed):
        np.random.seed(seed)


if __name__ == "__main__":
    from rl_pipeline.algo_devel.dmp.forcing_function.rbf import RBFPolicy
    
    config = {
        'scope': 'dmp',
        ####
        # gain on attractor term y dynamics
        'ay': None,
        # gain on attractor term y dynamics
        'by': None,
        # timestep
        'dt': 0.01,
        # time scaling, increase tau to make the system execute faster
        'tau': 1.0,
        'forcing_function': {
            'type': RBFPolicy,
            'config': {
                'scope': 'rbf',
                'obs_dim': 2,
                'action_space': {'type': 'float', 'shape': (2, ), 'upper_bound': [], 'lower_bound': []},
                'restore_policy_dir': None,
                'restore_policy_name': 'policy',
                ####
                'n_dmps': 2,
                # number of basis functions per DMP (only for rbf forcing fucntions)
                'n_bfs': 10,
                # tunable parameters, control amplitude of basis functions
                # has dimension n_dmps x n_bfs
                'w': np.random.rand(2, 10),
                'canonical_system': None
            }
        }
    }

    cls = DMPPolicy(config)
    print(cls.get_action(np.array([2,2])))