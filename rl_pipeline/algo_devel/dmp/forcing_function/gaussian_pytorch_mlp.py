import torch
import torch.nn as nn
import torch.nn.functional as F

from rl_pipeline.configuration.configuration import Configuration
import os
import numpy as np

class AddBias(nn.Module):
    def __init__(self, bias):
        super(AddBias, self).__init__()
        self._bias = nn.Parameter(bias.unsqueeze(1))

    def forward(self, x):
        if x.dim() == 2:
            bias = self._bias.t().view(1, -1)
        else:
            bias = self._bias.t().view(1, -1, 1, 1)

        return x + bias


class DiagGaussian(nn.Module):
    def __init__(self, num_inputs, num_outputs):
        super(DiagGaussian, self).__init__()

        init_ = lambda m: self.init(m,
            nn.init.orthogonal_,
            lambda x: nn.init.constant_(x, 0))

        self.fc_mean = init_(nn.Linear(num_inputs, num_outputs))
        self.logstd = AddBias(torch.zeros(num_outputs))

    def init(self, module, weight_init, bias_init, gain=1):
        weight_init(module.weight.data, gain=gain)
        bias_init(module.bias.data)
        return module
        
    def forward(self, x):
        action_mean = self.fc_mean(x)
        
        zeros = torch.zeros(action_mean.size()[-1]).unsqueeze(0)
        if x.is_cuda:
            zeros = zeros.cuda()

        action_logstd = self.logstd(zeros)
        action_std = action_logstd.exp()
        cov = torch.diag(action_std.view(action_std.numel()))

        return torch.distributions.multivariate_normal.MultivariateNormal(action_mean, cov)



default_config = {
    'scope': 'policy',
    'obs_dim': 2,
    'action_dim': 2,
    'restore_policy_dir': None,
    'restore_policy_name': 'policy'
}

class GaussianPytorchMlp(nn.Module):
    def __init__(self,
                 config={},
                 seed=0,
                 trainable=True,
                 logger=None,
                 exploration_strategy=None):
        
        self.config = Configuration(default_config)
        self.config.update(config)

        super(GaussianPytorchMlp, self).__init__()

        # print()
        self.explore = exploration_strategy
        self.logger = logger

        self.set_seed(seed)
        self.nb_get_action_calls = 0

        self.scope = self.config.get('scope')

        self.build_graph()
        if self.config.get('restore_policy_dir') is not None:
            self.restore(self.config.get('restore_policy_dir'), model_name=self.config.get('restore_policy_name'))
       

        self.mean_entropy = None
        self.action_log_probs = None    
            
    def build_graph(self):
        self.fc1 = nn.Linear(self.config.get('obs_dim'), 300)
        self.fc2 = nn.Linear(300, 200)
        self.fc3 = nn.Linear(200, 100)
        self.dist = DiagGaussian(100, self.config.get('action_dim'))
        
    def save(self, model_dir, model_name="model"):
        save_dir = os.path.join(model_dir, self.config.get('scope'))
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)

        torch.save(self.state_dict(), os.path.join(save_dir, model_name))
            
    def restore(self, model_dir, model_name="model"):
        if self.config.get('scope'):
            restore_path = os.path.join(model_dir, self.config.get('scope'), model_name)
            print("!!!!!!!")
            print(restore_path)
        else:
            restore_path = os.path.join(model_dir, model_name)

        saved_dict = torch.load(restore_path)
        if "state_dict" in saved_dict.keys():
           model_dict = saved_dict['state_dict']
        else:
           model_dict = saved_dict
        self.load_state_dict(model_dict)
        print("Restored policy from: {}".format(os.path.join(model_dir, model_name)))
        
    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.relu(self.fc3(x))
        out = self.dist(x)
        return out

    def get_forcing_action(self, x, deterministic=False):
        '''
        currently doesn't handle batch x
        '''
        if isinstance(x, np.ndarray):
            dim = x.ndim
        else:
            dim = x.dim()
                
        if dim == 1:
            x = np.array([x])

        self.nb_get_action_calls += 1
        x = np.array(x, dtype=np.float32)
        x = torch.from_numpy(x)

        current_dist = self.forward(x)
        
        if deterministic:
            a = current_dist.mean
        else:
            a = current_dist.sample()
            
        # returns a numpy array of size 1 x action_dim
        return a.data.numpy()

        
    def get_action(self, x, deterministic=False):
        '''
        currently doesn't handle batch x
        '''
        if isinstance(x, np.ndarray):
            dim = x.ndim
        else:
            dim = x.dim()
                
        if dim == 1:
            x = np.array([x])

        self.nb_get_action_calls += 1
        x = np.array(x, dtype=np.float32)
        x = torch.from_numpy(x)

        current_dist = self.forward(x)
        
        if deterministic:
            a = current_dist.mean
        else:
            a = current_dist.sample()
            
        # returns a numpy array of size 1 x action_dim
        return a.data.numpy()
        
    def get_action_log_probs_and_entropy(self, state, action):
        '''
        state and action have dim n x state_dim/action_dim
        '''
        #print(state.size())
        #print(current_dist.mean.size())
        #print(action.size())

        current_dist = self.forward(state)
        
        action_log_probs = current_dist.log_prob(action)

        entropy = current_dist.entropy()
        return action_log_probs, entropy
        
    def set_seed(self, seed):
        torch.manual_seed(seed)
        np.random.seed(seed)
