import numpy as np
import os
from rl_pipeline.configuration.configuration import Configuration
import copy

default_config = {
    'scope': 'forcing_function',
    'obs_dim': 2,
    'action_space': None,
    'restore_policy_dir': None,
    'restore_policy_name': 'policy',
    ####
    'n_dmps': 10,
    # number of basis functions per DMP (only for rbf forcing fucntions)
    'n_bfs': 10,
    # tunable parameters, control amplitude of basis functions
    # has dimension n_dmps x n_bfs
    'w': None,
    'canonical_system': None
}

class RBFPolicy(object):

    def __init__(self,
                 config={},
                 seed=0,
                 trainable=True,
                 logger=None,
                 exploration_strategy=None,
                 ####
                 n_dmps=None,
                 canonical_system=None):
        
        self.RBFPolicy_config = Configuration(default_config)
        self.RBFPolicy_config.update(config)

        self.explore = exploration_strategy
        self.logger = logger

        self.set_seed(seed)
        self.nb_get_action_calls = 0

        self.scope = self.RBFPolicy_config.get('scope')
        self.obs_dim = self.RBFPolicy_config.get('obs_dim')
        self.action_space = self.RBFPolicy_config.get('action_space')
        self.action_dim = self.action_space['shape'][0]

        self.build_graph()

        ####

        self.n_bfs = self.RBFPolicy_config.get('n_bfs')
        self.w = self.RBFPolicy_config.get('w')
        self.cs = canonical_system
        if canonical_system is None:
            self.cs = self.RBFPolicy_config.get('canonical_system')
        self.n_dmps = n_dmps
        if n_dmps is None:
            self.n_dmps = self.RBFPolicy_config.get('n_dmps')
        
        self.gen_centers()

        # set variance of Gaussian basis functions
        # trial and error to find this spacing
        self.h = np.ones(self.n_bfs) * self.n_bfs**1.5 / self.c / self.cs.ax
        self.psi = []
        
    def build_graph(self):
        pass

    def gen_centers(self):
        """Set the centre of the Gaussian basis
        functions be spaced evenly throughout run time"""

        '''x_track = self.cs.discrete_rollout()
        t = np.arange(len(x_track))*self.dt
        # choose the points in time we'd like centers to be at
        c_des = np.linspace(0, self.cs.run_time, self.n_bfs)
        self.c = np.zeros(len(c_des))
        for ii, point in enumerate(c_des):
            diff = abs(t - point)
            self.c[ii] = x_track[np.where(diff == min(diff))[0][0]]'''

        # desired activations throughout time
        des_c = np.linspace(0, self.cs.run_time, self.n_bfs)

        self.c = np.ones(len(des_c))
        for n in range(len(des_c)):
            # finding x for desired times t
            self.c[n] = np.exp(-self.cs.ax * des_c[n])

        # # c has dimension n_dmps x n_bfs
        # self.c = np.title(self.c, (self.n_dmps, 1))
            
   
    def gen_psi(self, cx):
        """Generates the activity of the basis functions for a given
        canonical system rollout.

        x float, array: the canonical system state or path
        """

        if isinstance(cx, np.ndarray):
            cx = cx[:, None]
        return np.exp(-self.h * (cx - self.c)**2)

    def get_info(self):
        info = {}
        i = 0
        for psi in self.psi:
            info['rbf'+str(i)] = psi
            i += 1
        return info
        
    def get_action(self, x, cx=0., deterministic=False):
        '''
        here x is the state of the MDP state, cx is the state of the canonical system
        '''
        self.psi = self.gen_psi(cx)
        return np.dot(self.w, self.psi.T) / np.sum(self.psi)

            
    def save(self, model_dir, model_name="model"):
        save_dir = os.path.join(model_dir, self.config.get('scope'))
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)

    def restore(self, model_dir, model_name="model"):
        if self.config.get('scope'):
            restore_path = os.path.join(model_dir, self.config.get('scope'), model_name)
            print("!!!!!!!")
            print(restore_path)
        else:
            restore_path = os.path.join(model_dir, model_name)

    def set_seed(self, seed):
        np.random.seed(seed)

        
    #### for imitation ####
        
    def gen_goal(self, y_des):
        """Generate the goal for path imitation.
        For rhythmic DMPs the goal is the average of the
        desired trajectory.

        y_des np.array: the desired trajectory to follow
        """

        return np.copy(y_des[:, -1])

        
    def gen_weights(self, f_target):
        """Generate a set of weights over the basis functions such
        that the target forcing term trajectory is matched.

        f_target np.array: the desired forcing term trajectory
        """

        # calculate x and psi
        x_track = self.cs.rollout()
        psi_track = self.gen_psi(x_track)

        # efficiently calculate BF weights using weighted linear regression
        self.w = np.zeros((self.n_dmps, self.n_bfs))
        for d in range(self.n_dmps):
            # spatial scaling term
            k = (self.goal[d] - self.y0[d])
            for b in range(self.n_bfs):
                numer = np.sum(x_track * psi_track[:, b] * f_target[:, d])
                denom = np.sum(x_track**2 * psi_track[:, b])
                self.w[d, b] = numer / (k * denom)
        self.w = np.nan_to_num(self.w)

        
