import os
import shutil

class ExperimentDirs(object):

    '''
    This class holds all the paths and hyperparameters. Experiment paths follow this hierarchy

    + Experiment
    |
    + - exp1
    |
    + -- csv_data
    |
    + --- hyperparam1 (csv_data for hyperparam1 is stored in this folder)
    .
    .
    .
    |
    + -- log
    |
    + --- hyperparam1
    .
    .
    .
    + -- models
    |
    + --- hyperparam1
    .
    .
    .
    + --- config
    |
    + -- hyperparam1
    .
    .
    .
    + --- info
    |
    + -- hyperparam1
    .
    .
    .
    + - exp2
    |
    .
    .
    .
    
    '''
    def __init__(self,
                 experiment_dir,
                 experiment_name):
        
        self.exp_dir = experiment_dir          # this is the highest level experiment data directory
        self.this_exp_dir = None               # this is the directory for the current experiment
        self.csv_data_dir = None               # this is the directory for csv data
        self.log_dir = None                    # this is the directory for logs (etc error log or tensorboard log)
        self.model_dir = None                  # this is the directory for the saved models
        self.transitions_log_dir = None        # this is the directory for saving all transitions experienced (for model learning)

        self.csv_data_hyperparam_exp_dir = None # this is where the data.csv is stored for this hyperparam setting
        self.log_hyperparam_exp_dir = None      # .
        self.model_hyperparam_exp_dir = None    # .
        self.config_hyperparam_exp_dir = None   # .
        self.info_hyperparam_exp_dir = None   # .
        self.transitions_hyperparam_exp_dir = None # this is where episodic transitions are stored

        if not os.path.exists(experiment_dir):
            self.create_dir(experiment_dir, with_deletion=False)
        
        # ------ Create directory for current experiment -------
        self.this_exp_dir = os.path.join(self.exp_dir, experiment_name)
        if experiment_name == "test":
            self.remove_dir(self.this_exp_dir) # if experiment directory already exists, delete and recreate
        self.create_dir(self.this_exp_dir)

        self.transitions_log_dir = os.path.join(self.this_exp_dir, "transitions")
        self.create_dir(self.transitions_log_dir)

        self.csv_data_dir = os.path.join(self.this_exp_dir, "csv_data")
        self.log_dir = os.path.join(self.this_exp_dir, "log")
        self.model_dir = os.path.join(self.this_exp_dir, "model")
        self.config_dir = os.path.join(self.this_exp_dir, "config")
        self.info_dir = os.path.join(self.this_exp_dir, "info")
        
        self.create_dir(self.csv_data_dir)
        self.create_dir(self.log_dir)
        self.create_dir(self.model_dir)
        self.create_dir(self.config_dir)
        self.create_dir(self.info_dir)
    
        
    def create_hyperparam_exp(self,hyperparam_str):
        '''
        hyperparam_str can be a string that contains the hyperparams that require tuning, or any string that the user defines
        '''
        self.csv_data_hyperparam_exp_dir = os.path.join(self.csv_data_dir, hyperparam_str)
        self.log_hyperparam_exp_dir = os.path.join(self.log_dir, hyperparam_str)
        self.model_hyperparam_exp_dir = os.path.join(self.model_dir, hyperparam_str)
        self.config_hyperparam_exp_dir = os.path.join(self.config_dir, hyperparam_str)
        self.info_hyperparam_exp_dir = os.path.join(self.info_dir, hyperparam_str)
        self.transitions_hyperparam_exp_dir = os.path.join(self.transitions_log_dir, hyperparam_str)
        
        
        self.create_dir(self.csv_data_hyperparam_exp_dir)
        self.create_dir(self.log_hyperparam_exp_dir)
        self.create_dir(self.model_hyperparam_exp_dir)
        self.create_dir(self.config_hyperparam_exp_dir)
        self.create_dir(self.transitions_hyperparam_exp_dir)
        self.create_dir(self.info_hyperparam_exp_dir)
        
    def create_dir(self, directory, with_deletion=True):
        if with_deletion:
            if not os.path.exists(directory):
                os.makedirs(directory)
            else:
                self.remove_dir(directory)
                os.makedirs(directory)
        else:
            os.makedirs(directory)
                
    def remove_dir(self, directory):
        if os.path.isdir(directory):
            print("Path exists, removing")
            shutil.rmtree(directory)


Configuration = ExperimentDirs