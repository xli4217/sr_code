import scipy.signal
from future.utils import viewitems
import numpy as np

class Evaluation(object):

    def __init__(self, config):
        self.config = config
        self.gamma = config['gamma'] # discount factor

    def discount(self, x, gamma):
        """ Calculate discounted forward sum of a sequence at each point """
        return scipy.signal.lfilter([1.0], [1.0, -gamma], x[::-1])[::-1]
 
        
    def get_diagnostics(self, mode, batch):
        '''
        mode can be "training" or "evaluation". if "evaluation", batch is collected without added noise to the policy 
        if metric is float array, log mean and std, if metric is boolean array, log True rate 

        logs stat for: discounted_return, undiscounted_return, episode_length, done_rate, info, selected dimensions for observation and action as specified in config
        '''
        R = [] # undiscounted_return
        discounted_R = [] # discounted_return
        episode_length = []
        done = [] # statistics of where done=True in the traj
        info_stat = {}
        state_stats = None
        action_states = None
        
        log_dict = {}
        
        for traj in batch:
            R.append(np.sum(traj['Rewards']))
            discounted_R.append(self.discount(traj['Rewards'].flatten(), self.gamma)[0])
            episode_length.append(traj['Rewards'].size+1)
            if any(traj['Done']):
                done.append(1)
            else:
                done.append(0)

            
            if len(self.config['log_state_dimensions']) != 0:
                obs = np.vstack([traj['Observations'], traj['Terminal_state']])
                if not state_stats:
                    state_stats = obs
                else:
                    state_stats = np.vstack([state_stats, obs])

            if len(self.config['log_action_dimensions']) != 0:
                act = traj['Actions']
                if not action_stats:
                    action_stats = act
                else:
                    action_stats = np.vstack([action_stats, act])
       
            for key, value in viewitems(traj['Info']):
                if key in info_stat.keys():
                    info_stat[key] = np.vstack([info_stat[key], np.array(value)])
                else:
                    info_stat[key] = np.array(value)
                    
                
        R = np.array(R)
        discounted_R = np.array(discounted_R)
        episode_length = np.array(episode_length)
        done = np.array(done)

        for key in self.config['log_info_keys']:
            value = info_stat[key]
            value = np.array(value).flatten()
            if value.dtype == np.bool:
                log_dict[mode+"_"+key+"_rate"] = value[value==True].size / value.size
            else:
                log_dict[mode+"_"+key+"_mean"] = np.mean(value)
                log_dict[mode+"_"+key+"_std"] = np.std(value)

        for idx in self.config['log_state_dimensions']:
            log_dict[mode+"_state_dim"+str(idx)+"_mean"] = np.mean(state_stats[:,idx])
            log_dict[mode+"_state_dim"+str(idx)+"_std"] = np.std(state_stats[:,idx])

        for idx in self.config['log_action_dimensions']:
            log_dict[mode+"_action_dim"+str(idx)+"_mean"] = np.mean(action_stats[:,idx])
            log_dict[mode+"_action_dim"+str(idx)+"_std"] = np.std(action_stats[:,idx])
      
            
        log_dict[mode+"_undiscounted_return_mean"] = np.mean(R)
        log_dict[mode+"_undiscounted_return_std"] = np.std(R)
        log_dict[mode+"_discounted_return_mean"] = np.mean(discounted_R)
        log_dict[mode+"_discounted_return_std"] = np.std(discounted_R)
        log_dict[mode+"_episode_length_mean"] = np.mean(episode_length)
        log_dict[mode+"_episode_length_std"] = np.std(episode_length)
        log_dict[mode+"_done_rate"] = done[done==True].size / len(batch)

        return log_dict