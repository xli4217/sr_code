from collections import deque
import numpy as np
import random
from .segment_tree import SumSegmentTree, MinSegmentTree
from rl_pipeline.schedule.schedule import LinearSchedule
from future.utils import viewitems
from tqdm import tqdm
import cloudpickle
import os

default_config = {
    # demo_data = {"States": <states>, "Actions": <actions>}
    'demo_data': None, 
    'capacity': 1e6,
    'seed': 0,
    'shuffle': True,
    'per_alpha': 0.5, 
    'beta0': 0.4, # To what degree to use importance weights, (0 - no corrections, 1 - full correction)
    'epsilon': 1e-6,
    'prioritized_replay_beta_itrs': 1e5,
    # can be a string to a path or a list of strings to paths
    'load_replay_buffer_path_list': [] 
}


class ReplayBufferBase(object):
    def __init__(self, config={}, seed=None):
        self.config = default_config
        self.config.update(config)

        self.capacity = int(self.config['capacity'])
        self.buffer = deque(maxlen=self.capacity)

        #### set seed ####
        if not seed:
            self.seed = self.config['seed']
        else:
            self.seed = seed
        random.seed(self.seed)
        np.random.seed(self.seed)
        
        self.shuffle = self.config['shuffle']
        self.alpha = self.config['per_alpha'] # 0 - no prioritization, 1 - full prioritization
        
        if self.alpha > 0:
            self.next_idx = 0
            it_capacity = 1
            while it_capacity < self.capacity:
                it_capacity *= 2
            self.it_sum = SumSegmentTree(it_capacity)
            self.it_min = MinSegmentTree(it_capacity)
            self.max_priority = 1.0
            self.beta_schedule = LinearSchedule(self.config['prioritized_replay_beta_itrs'], initial_p=self.config['beta0'], final_p=1.0)

        if self.config['demo_data'] is not None:
            print("loading demo data")
            self.demo_buffer = deque()
            demo_states = self.config['demo_data']['States']
            demo_actions = self.config['demo_data']['Actions']
            for i in tqdm(range(demo_states.shape[0])):
                state = np.expand_dims(demo_states[i,:], 0)
                action = np.expand_dims(demo_actions[i,:], 0)
                self.demo_buffer.append((state, action))
        else:
            self.demo_buffer = None

        # restore saved buffer(s)
        self.restore(restore_dir_list=self.config['load_replay_buffer_path_list'])
            
    def restore(self, restore_dir_list=[]):
        for restore_dir in restore_dir_list:
            p = os.path.join(restore_dir, 'replay_buffer.pkl')
            load_buffer = cloudpickle.loads(open(p, 'rb').read())
            self.buffer.extend(load_buffer)
    
                
    def size(self):
        return len(self.buffer)
            
    def insert_sample(self, state, action, next_state, reward, done):
        if self.alpha > 0:
            idx = self.next_idx
        
        state = np.expand_dims(state, 0)
        next_state = np.expand_dims(next_state, 0)
        action = np.expand_dims(action, 0)
        reward = np.expand_dims(reward, 0)
        self.buffer.append((state, action, next_state, reward, done))
        
        if self.alpha > 0:
            self.next_idx = (self.next_idx + 1) % self.capacity
            self.it_sum[idx] = self.max_priority ** self.alpha
            self.it_min[idx] = self.max_priority ** self.alpha
            
    def sample_proportional(self, batch_size):
        res = []
        for _ in range(batch_size):
            mass = random.random() * self.it_sum.sum(0, len(self.buffer) - 1)
            idx = self.it_sum.find_prefixsum_idx(mass)
            res.append(idx)
        return res

    def insert_batch(self, batch):
        '''
        batch is a list of dictionaries, each with keys "Observations", "Actions", "Rewards", "Done", "Terminal_state"
        '''
        for traj in batch:
            terminal_idx = traj['Observations'].shape[0]        
            for i in range(terminal_idx - 1):
                state = traj['Observations'][i,:]
                action = traj['Actions'][i,:]
                next_state = traj['Observations'][i+1,:]
                reward = traj['Rewards'][i, 0]
                done = traj['Done'][i, 0]

                self.insert_sample(state, action, next_state, reward, done)

            # insert terminal sample
            self.insert_sample(traj["Observations"][terminal_idx-1,:],
                               traj["Actions"][terminal_idx-1,:],
                               traj["Terminal_state"],
                               traj["Rewards"][terminal_idx-1, 0],
                               traj['Done'][terminal_idx-1, 0])
            
    def encode_sample(self, idxes):
        states, actions, next_states, rewards, dones = [], [], [], [], []
        for i in idxes:
            state, action, next_state, reward, done = self.buffer[i]
            states.append(state)
            actions.append(action)
            next_states.append(next_state)
            rewards.append(reward)
            dones.append([done])

        return np.vstack(states), np.vstack(actions), np.vstack(next_states), np.array(rewards), np.array(dones).astype(np.float32)

    def sample_demo(self, batch_size):
        if self.demo_buffer is None:
            raise ValueError("demo buffer is empty")
        demo_batch = random.sample(self.demo_buffer, batch_size)
        if self.shuffle:
            random.shuffle(demo_batch)
        states, actions = zip(*demo_batch)

        #print("{} samples from demo buffer".format(batch_size))
        
        return np.concatenate(states), np.concatenate(actions)
        
            
    def sample(self, batch_size, timestep=None):
        if self.alpha == 0:
            #state, action, next_state, reward, done = zip(*random.sample(self.buffer, batch_size))
            batch = random.sample(self.buffer, batch_size)
            if self.shuffle:
                random.shuffle(batch)
            state, action, next_state, reward, done = zip(*batch)
            weights = np.ones(np.concatenate(reward).shape)

            # print("{} samples from buffer".format(batch_size))
            return np.concatenate(state), np.concatenate(action), np.concatenate(next_state), np.concatenate(reward), np.array(done).astype(np.float32), weights, -1 * np.ones(np.concatenate(reward).shape) 
        else:
            beta = self.beta_schedule.value(timestep)
            idxes = self.sample_proportional(batch_size)
            weights = []
            p_min = self.it_min.min() / self.it_sum.sum()
            max_weight = (p_min * len(self.buffer)) ** (-beta)

            for idx in idxes:
                p_sample = self.it_sum[idx] / self.it_sum.sum()
                weight = (p_sample * len(self.buffer)) ** (-beta)
                weights.append(weight / max_weight)

            weights = np.array(weights)
            state, action, next_state, reward, done = self.encode_sample(idxes)

            # print("{} samples from buffer".format(batch_size))
            return state, action, next_state, reward, done, weights, idxes

    def update_priorities(self, idxes, priorities):
        """Update priorities of sampled transitions.

        sets priority of transition at index idxes[i] in buffer
        to priorities[i].

        Parameters
        ----------
        idxes: [int]
            List of idxes of sampled transitions
        priorities: [float]
            List of updated priorities corresponding to
            transitions at the sampled idxes denoted by
            variable `idxes`.
        """
        assert len(idxes) == len(priorities)
        priorities = np.abs(priorities) + self.config['epsilon']
        for idx, priority in zip(idxes, priorities):
            assert priority > 0
            assert 0 <= idx < len(self.buffer)
            self.it_sum[idx] = priority ** self.alpha
            self.it_min[idx] = priority ** self.alpha

            self.max_priority = max(self.max_priority, priority)


if __name__ == "__main__":
    config = {'capacity': 5, 'seed':10, 'shuffle': True, 'alpha':1}
    buffer = ReplayBufferBase(config)

    traj = {"Observations": np.random.rand(4,2), "Actions": np.random.rand(4,1), "Rewards": np.random.rand(4,1), "Done": np.random.rand(4,1), "Terminal_state": np.random.rand(2)}

    batch = [traj]
    print("111111111111111")
    print(batch)

    buffer.insert_batch(batch)
    print("2222222222222222222")
    print(buffer.buffer)

    print("3333333333333333333333")
    print(buffer.sample(2))

    