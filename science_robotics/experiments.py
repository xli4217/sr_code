import os
import numpy as np
import glob
from tqdm import tqdm
import cloudpickle
import fire

default_config = {}

class RunExperiment(object):

    def __init__(self, config={}):
        self.config = default_config
        self.config.update(config)    

    def teleop(self):
        env_type = self.config['Learner']['config']['ExperimentRunner']['config']['BaseRunner']['config']['Environment']['type']
        env_config = self.config['Learner']['config']['ExperimentRunner']['config']['BaseRunner']['config']['Environment']['config']
        env = env_type(env_config)
        env.reset()
        prev_state =  env.get_state(env.all_info)
        R = 0
        while True:
            cmd = input('insert cmd: ')
            env.teleop(cmd)
            curr_state = env.get_state(env.all_info)
            done = env.is_done(next_state=curr_state, all_info=env.all_info)
            r = env.get_reward(state=prev_state, action=np.zeros(env.action_space['shape'][0]), next_state=curr_state, all_info=env.all_info)
            print("state: ", curr_state)
            print("reward: ", r)
            print("done: ", done)
            R += r
            if done:
                print('done')
                print("return: ", R)
                R = 0
                # env.reset()
            prev_state = curr_state
            
    def learn(self):
        '''
        Starts learning
        '''
        learner_config = self.config['Learner']
        learner= learner_config['type'](learner_config['config'])
        learner.run_one_experiment()

    def deploy(self):
        deploy_config = self.config['Deployer']['config']

        experiment_root_dir = self.config['experiment_root_dir']
        itr_dir = os.path.join(experiment_root_dir,
                               'experiments',
                               deploy_config['deploy_experiment_name'],
                               'transitions',
                               deploy_config['deploy_hyperparam_dir'],
                               'itr_'+str(deploy_config['deploy_itr']))

        base_runner_config_path = os.path.join(experiment_root_dir,
                                   'experiments',
                                   deploy_config['deploy_experiment_name'],
                                   'config',
                                   deploy_config['deploy_hyperparam_dir'],
                                   'config.pkl')

    
        # #### load config ####
        base_runner_config = cloudpickle.loads(open(base_runner_config_path, 'rb').read())

        # #### construct env from config ####
        if deploy_config['deploy_env_from_config']:
        #### construct environment ####
            env_config = base_runner_config.get(['Environment', 'config'])
            env_config['headless'] = deploy_config['deploy_headless']
            if 'fsa_save_dir' in env_config.keys():
                env_config['fsa_save_dir'] = self.config['Learner']['config']['ExperimentRunner']['config']['BaseRunner']['config']['Environment']['config']['fsa_save_dir']
            env = base_runner_config.get(['Environment', 'type'])(env_config)
        else:
            #### construct fresh environment (useful if want to change param) ####
            env_type = self.config['Learner']['config']['ExperimentRunner']['config']['BaseRunner']['config']['Environment']['type']
            env_config = self.config['Learner']['config']['ExperimentRunner']['config']['BaseRunner']['config']['Environment']['config']
            
            env = env_type(env_config,
                              seed=0,
                              port_num=None,
                              suffix="")
                
            
            
        #### load exploration ####
        exploration_config = base_runner_config.get(['Exploration', 'config'])
        if base_runner_config.get(['Exploration', 'type']) is not None:
            explore = base_runner_config.get(['Exploration', 'type'])(exploration_config, action_space=env.action_space) 
        else:
            explore = None

        #### load state preprocessor ####
        state_preprocessor_config = base_runner_config.get(['Preprocessors', 'state_preprocessor', 'config'])
        if base_runner_config.get(['Preprocessors', 'state_preprocessor', 'type']) is not None:
            state_preprocessor = base_runner_config.get(['Preprocessors', 'state_preprocessor', 'type'])(state_preprocessor_config)
            state_preprocessor_load_path = os.path.join(experiment_root_dir,
                                                   'experiments',
                                                   deploy_config['deploy_experiment_name'],
                                                   'info',
                                                   deploy_config['deploy_hyperparam_dir'],
                                                   'state_preprocessor_params.pkl')
            state_preprocessor.restore_preprocessor(state_preprocessor_load_path)
        else:
            state_preprocessor = None

        # #### load policy ####
        policy_config = base_runner_config.get(['Actor', 'config'])
        policy_config['state_space'] = env.state_space
        policy_config['action_space'] = env.action_space
        policy = base_runner_config.get(['Actor', 'type'])(policy_config, exploration_strategy=explore)
        policy.restore(os.path.join(itr_dir, 'policy', 'policy'))
        print("Loaded policy from {}".format(os.path.join(itr_dir, 'policy')))
        
        # #### run #### 
        return_list = []
        for _ in range(deploy_config['deploy_nb_trial_runs']):
            env.reset(randomize_q=False)
            print('reset!')
            R = 0
            obs_prev = None
            action_prev = None
            for i in range(deploy_config['deploy_max_episode_timesteps']):
                env_info = env.get_info()
                obs = env.get_state(env_info)
                if state_preprocessor is not None:
                    n_obs = state_preprocessor.get_scaled_x(obs)
                else:
                    n_obs = obs
                if env.is_done(next_state=obs, all_info=env_info):
                    print("done")
                    break
                action = policy.get_action(n_obs, deterministic=True)
                env.step(action)
                if i > 0:
                    R += env.get_reward(obs_prev, action_prev, obs, env_info)
                obs_prev = obs
                action_prev = env.action_dict['actions']
            print("episode return: {}".format(R))
            return_list.append(R)
    
        print("average return: {}".format(np.mean(np.array(return_list))))
        print("return std: {}".format(np.std(np.array(return_list))))
        
    def run(self):
        '''
        Function to run 
        '''
        if self.config['mode'] == 'train' or self.config['mode'] == 'tune_hyperparam':
            self.learn()
        elif self.config['mode'] == 'deploy':
            self.deploy()
        elif self.config['mode'] == 'teleop':
            self.teleop()
        else:
            raise ValueError('unsupported mode')
        
        

if __name__ == "__main__":
    pass
    # from training_config import construct_task_agent_config
    # experiment_config = fire.Fire(construct_task_agent_config)
    # cls = RunExperiment(experiment_config)
    # cls.run()
