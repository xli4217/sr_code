import os
import numpy as np
import glob
from tqdm import tqdm
import copy
import json

from rl_pipeline.replay_buffer.replay_buffer_base import ReplayBufferBase
from rl_pipeline.exploration.epsilon_linear_decay import EpsilonLinearDecay
from rl_pipeline.sampler.batch_sampler import BatchSampler
from rl_pipeline.train.rl.sync_runner import SynchronousRunner
from rl_pipeline.evaluation.evaluation import Evaluation
from rl_pipeline.preprocessor.running_average import RunningAverage
from rl_pipeline.hyperparameter_tuner.random_search import RandomSearch
from rl_pipeline.hyperparameter_tuner.skopt_tuner import SkoptTuner
from rl_pipeline.algo_devel.agent_library import AgentLibrary
from rl_pipeline.logging.tensorboardX_logger import TensorboardXLogger
from rl_pipeline.preprocessor.running_average import RunningAverage

#### config ####
from sacred import Experiment, SETTINGS
from sacred.observers import FileStorageObserver
from fire import Fire


SETTINGS['CONFIG']['READ_ONLY_CONFIG']= False

ex = Experiment('test')

default_args = {
    'experiment_root_dir': os.path.join(os.environ['RLFPS_PATH'], 'examples', 'science_robotics'),
    'exp_name': 'test',
    # this can be 'train', 'tune_hyperparam', 'deploy', 'teleop'
    'mode': 'train',
    'sequential_or_concurrent': 'sequential',
    'concurrency_os': 'ros',
    # this can be 'sim' or 'real', or 'sim_headless'
    'sim_or_real': 'sim',
    'seed': 0,
    'task_agent_log': False,
    'gpu': False,
    'json_config_file': None,
    'cmd_log': None,
    #### Logger ####
    'log_model_interval': 10,
    'log_transitions_interval': 100,
    ##########
    # Teleop #
    ##########
    'teleop_reasoner': False,
    'teleop_planner': False,
    'teleop_controller': False,
    'teleop_manipulator': False,
    ##########
    # Deploy #
    ##########
    'deploy_experiment_name': None,
    'deploy_hyperparam_dir': None,
    'deploy_itr': None,
    'deploy_nb_trial_runs': 10,
    'deploy_sampler_traj': True,
    'deploy_env_from_config': True,
    'deploy_log_episode': False,
    'deploy_log_batch': False,
    'deploy_max_episode_timesteps': 50,
    'deploy_headless': False,
    #########
    # Train #
    #########
    #### rl_config ####
    'max_itr': 1000,
    # -- loading for restore training --
    'load_actor_path': None,
    'load_critic_path': None,
    'load_state_preprocessor_path': None,
    'load_reward_preprocessor_path': None,
    'load_replay_buffer_path_list': None,
    #### rl_config -> run_one_experiment_config ####
    #### run_one_experiment_config -> synchoronous runner config ####
    'nb_remote_runners': 0,
    'ray': False,
    #### environment config ####
    'synchronous': True,
    # -- this can be 'gotored' (vrep), 'gym_Reacher-v2' (more general 'gym_<env_id>'), 'mujoco_snake', 'mujoco_reacher'
    # -- or all the above prefixed with 'tltask<task#>_'
    'task': 'gotored',
    'dist_th': 0.05,
    'fsa_save_dir': os.path.join(os.environ['RLFPS_PATH'], 'examples', 'science_robotics', 'figures'),
    'fsa_name': 'fsa',
    'headless': False,
    'frameskip':1,
    'scene_file': os.path.join(os.environ['RLFPS_PATH'], 'examples', 'science_robotics', 'env', 'vrep_env', 'ttt', 'particle_test_env.ttt'),
    ##### replay buffer config ####
    'per_alpha': 0.,
    'per_beta0': 0.4,
    #### agent_config ####
    'rl_agent': 'ppo',
    # -- common
    'actor_lr': 0.005,
    'critic_lr': 0.005,
    'minibatch_size':64,
    'nb_optimization_steps_per_itr':10,
    'reset_config':{},
    'gamma':0.99,
    'no_update_until':5000,
    'save_replay_buffer': False,
    'log_components':{
        'graph': False,
        'text': False,
        'scaled_state_dist': False,
        'unscaled_state_dist': False,
        'action_dist': False,
        'scaled_reward_dist': False,
        'unscaled_reward_dist': False,
        'done_dist': False,
        'adv_dist': False,
        'actor_gradient_dist': False,
        'critic_gradient_dist':False,
        'actor_weights': False,
        'critic_weights': False,
        'info_dict_scalar': [],
        'info_dict_dist': [],
        'info_dict_text': [],
        "info_dict_hparam": {
            "hparam_dict_keys": [],
            "metric_dict_keys": []
        }
    },
    # -- ddpg configs
    'target_q_update_interval':1,
    'soft_target_update':0.005,
    # --- td3 configs
    'td3': False,
    'policy_frequency': 2,
    'policy_noise': 0.2,
    'noise_clip': 0.5,
    # --- composition configs
    'update_critic': True,
    # -- ppo configs
    'policy_lr': 3e-4,
    'minibatch_size':64,
    'nb_training_epoches': 1,
    'entropy_coef': 0.01,
    'value_loss_coef': 0.5,
    'lam': 0,
    'kl_target': 0.04,
    #### Reset config ####
    #### Preprocessor config ####
    'state_preprocessor': None,
    'reward_preprocessor': None,
    #### Exploration config ####
    'exploration_type': "epsilon_decay",
    # --- epsilon decay configs
    'total_decay_steps': 'auto',
    'initial_epsilon': 1.,
    'final_epsilon': 0.1,
    'steps_of_uniform_random':5000, 
    #### Actor config ####
    #### Critic config ####
    #### ReplyBuffer config ####
    #### sampler config ####
    'rollout_batch_size': 5,
    'max_episode_timesteps':100,
    'use_preprocessors': True,
    'update_preprocessors': True,
    'save_preprocessors': True,
    'process_rewards': None,
    'log_episode': False,
    'log_batch': False,
    #### HyperparamterTuner ####
    'HyperparameterTuner':{
        'type': 'skopt',
        'config':{
            "performance_metric": "average_return",
            "nb_steps": 100,
            "params_dict":{
                #### Common ####
                'actor_lr': {"type": "float", "range": [1e-3, 5e-2], "prior": "log-uniform", "transform": "identity"},
                'critic_lr': {"type": "float", "range": [1e-3, 5e-2], "prior": "log-uniform", "transform": "identity"},                                                                                                                        
                "minibatch_size": {"type": "integer", "range": [64, 128], "prior": "uniform", "transform": "identity"},                                                                                                                         
                "nb_optimization_steps_per_itr": {"type": "integer", "range": [10, 50], "prior": "uniform", "transform": "identity"},                                                                                                                
                'soft_target_update': {"type": "float", "range": [0.01, 0.05], "prior": "uniform", "transform": "identity"},
                # 'per_alpha': {"type": "float", "range": [0., 0.6], "prior": "uniform", "transform": "identity"},
                #### PPO agent ####
                "nb_training_epoches": {"type": "integer", "range": [5, 15], "prior": "uniform", "transform": "identity"},                                                                                                                     
                'value_loss_coef': {"type": "float", "range": [0.4, 0.6], "prior": "uniform", "transform": "identity"},                                                                                                                       
                'entropy_coef': {"type": "float", "range": [0.01, 0.1], "prior": "uniform", "transform": "identity"},                                                                                                                           
                'lam': {"type": "float", "range": [0., 0.2], "prior": "uniform", "transform": "identity"}     
                    
            },
            # this is optimizer specific arguments
            "optimizer": {
                "base_estimator": "GP",
                "n_initial_points": 1
            }
        }
    }
}

#### add config ####
ex.add_config(default_args)

@ex.capture
def construct_task_agent_config(_config):
    a = dict(_config)
    if a['json_config_file'] is not None:
        b = json.loads(open(a['json_config_file'], 'r').read())
        a.update(b)
        
    ##########
    # Common #
    ##########
    #### Logger ####
    Logger = {
        'type': TensorboardXLogger,
        'config': {
            "csv_data_params": {},
            "log_params": {},
            "model_params": {
                "interval": a['log_model_interval'] # save model every 10 iterations
            },
            "transitions_params":{
                "interval": a['log_transitions_interval'], # record trajecory batch and the computation graph every this many iterations,
            }
        }
    }

        
    ###########
    # Learner #
    ###########

    #### Reinforcement Learner ####
    
    # ---- Learning Agent, Exploration, Actor, Critic ----
    agent_library = AgentLibrary()
    
    common_agent_config  = {
        'actor_lr': a['actor_lr'],
        'critic_lr': a['critic_lr'],
        'seed': a['seed'],
        'gamma': a['gamma'],
        'minibatch_size': a['minibatch_size'],
        'nb_optimization_steps_per_itr': a['nb_optimization_steps_per_itr'],
        'target_q_update_interval': a['target_q_update_interval'],
        'no_update_until': a['no_update_until'],
        'save_replay_buffer': a['save_replay_buffer'],
        'log_components':a['log_components'],
        'gpu': a['gpu']
    }

    if a['total_decay_steps'] == 'auto':
        total_decay_steps = a['max_itr'] * a['max_episode_timesteps'] * a['rollout_batch_size']
    else:
        total_decay_steps = a['total_decay_steps'] 
    
    if a['rl_agent'] == "ddpg":
        from rl_pipeline.algo_devel.ddpg.pytorch.ddpg_runner import DDPGRunner
        base_runner = DDPGRunner
  
        ddpg_agent_config = copy.copy(common_agent_config)
        ddpg_agent_config.update(update_critic=a['update_critic'],
                                 soft_target_update=a['soft_target_update'],
                                 td3=a['td3'],
                                 policy_frequency=a['policy_frequency'],
                                 policy_noise=a['policy_noise'],
                                 noise_clip=a['noise_clip'],
                                 exploration_type=a['exploration_type'],
                                 total_decay_steps=total_decay_steps,
                                 initial_epsilon=a['initial_epsilon'],
                                 final_epsilon=a['final_epsilon'],
                                 steps_of_uniform_random=a['steps_of_uniform_random'])
        
        Exploration, Actor, Critic, Agent = agent_library.get_agent('ddpg', ddpg_agent_config)
        
    elif a['rl_agent'] == 'ppo':
        from rl_pipeline.algo_devel.ppo.pytorch.ppo_runner import PPORunner
        base_runner = PPORunner

        ppo_agent_config = copy.copy(common_agent_config)
        ppo_agent_config.update(policy_lr=a['policy_lr'],
                                minibatch_size=a['minibatch_size'],
                                nb_training_epoches=a['nb_training_epoches'],
                                value_loss_coef=a['value_loss_coef'],
                                entropy_coef=a['entropy_coef'],
                                lam=a['lam'],
                                kl_target=a['kl_target'])
        
        Exploration, Actor, Critic, Agent = agent_library.get_agent('ppo', ppo_agent_config)

    else:
        raise ValueError('unsupported agent')
 
    Actor['config']['load_path'] = a['load_actor_path']
    Critic['config']['load_path'] = a['load_critic_path']
    
    # ---- Sampler ----
    Sampler = {
        'type': BatchSampler,
        'config': {
            'rollout_batch_size': a['rollout_batch_size'], # in units of episodes (if ray is used, this is for each agent)
            "max_episode_timesteps": a['max_episode_timesteps'],  
            "init_batch_size": 0, # run a few episode of untrained policy to initialize scaler
            'use_preprocessors': a['use_preprocessors'],
            'update_preprocessors': a['update_preprocessors'], # this might be false if multiple runners are used to collect sample, their preprocessors might need to be updated with all experiences
            'save_preprocessors': a['save_preprocessors'],
            'log_info_keys': [],
            'process_rewards': a['process_rewards'],
            'log_episode': a['log_episode'],
            'log_batch': a['log_batch']
        }
    }
    
    # ---- ReplayBuffer ----
    ReplayBuffer = {
        'type': ReplayBufferBase,
        'config': {
            'load_path': a['load_replay_buffer_path_list'],
            # demo_data = {"States": <states>, "Actions": <actions>}
            'demo_data': None, 
            'capacity': 1e6,
            'seed': a['seed'],
            'shuffle': True,
            'per_alpha': a['per_alpha'], # 0 is no PER 
            'beta0': a['per_beta0'], # To what degree to use importance weights, (0 - no corrections, 1 - full correction)
            'epsilon': 1e-6,
            'prioritized_replay_beta_itrs': 1e5,
        }
    }
    

    # ---- Preprocessors ----
    if a['state_preprocessor'] == "RunningAverage":
        state_preprocessor = {"type": RunningAverage, 'config':{"dim": None, "shift": True, "scale": True, "load_path": a['load_state_preprocessor_path']}}
    elif a['state_preprocessor'] is None:
        state_preprocessor = {'type': None, 'config':{}}
    else:
        raise ValueError('invalid state preprocessor')

    if a['reward_preprocessor'] is None:
        reward_preprocessor = {'type': None, 'config':{}}
    elif a['reward_preprocessor'] == 'RunningAverage':
        reward_preprocessor =  {"type": RunningAverage, 'config':{"dim": 1, "shift": False, "scale": True, "load_path": a['load_reward_preprocessor_path']}}
        
    Preprocessors = {
        "state_preprocessor": state_preprocessor,
        "reward_preprocessor": reward_preprocessor
    }
    
    # --- reset ----
    Reset = {
        'type': None,
        'config': {}
    }

    # --- environment ---
    from examples.science_robotics.task import Task
    task_config = {
        'synchronous': a['synchronous'],
        'task': a['task'],
        'seed': a['seed'],
        'dist_th': a['dist_th'],
        'fsa_save_dir': a['fsa_save_dir'],
        'fsa_name': a['fsa_name'],
        'headless': a['headless'],
        'frameskip': a['frameskip'],
        'scene_file': a['scene_file'],
        'nb_remote_runners': a['nb_remote_runners']
    }
    task = Task(config=task_config)
    Environment = task.Environment
        
    # ---- base runner -----
    BaseRunner = {
        'type': base_runner,
        'config': {
            'seed': a['seed'],
            'cmd_log': a['cmd_log'],
            'state_space': task.state_space,
            'action_space': task.action_space,
            'Logger': Logger,
            'Reset': Reset,
            'Environment': Environment,
            'env_constructed': False,
            'ReplayBuffer': ReplayBuffer,
            "Preprocessors":Preprocessors,
            'Exploration':Exploration,
            'Actor':Actor,
            'Critic': Critic,
            'Agent':Agent,
            'Sampler': Sampler
        }
    }
    
    # ---- evaluator -----
    Evaluator = {
        'type': Evaluation,
        'config': {
            'gamma': 0.99,
            'log_state_dimensions': [],
            'log_action_dimensions': [],
            'log_info_keys': []
        }
    }

    if a['mode'] != 'tune_hyperparam':
        HyperparamterTuner = None
    else:
        HyperparamterTuner = a['HyperparameterTuner']
        if HyperparamterTuner['type'] == 'skopt':
            HyperparamterTuner['type'] = SkoptTuner
            HyperparamterTuner['config']['optimizer']['random_state'] = a['seed']
            
    from rl_pipeline.train.rl.run_one_experiment import RunOneExperiment
    ReinforcementLearner = {
        'type': RunOneExperiment,
        'config': {
            'exp_root_dir': a['experiment_root_dir'],
            'exp_name': a['exp_name'],
            'seed': a['seed'],
            'nb_random_seed_exp': 1,
            'ExperimentRunner':{
                'type': SynchronousRunner,
                'config': {
                    'BaseRunner':BaseRunner,
                    'Evaluator': Evaluator,
                    'seed': a['seed'],
                    'update_parameters': True,
                    'nb_remote_runners': a['nb_remote_runners'],
                    'ray': a['ray'],
                    # this should have length a['nb_remote_runners']+1, last one is for local runner
                    'ports': [19998, 19999, 20000, 20001, 20002, 20003], 
                    'max_itr': a['max_itr'],
                    'evaluation_logging': False,
                    'evaluation_batch_size': 0,
                    'evaluation_logging_interval': 0,
                }
            },
            'HyperparameterTuner': HyperparamterTuner
        }
    }


    ############
    # Deployer #
    ############
    Deployer = {
        'type': None,
        'config':{
            'deploy_experiment_name': a['deploy_experiment_name'],
            'deploy_hyperparam_dir': a['deploy_hyperparam_dir'],
            'deploy_itr': a['deploy_itr'],
            'deploy_nb_trial_runs': a['deploy_nb_trial_runs'],
            'deploy_sampler_traj': a['deploy_sampler_traj'],
            'deploy_env_from_config': a['deploy_env_from_config'],
            'deploy_log_episode': a['deploy_log_episode'],
            'deploy_log_batch': a['deploy_log_batch'],
            'deploy_max_episode_timesteps': a['deploy_max_episode_timesteps'],
            'deploy_headless': a['deploy_headless']
        }
    }

    ##########
    # Teleop #
    ##########
    Teleop = {
        'type': None,
        'config': {
            'reasoner': a['teleop_reasoner'],
            'planner': a['teleop_planner'],
            'controller': a['teleop_controller'],
            'manipulator': a['teleop_manipulator'],
        }
    }
    
    #####################
    # experiment_config #
    #####################
    task_agent_config = {
        'log': a['task_agent_log'],
        # experiment_root_dir: root of the experiment folder where training/evaluation results are saved 
        'experiment_root_dir': a['experiment_root_dir'],
        # exp_name="test": current experiment name
        'exp_name': a['exp_name'],
        # this can be 'train', 'tune_hyperparam', or 'deploy' or 'teleop'
        'mode': a['mode'],
        # this can be 'sim' or 'sim_headless' or 'real'
        'sim_or_real': a['sim_or_real'],
        'seed': a['seed'],
        'Logger': Logger, 
        'Learner': ReinforcementLearner,
        'Deployer': Deployer,
        'Teleop': Teleop
    }
    
    return task_agent_config

def get_config_file(config_file=None):
    return config_file

@ex.automain
def run(_config):
    a = _config
    config = construct_task_agent_config()
    from examples.science_robotics.experiments import RunExperiment
    cls = RunExperiment(config)
    cls.run()