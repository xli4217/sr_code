import numpy as np
from future.utils import viewitems
import os
from rl_pipeline.configuration.configuration import Configuration

import json

default_config = {
    'synchronous': True,
    'seed': 0,
    'task': 'gotored',
    'dist_th': 0.05,
    'fsa_save_dir': os.getcwd(),
    'fsa_name': 'fsa',
    'softmax': False,
    'beta': 5,
    'headless': False,
    'frameskip': 1,
    'scene_file': "",
    'nb_remote_runners': 0
}


class Task(object):

    def __init__(self, config={}):
        self.Task_config = Configuration(default_config)
        self.Task_config.update(config)

        self.task = self.Task_config.get('task')
        self.task_list = self.task.split('_')
        self.dist_th = self.Task_config.get('dist_th')

        self.set_seed()
        
        self.construct_env()

        self.Environment = {
            'type': self.env_type,
            'config': self.env_config
        }

    def set_seed(self, seed=None):
        if seed is None:
            seed = self.Task_config.get('seed')
        np.random.seed(seed)

    def construct_env(self):
        #   MDP Env  #
        #### vrep ####
        if 'vrep' in self.task_list or 'pyrep' in self.task_list:
            mdp_env_type, mdp_env_config, mdp_state_space, mdp_action_space, other = self.construct_vrep_env()
        #### mujoco ####
        if 'mujoco' in self.task_list:
            mdp_env_type, mdp_env_config, mdp_state_space, mdp_action_space, other = self.construct_mujoco_env()


        #    TL Env    #
        #### tl augmented mdp ####
        if 'tl' not in self.task_list:
            self.env_type = mdp_env_type
            self.env_config = mdp_env_config
            self.state_space = mdp_state_space
            self.action_space = mdp_action_space
        else:
            tl_mdp_env_type, tl_mdp_env_config, tl_mdp_state_space, tl_mdp_action_space, other = self.construct_tl_env(mdp_env_type,
                                                                                                                       mdp_env_config,
                                                                                                                       mdp_state_space,
                                                                                                                       mdp_action_space,
                                                                                                                       state_idx_map=other['state_idx_map']
            )

            self.env_type = tl_mdp_env_type
            self.env_config = tl_mdp_env_config
            self.state_space = tl_mdp_state_space
            self.action_space = tl_mdp_action_space

    def construct_mujoco_env(self):

        if 'reacher' in self.task_list:
            from examples.iclr2020_max_composition.env.mujoco_env.reacher_env import ReacherEnv
            env_type = ReacherEnv

            state_idx_map = {
                'rel_red_p': [10,12],
                'rel_green_p': [12, 14]
            }

            def get_state(all_info):
                mdp_state = np.concatenate([
                    np.cos(all_info['jp']),
                    np.sin(all_info['jp']),
                    all_info['jv'],
                    all_info['red_p'],
                    all_info['green_p'],
                    all_info['ee_p'] - all_info['red_p'],
                    all_info['ee_p'] - all_info['green_p']
                ])

                return mdp_state

            def get_reward(state=None, action=None, next_state=None, all_info={}):
                if 'gotored' in self.task_list:
                    r = -np.linalg.norm(all_info['ee_p'] - all_info['red_p']) - np.square(action).sum()
                elif 'gotogreen' in self.task_list:
                    r = -np.linalg.norm(all_info['ee_p'] - all_info['green_p']) - np.square(action).sum()
                else:
                    raise ValueError('reward not defined')

                return r

            def is_done(state=None, action=None, next_state=None, all_info={}):
                return False

            state_space = {'type': 'float', 'shape': (14, ), 'upper_bound': [], 'lower_bound': []}
            action_space = {'type': 'float', 'shape': (2, ), "upper_bound": np.ones(2), "lower_bound": -np.ones(2)}
            other = {'state_idx_map': state_idx_map}

            env_config = {
                # Common to all envs
                "seed": 10,
                'synchronous': self.Task_config.get('synchronous'),
                "debug": False,
                "state_space": state_space,
                "action_space": action_space,
                "get_state": get_state,
                "get_reward": get_reward,
                "is_done": is_done,
                # specific
                'headless': self.Task_config.get('headless'),
                'frameskip': self.Task_config.get('frameskip')
            }
        else:
            raise ValueError('env not supported')

        return env_type, env_config, state_space, action_space, other

    def construct_vrep_env(self):
        if ('gotohotdogplate' in self.task_list or 'gotogoal' in self.task_list):
            obs_dim = 9
            other = {}
        elif 'tl' in self.task_list and 'makehotdog' in self.task_list:
            obs_dim = 21
            state_idx_map = {
                'rel_hotdogplate_p': [3,6],
                'rel_grill_mapped_p': [6, 9],
                'rel_button_p': [9, 12],
                'rel_bunplate_p': [12, 15],
                'rel_condiment_p': [15, 18]
            }
            other = {'state_idx_map': state_idx_map}

        def get_state(all_info):
            if  'gotohotdogplate' in self.task_list:
                mdp_state = list(all_info['ee_target_pose']['pos']) + \
                            list(all_info['ee_target_pose']['pos'] - all_info['hotdogplate']['pos']) + \
                            list(all_info['ee_target_vel']['linear'])
            elif 'gotogoal' in self.task_list:
                mdp_state = list(all_info['ee_target_pose']['pos']) + \
                            list(all_info['ee_target_pose']['pos'] - all_info['goal']['pos']) + \
                            list(all_info['ee_target_vel']['linear'])
            elif 'makehotdog' in self.task_list:
                mdp_state = list(all_info['ee_target_pose']['pos']) + \
                            list(all_info['ee_target_pose']['pos'] - all_info['hotdogplate']['pos']) + \
                            list(all_info['ee_target_pose']['pos'] - all_info['grill_mapped']['pos']) + \
                            list(all_info['ee_target_pose']['pos'] - all_info['button_revolute_joint']['pos']) + \
                            list(all_info['ee_target_pose']['pos'] - all_info['bunplate']['pos']) + \
                            list(all_info['ee_target_pose']['pos'] - all_info['condiment_frame']['pos']) + \
                            list(all_info['ee_target_vel']['linear'])
            else:
                raise ValueError('task not supported')

            return np.array(mdp_state)

        def get_reward(state=None, action=None, next_state=None, all_info={}):

            if 'gotohotdogplate' in self.task_list or 'gotogoal' in self.task_list:
                sqrt_dist = np.linalg.norm(next_state[3:6])
                r = (-sqrt_dist - 0.05 * np.square(action).sum()) * 10
                # r = -sqrt_dist * 10
                # r = (1 - np.power((sqrt_dist)/0.01, 0.4))
                return r
            else:
                return 0


        def is_done(state=None, action=None, next_state=None, all_info={}):
            ee_target_pos = all_info['ee_target_pose']['pos']
            motion_bound = all_info['motion_bound']

            done = False
            if ee_target_pos[0] < motion_bound['x'][0] or ee_target_pos[0] > motion_bound['x'][1] or \
               ee_target_pos[1] < motion_bound['y'][0] or ee_target_pos[1] > motion_bound['y'][1] or \
               ee_target_pos[2] < motion_bound['z'][0] or ee_target_pos[2] > motion_bound['z'][1]:
                # print('out of motion range')
                done = False

            if 'gotogoal' in self.task_list:
                goal_pos = all_info['goal']['pos']
                sqrt_dist = np.linalg.norm(goal_pos - ee_target_pos)
                if sqrt_dist < 0.01:
                    print('reached goal')
                    done = True
                else:
                    done = False

            if 'gotohotdogplate' in self.task_list:
                goal_pos = all_info['hotdogplate']['pos']
                sqrt_dist = np.linalg.norm(goal_pos - ee_target_pos)
                if sqrt_dist < 0.01:
                    print('reached goal')
                    done = True
                else:
                    done = False

            return done

        def reset(all_info):
            quat = [-0.6095061302185059, -0.06530048698186874, -0.11404870450496674]
            sr = all_info['sample_region_bounds']
            nb_sample_regions = len(sr)

            ## randomly select a sample region
            idx = np.random.randint(0, nb_sample_regions)
            low = [sr[idx]['x'][0], sr[idx]['y'][0], sr[idx]['z'][0]]
            high = [sr[idx]['x'][1], sr[idx]['y'][1], sr[idx]['z'][1]]
            pos = np.random.uniform(low, high, len(low))

            new_ee_target_pose = np.array(list(pos) + quat)
            reset_dict = {"ee_target": new_ee_target_pose}

            if "gotohotdogplate" in self.task_list:
                hotdogplate_pose = list(all_info['hotdogplate']['pos']) + list(all_info['hotdogplate']['quat'])
                idx = 0
                low = [sr[idx]['x'][0], sr[idx]['y'][0], sr[idx]['z'][0]]
                high = [sr[idx]['x'][1], sr[idx]['y'][1], sr[idx]['z'][1]]
                pos = np.random.uniform(low, high, len(low))
                pos[2] = hotdogplate_pose[2]

                reset_dict['hotdogplate'] = list(pos) + hotdogplate_pose[3:]
                
            return reset_dict
            
        state_space = {'type': 'float', 'shape': (obs_dim, ), 'upper_bound': [], 'lower_bound': []}
        action_space = {'type': 'float', 'shape': (3, ), "upper_bound": 0.2 * np.ones(3), "lower_bound": -0.2 * np.ones(3)}

        if 'pyrep' in self.task_list:
            from examples.science_robotics.env.vrep_env.pyrep_hotdog_env import PyRepHotdogEnv
            env_type = PyRepHotdogEnv
        else:
            from examples.science_robotics.env.vrep_env.hotdog_env import HotdogEnv
            env_type = HotdogEnv

        env_config = {
            # Common to all envs
            "seed": 10,
            'synchronous': self.Task_config.get('synchronous'),
            "headless": self.Task_config.get('headless'),
            "debug": False,
            "state_space": state_space,
            "action_space": action_space,
            "get_state": get_state,
            "get_reward": get_reward,
            "is_done": is_done,
            "reset": reset,
            # specific to this env
            "suffix": "",
            "frameskip": self.Task_config.get('frameskip'),
            "scene_file": self.Task_config.get('scene_file'),
            "nb_remote_runners": self.Task_config.get('nb_remote_runners')
        }

        return env_type, env_config, state_space, action_space, other

    def construct_tl_env(self, mdp_env_type, mdp_env_config, mdp_state_space, mdp_action_space, state_idx_map):
        from rl_pipeline.env.fsa_augmented_env.fsa_augmented_env import FsaAugmentedEnv
        from lomap.classes.fsa import Fsa

        spec = self.get_tl_spec(state_idx_map=state_idx_map)

        if 'tl' in self.task_list and 'makehotdog' in self.task_list:
            #### F red && F green ####
            tl_env_type = FsaAugmentedEnv
            tl_env_config = {
                'fsa_save_dir': self.Task_config.get('fsa_save_dir'),
                'dot_file_name': self.Task_config.get('fsa_name'),
                'svg_file_name': self.Task_config.get('fsa_name'),
                'softmax': self.Task_config.get('softmax'),
                'beta': self.Task_config.get('beta'),
                'base_env': {
                    'type': mdp_env_type,
                    'config': mdp_env_config
                },
                'spec': {
                    'predicate_form': spec['predicate_form'],
                    'predicate_robustness': spec['predicate_robustness']
                },
                'visdom': False,
                'randomize_q': True,
                'q_scale': 0.1
            }

            fsa = Fsa()
            fsa.from_formula(spec['predicate_form'])
            fsa.add_trap_state()

            # type should really be mixed, this part is duplicated from that in fsa_augmented_mdp
            tl_state_space = {'type': 'float',
                              'shape': (mdp_state_space['shape'][0]+1, ),
                              'upper_bound': [len(fsa.g.nodes())] + list(mdp_state_space['upper_bound']),
                              'lower_bound': [0] + list(mdp_state_space['upper_bound'])}

            tl_action_space = mdp_env_config['action_space']

            return tl_env_type, tl_env_config, tl_state_space, tl_action_space, {}


    def get_tl_spec(self, state_idx_map={}):

        def in_region(current_pose=None, target_pose=None, rel_pose=None, dist_th=0.1):
            if rel_pose is None:
                rel_pos = np.array(current_pos_pose) - np.array(target_pose)
            sqrt_dist = np.linalg.norm(rel_pose)
            r_sqrt = dist_th - sqrt_dist
    
            
            # r_log = np.log(dist_th) - np.log(sqrt_dist)
            # th_precise = dist_th**2 - np.exp(-100*dist_th**2)
            # r_precise = th_precise - (sqrt_dist**2 - np.exp(-100*(sqrt_dist**2)))
            # r_shaped = 1. - np.power((sqrt_dist / dist_th),0.4)

            return r_sqrt, True

        #####################
        # Define predicates #
        #####################
        hotdogplate = {
            "predicate_name": "hotdogplate",
            'predicate_robustness': lambda s, a, sp: in_region(rel_pose=sp[state_idx_map['rel_hotdogplate_p'][0]:state_idx_map['rel_hotdogplate_p'][1]], dist_th=self.dist_th) # these are all mdp states without q pre-appended
        }

        grill_mapped = {
            "predicate_name": "grill_mapped",
            'predicate_robustness': lambda s, a, sp: in_region(rel_pose=sp[state_idx_map['rel_grill_mapped_p'][0]:state_idx_map['rel_grill_mapped_p'][1]], dist_th=self.dist_th) # these are all mdp states without q pre-appended
        }

        button = {
            "predicate_name": "button",
            'predicate_robustness': lambda s, a, sp: in_region(rel_pose=sp[state_idx_map['rel_button_p'][0]:state_idx_map['rel_button_p'][1]], dist_th=self.dist_th) # these are all mdp states without q pre-appended
        }

        bunplate = {
            "predicate_name": "bunplate",
            'predicate_robustness': lambda s, a, sp: in_region(rel_pose=sp[state_idx_map['rel_bunplate_p'][0]:state_idx_map['rel_bunplate_p'][1]], dist_th=self.dist_th) # these are all mdp states without q pre-appended
        }

        condiment = {
            "predicate_name": "condiment",
            'predicate_robustness': lambda s, a, sp: in_region(rel_pose=sp[state_idx_map['rel_condiment_p'][0]:state_idx_map['rel_condiment_p'][1]], dist_th=self.dist_th) # these are all mdp states without q pre-appended
        }

        
        
        predicate_form =  'F (hotdogplate && ' + \
                          'X F (grill_mapped && ' + \
                          'X F (button && ' + \
                          'X F (bunplate &&' + \
                          'X F (condiment' + \
                          ')))))'
        
        spec = {
            'predicate_form': predicate_form,
            'predicate_robustness': {
                'hotdogplate': hotdogplate['predicate_robustness'],
                'grill_mapped': grill_mapped['predicate_robustness'],
                'button': button['predicate_robustness'],
                'bunplate': bunplate['predicate_robustness'],
                'condiment': condiment['predicate_robustness']
            }
        }


        return spec


if __name__ == "__main__":
    config = {
        'synchronous': True,
        'task': 'mujoco_reacher_tl_1',
        'dist_th': 0.05,
        'fsa_save_dir': os.getcwd(),
        'fsa_name': 'fsa',
        'headless': False,
    }

    task = Task(config)

    env = task.Environment['type'](task.Environment['config'])
