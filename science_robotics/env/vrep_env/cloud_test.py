from examples.science_robotics.env.vrep_env.base.vrep_env_base import VrepEnvBase
import examples.science_robotics.env.vrep_env.base.vrep as vrep
import numpy as np
from rl_pipeline.configuration.configuration import Configuration
from future.utils import viewitems
import time
import os
import ctypes

class CloudTestEnv(VrepEnvBase):

    '''
    'port_num' determines which simulation this env is connected to
    'suffix' determines which robot this env is connected to
    'reset' is a class that should return a dict with new_joint_angles as an entry
    '''
    def __init__(self, config={}, port_num=19997, suffix="", reset=None, seed=None, logger=None):

        super(CloudTestEnv, self).__init__(config=config, port_num=port_num)

        rc, sim_time = vrep.simxGetFloatSignal(self.clientID, "sim_time", vrep.simx_opmode_streaming)

    def test(self):
        for _ in range(100):
            rc, sim_time = vrep.simxGetFloatSignal(self.clientID, "sim_time", vrep.simx_opmode_buffer)
            self.synchronous_trigger()
            print(sim_time)

if __name__ == "__main__":
    cls = CloudTestEnv()
    cls.test()