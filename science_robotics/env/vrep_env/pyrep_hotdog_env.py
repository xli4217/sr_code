import numpy as np
from rl_pipeline.configuration.configuration import Configuration
import examples.fm_rl_robot_cooking.utils.transformations as t

from future.utils import viewitems
import time
import os
import ctypes

from pyrep import PyRep
from pyrep.backend import sim


def quaternion_exp(q):
    '''
    this only supports unit quaternion
    '''
    u = q[:3]
    u_norm = np.linalg.norm(u)
    if u_norm < 0.001:
        return np.array([0,0,0,1])
    else:
        return np.concatenate([np.sin(u_norm) * (u / u_norm), np.array([ np.cos(u_norm)])])


default_config = {
    # Common to all envs
    "seed": 10,
    "synchronous": True,
    "debug": False,
    "state_space": None,
    "action_space": None,
    "get_state": None,
    "get_reward": None,
    "reset": None,
    "is_done": None,
    # specific to this env
    'scene_file': "",
    "suffix": "",
    "frameskip": 1,
    "headless": True,
    "nb_remote_runners": 0,
    "log_components":{
        'action_dist': False
    }
}

class PyRepHotdogEnv(object):

    '''
    'port_num' determines which simulation this env is connected to
    'suffix' determines which robot this env is connected to
    'reset' is a class that should return a dict with new_joint_angles as an entry
    '''
    def __init__(self, config={}, port_num=19997, suffix=None, reset=None, seed=None, logger=None):
        
        self.PyRepHotdogEnv_config = Configuration(default_config)
        self.PyRepHotdogEnv_config.update(config)

        self.name = 'pyrep_hotdog_env'
        self.port_num = port_num
        self.logger = logger
        self.synchronous = self.PyRepHotdogEnv_config.get('synchronous')
        
        self.suffix = suffix
        if not suffix:
            self.suffix = self.PyRepHotdogEnv_config.get('suffix')
        if self.suffix != "":
            self.suffix = "#" + self.suffix

        #### start vrep, pyrep ####
        self.pr = PyRep()
        self.pr.launch( self.PyRepHotdogEnv_config.get('scene_file'), self.PyRepHotdogEnv_config.get('headless'))
        self.pr.start()
        
        self.setup_handles()
        self.all_info = {
            'motion_bound': self.motion_bound,
            'sample_region_bounds': self.sample_region_bounds
        }

        #### initialize member variables ####
        self.action_dict = {'timestamp': 0, 'actions': None}
                
    def setup_handles(self):
        
        # #### world frame handle ####
        self.world_frame_handle = sim.simGetObjectHandle('Jaco_World_Frame'+self.suffix)
        
        #### ee_target handle ####
        self.ee_target_handle = sim.simGetObjectHandle('ee_target'+self.suffix)
  
        
        # #### object handles ####
        object_handle_names = [
            'goal'+self.suffix,
            'grill_mapped'+self.suffix,
            'toaster_joint_frame'+self.suffix,
            'button_revolute_joint'+self.suffix,
            'hotdogplate'+self.suffix,
            'bunplate'+self.suffix,
            'serveplate'+self.suffix,
            'condiment_frame'+self.suffix,
        ]
        self.object_handles = {}
        for oh in object_handle_names:
            h = sim.simGetObjectHandle(oh)
            self.object_handles[oh] = h
        
        # #### ee motion region ####
        self.motion_region_handle = sim.simGetObjectHandle("motion_region"+self.suffix)
        self.motion_bound = self.get_region_info(self.motion_region_handle)

        sample_region_names = ['sample_region0', 'sample_region1']
        self.sample_region_bounds = []
        for sn in sample_region_names:
            sample_region_handle = sim.simGetObjectHandle(sn+self.suffix)
            self.sample_region_bounds.append(self.get_region_info(sample_region_handle))

        
        
    def get_region_info(self, region_handle):
        handle = region_handle
        
        pos = sim.simGetObjectPosition(handle, self.world_frame_handle)

        bb_min_x = sim.simGetObjectFloatParameter(handle, 15)
        bb_min_y = sim.simGetObjectFloatParameter(handle, 16)
        bb_min_z = sim.simGetObjectFloatParameter( handle, 17)
        
        
        bb_max_x = sim.simGetObjectFloatParameter( handle, 18)
        bb_max_y = sim.simGetObjectFloatParameter( handle, 19)
        bb_max_z = sim.simGetObjectFloatParameter( handle, 20)

        bb = {
            'x': np.array([bb_min_x, bb_max_x]) + pos[0],
            'y': np.array([bb_min_y, bb_max_y]) + pos[1],
            'z': np.array([bb_min_z, bb_max_z]) + pos[2],
        }

        return bb
        

    def get_pose(self, handle):
        target_pos = sim.simGetObjectPosition(handle, self.world_frame_handle)
        target_quat = sim.simGetObjectQuaternion(handle, self.world_frame_handle)

        return np.array(target_pos), np.array(target_quat)

    def get_velocity(self, handle):
        linear_vel, angular_vel = sim.simGetObjectVelocity(handle)
        
        return np.array(linear_vel), np.array(angular_vel)
        
    def set_pose(self, pos, quat, handle):
        sim.simSetObjectPosition( handle, self.world_frame_handle, list(pos))
        sim.simSetObjectQuaternion( handle, self.world_frame_handle, list(quat))


    def update_all_info(self):
        # sim_time = sim.simGetFloatSignal( "sim_time", sim.sim_opmode_buffer)

        #t1 = time.time() 
        #### ee_target pose ####
        pos = sim.simGetObjectPosition( self.ee_target_handle, self.world_frame_handle)
        quat = sim.simGetObjectQuaternion( self.ee_target_handle, self.world_frame_handle)
        ee_target_pose = {'pos': np.array(pos), 'quat': np.array(quat)}
        
        #### ee_target vel ####
        linear_vel, angular_vel = self.get_velocity(self.ee_target_handle)
        ee_target_vel = {'linear': linear_vel, 'angular': angular_vel}
        
        
        #### retrieve object positions ####
        object_poses = self.get_object_pose()
        
        self.all_info = object_poses
        self.all_info['ee_target_pose'] = ee_target_pose
        self.all_info['ee_target_vel'] = ee_target_vel
        self.all_info['motion_bound'] = self.motion_bound
        self.all_info['sample_region_bounds'] = self.sample_region_bounds
        self.all_info['executed_action'] = self.action_dict

        #t2 = time.time() - t1
        #print('update info time:', t2)
        
    def get_info(self):
        return self.all_info

    def get_state(self, all_info):
        self.update_all_info()
        if self.PyRepHotdogEnv_config.get('get_state'):
            return self.PyRepHotdogEnv_config.get('get_state')(all_info)
        else:
            return np.array([0])


    def get_reward(self, state=None, action=None, next_state=None, all_info={}):
        if self.PyRepHotdogEnv_config.get('get_reward'):
            return self.PyRepHotdogEnv_config.get('get_reward')(state, action, next_state, all_info)
        else:
            return 0


    def is_done(self, state=None, action=None, next_state=None, all_info={}):
        if self.PyRepHotdogEnv_config.get('is_done'):
            return self.PyRepHotdogEnv_config.get('is_done')(state, action, next_state, all_info)
        else:
            return False

    
    def reset(self, **kwargs):
        self.update_all_info()
        #### reset ee_target ####
        self.ee_target_pose = np.array([0.161, -0.335, 0.132, 0.7818126082420349, -0.6095061302185059, -0.06530048698186874, -0.11404870450496674])
        
        if self.PyRepHotdogEnv_config.get('reset') is None:
            new_pose = self.ee_target_pose
            self.set_pose(pos=new_pose[:3], quat=new_pose[3:], handle=self.ee_target_handle)
        else:
            reset_dict = self.PyRepHotdogEnv_config.get('reset')(self.all_info)
            for k, v in viewitems(reset_dict):
                if k == 'ee_target':
                    reset_object_handle = self.ee_target_handle
                elif k == 'hotdogplate':
                    reset_object_handle = self.object_handles['hotdogplate'+self.suffix]
                else:
                    raise ValueError('reset object not supported')
                self.set_pose(pos=v[:3], quat=v[3:], handle=reset_object_handle)
                
        self.pr.step()

        self.update_all_info()
        
        return self.get_state(self.all_info)
        

    def get_object_pose(self):
        #### retrive object pose ####
        if self.object_handles is not None:
            object_pose = {}
            for obj_name, obj_handle in viewitems(self.object_handles):
                object_position = sim.simGetObjectPosition( obj_handle, self.world_frame_handle)
                object_quat = sim.simGetObjectQuaternion( obj_handle, self.world_frame_handle)

                if obj_name[-2] == "#":
                    object_pose[obj_name[:-2]] = {'pos': np.array(object_position), 'quat': np.array(object_quat)}
                else:
                    object_pose[obj_name] = {'pos': np.array(object_position), 'quat': np.array(object_quat)}
                    
            return object_pose
        else:
            return None

    def set_next_pose_from_velocity(self, linear_vel, angular_vel):

        # rc, ret_int, ret_float, ret_str, ret_buffer = sim.simCallScriptFunction( 'ee_target', sim.sim_scripttype_childscript, 'set_linear_velocity', [], list(linear_vel), [], bytearray(), sim.sim_opmode_oneshot)
        
        
        # rc, dt = sim.simGetFloatSignal( "sim_time_step", sim.sim_opmode_buffer)
        dt = 0.05
        # #### linear ####
        curr_pos = self.all_info['ee_target_pose']['pos']
        next_pos = curr_pos + dt * linear_vel
        # self.ee_target_pose[:3] += np.array(linear_vel) * 0.05
        
        # #### angular ####
        curr_quat = self.all_info['ee_target_pose']['quat']
        next_quat = curr_quat
        next_quat = t.quaternion_multiply(quaternion_exp((dt / 2) * angular_vel), curr_quat)
        sim.simSetObjectPosition( self.ee_target_handle, self.world_frame_handle, list(next_pos))
        sim.simSetObjectQuaternion( self.ee_target_handle, self.world_frame_handle, list(next_quat))
        # time.sleep(0.05)
        
        return next_pos, next_quat
        
    def step(self, actions, axis=0):
        '''
        6 dim action space controls the linear and angular velocities of ee_target
        '''

        #print("min:", np.min(actions))
        #print("max:", np.max(actions))
        
        # t1 = time.time()
        assert isinstance(actions, np.ndarray)
        if actions.ndim == 2:
            actions = actions.flatten()

        actions = actions * np.array(self.action_space['upper_bound'])
    
        # clip actions to limits
        if self.PyRepHotdogEnv_config.get('action_space') is not None:
            clipped_actions = np.clip(np.array(actions), self.action_space['lower_bound'], self.action_space['upper_bound'])
        else:
            clipped_actions = actions

        self.action_dict['actions'] = clipped_actions
        for _ in range(self.PyRepHotdogEnv_config.get('frameskip')):
            next_pos, next_quat = self.set_next_pose_from_velocity(linear_vel=clipped_actions[:3], angular_vel=clipped_actions[3:])
            self.pr.step()
        # t2 = time.time() - t1
        
                

    @property
    def state_space(self):
        return self.PyRepHotdogEnv_config.get('state_space')

    @property
    def action_space(self):
        return self.PyRepHotdogEnv_config.get('action_space')

    def teleop(self, cmd):
        if cmd == 'w':
            action = np.array([0.,0.,0.1])
        elif cmd == 's':
            action = np.array([0.,0., -0.1])
        elif cmd == 'a':
            action = np.array([0.1,0., 0.])
        elif cmd == 'd':
            action = np.array([-0.1, 0., 0.])
        elif cmd == 'q':
            action = np.array([0., 0.1, 0.])
        elif cmd == 'e':
            action = np.array([0., -0.1, 0.])
            
        self.step(action)
            
    def stop(self):
        self.pr.stop()
        
    def pause(self):
        pass
    
    def close(self):
        self.pr.stop()
        self.pr.shutdown()
        
    def set_seed(self, seed):
        pass
            
if __name__ == "__main__":
    config = default_config
    config['action_space'] = {'type': 'float', 'shape': (6, ), 'upper_bound': 10 * np.ones(6), 'lower_bound': -10 * np.ones(6)}
    config['scene_file'] = os.path.join(os.environ['RLFPS_PATH'], 'examples', 'science_robotics', 'env', 'vrep_env', 'ttt', 'particle_test_env.ttt')
    config['headless'] = False
    cls = PyRepHotdogEnv(config=config)

    t1 = time.time()
    for _ in range(1):
        cls.reset()
        for _ in range(70):
            # actions = np.random.uniform(low=-1., high=1., size=3)
            actions = np.array([0, 0., 0., 0, 0, 100])
            cls.step(actions)
            cls.update_all_info()
            # print(cls.get_info()['ee_target_pose'])
    t2 = time.time() - t1
    print("!!!!!!!!!!")
    print(t2)
    cls.close()