import time

try:
    import examples.science_robotics.env.vrep_env.base.vrep as vrep
except:
    print ('--------------------------------------------------------------')
    print ('"vrep.py" could not be imported. This means very probably that')
    print ('either "vrep.py" or the remoteApi library could not be found.')
    print ('Make sure both are in the same folder as this file,')
    print ('or appropriately adjust the file "vrep.py"')
    print ('--------------------------------------------------------------')
    print ('')

default_config = {}
    
class VrepEnvBase(object):
    # Defines an rllab environment for the V-Rep scene ur5_sim_test.ttt
    def __init__(self, config={}, port_num=19997):
        self.VrepEnvBase_config = default_config
        self.VrepEnvBase_config.update(config)

        
        print ('Program started')
        vrep.simxFinish(-1)  # just in case, close all opened connections
        self.clientID = vrep.simxStart('127.0.0.1', port_num, True,
                                       True, 5000, 5)  # Connect to V-REP

        print("port_num:",port_num)
        print("client id:", self.clientID)
        print("----")
        
        if self.clientID != -1:
            # read http://www.forum.coppeliarobotics.com/viewtopic.php?t=2361
            print ('Connected to remote API server')
            
            if self.VrepEnvBase_config.get('synchronous'):
                print("starting in synchronous mode")
                vrep.simxSynchronous(self.clientID, True)
            else:
                print("starting in asynchronous mode")
            
            rc = vrep.simxStartSimulation(
                self.clientID, vrep.simx_opmode_blocking)

            e = vrep.simxGetInMessageInfo(self.clientID, vrep.simx_headeroffset_server_state)
            print('e:',e)
            
            if rc == 0:
                print('simulation started')
            
                
            # Now try to retrieve data in a blocking fashion (i.e. a service
            # call):
            rc, objs = vrep.simxGetObjects(
                self.clientID, vrep.sim_handle_all, vrep.simx_opmode_oneshot)

            if rc == vrep.simx_return_ok:
                print ('Number of objects in the scene: ', len(objs))
            else:
                print ('Remote API function call returned with error code: ', rc)
            print("connected through port number: {}".format(port_num))

            if self.VrepEnvBase_config.get('synchronous'):
                self.synchronous_trigger()
           
           
    def set_iteration_counter(self):
        # used to connect multiple clients in synchronous mode http://www.coppeliarobotics.com/helpFiles/en/remoteApiModusOperandi.htm
        return_code, iteration = vrep.simxGetIntegerSignal(self.clientID, "iteration", vrep.simx_opmode_streaming)
        
    def stop_iteration_counter(self):
        vrep.simxGetIntegerSignal(self.clientID, "iteration", vrep.simx_opmode_discontinue)
        
        
    def synchronous_trigger(self):
        # return_code, iteration1 = vrep.simxGetIntegerSignal(self.clientID, 'iteration', vrep.simx_opmode_buffer)

        
        # if return_code != vrep.simx_return_ok:
        #     iteration1 = -1

        vrep.simxSynchronousTrigger(self.clientID)
       
        # # # print(vrep.simxGetPingTime(self.clientID))
        # iteration2 = iteration1
        # while iteration2 == iteration1: # wait until the iteration counter has changed
        #     return_code, iteration2 = vrep.simxGetIntegerSignal(self.clientID, 'iteration', vrep.simx_opmode_buffer)
        #     if return_code != vrep.simx_return_ok:
        #         iteration2 = -1

    def close(self):
        vrep.simxStopSimulation(self.clientID, vrep.simx_opmode_oneshot)