from examples.science_robotics.env.vrep_env.base.vrep_env_base import VrepEnvBase
import examples.science_robotics.env.vrep_env.base.vrep as vrep
import numpy as np
from rl_pipeline.configuration.configuration import Configuration
from future.utils import viewitems
import time
import os
import ctypes

default_config = {
    # Common to all envs
    "seed": 10,
    "synchronous": True,
    "debug": False,
    "state_space": None,
    "action_space": None,
    "get_state": None,
    "get_reward": None,
    "is_done": None,
    # specific to this env
    "suffix": "",
    "frameskip": 1,
}

class HotdogEnv(VrepEnvBase):

    '''
    'port_num' determines which simulation this env is connected to
    'suffix' determines which robot this env is connected to
    'reset' is a class that should return a dict with new_joint_angles as an entry
    '''
    def __init__(self, config={}, port_num=None, suffix=None, reset=None, seed=None, logger=None):

        if config['synchronous'] and port_num is None:
            port_num = 19999 
        
        super(HotdogEnv, self).__init__(config=config, port_num=port_num)

        self.HotdogEnv_config = Configuration(default_config)
        self.HotdogEnv_config.update(config)

        self.name = 'hotdog_env'
        self.port_num = port_num
        self.logger = logger
        self.synchronous = self.HotdogEnv_config.get('synchronous')
        

        self.suffix = suffix
        if not suffix:
            self.suffix = self.HotdogEnv_config.get('suffix')
        if self.suffix != "":
            self.suffix = "#" + self.suffix
            
        self.setup_handles()
        self.all_info = {
            'motion_bound': self.motion_bound,
            'sample_region_bounds': self.sample_region_bounds
        }

        #### last thing to stream ####
        self.set_iteration_counter()

        #### ensure all streamed data arrive ####
        self.synchronous_trigger()
        vrep.simxGetPingTime(self.clientID)

        #### initialize member variables ####
        self.action_dict = {'timestamp': 0, 'actions': None}
        
        
    def setup_handles(self):
        #### simulation timestep ####
        vrep.simxGetFloatSignal(self.clientID, "sim_time_step", vrep.simx_opmode_streaming)

        #### simulation time ####
        vrep.simxGetFloatSignal(self.clientID, "sim_time", vrep.simx_opmode_streaming)
        
        # #### world frame handle ####
        rc, self.world_frame_handle = vrep.simxGetObjectHandle(self.clientID, 'Jaco_World_Frame'+self.suffix, vrep.simx_opmode_blocking)
        
        #### ee_target handle ####
        rc, self.ee_target_handle = vrep.simxGetObjectHandle(self.clientID, 'ee_target'+self.suffix, vrep.simx_opmode_blocking)
        vrep.simxGetObjectPosition(self.clientID, self.ee_target_handle, self.world_frame_handle, vrep.simx_opmode_streaming)
        vrep.simxGetObjectQuaternion(self.clientID, self.ee_target_handle, self.world_frame_handle, vrep.simx_opmode_streaming)
        vrep.simxGetObjectVelocity(self.clientID, self.ee_target_handle, vrep.simx_opmode_streaming)

        
        # #### object handles ####
        object_handle_names = [
            'goal'+self.suffix,
            'grill_mapped'+self.suffix,
            'toaster_joint_frame'+self.suffix,
            'button_revolute_joint'+self.suffix,
            'hotdogplate'+self.suffix,
            'bunplate'+self.suffix,
            'serveplate'+self.suffix,
            'condiment_frame'+self.suffix,
        ]
        self.object_handles = {}
        for oh in object_handle_names:
            _, h = vrep.simxGetObjectHandle(self.clientID, oh, vrep.simx_opmode_blocking)
            self.object_handles[oh] = h
            vrep.simxGetObjectPosition(self.clientID, h, self.world_frame_handle, vrep.simx_opmode_streaming)
            vrep.simxGetObjectQuaternion(self.clientID, h, self.world_frame_handle, vrep.simx_opmode_streaming)
        
        # #### ee motion region ####
        rc, self.motion_region_handle = vrep.simxGetObjectHandle(self.clientID, "motion_region"+self.suffix, vrep.simx_opmode_blocking)
        self.motion_bound = self.get_region_info(self.motion_region_handle)


        #### ee sample region ####
        sample_region_names = ['sample_region0', 'sample_region1']
        self.sample_region_bounds = []
        for sn in sample_region_names:
            rc, sample_region_handle = vrep.simxGetObjectHandle(self.clientID, sn+self.suffix, vrep.simx_opmode_blocking)
            self.sample_region_bounds.append(self.get_region_info(sample_region_handle))
            
    def get_region_info(self, region_handle):
        handle = region_handle
        
        _, pos = vrep.simxGetObjectPosition(self.clientID, handle, self.world_frame_handle, vrep.simx_opmode_blocking)

        _, bb_min_x = vrep.simxGetObjectFloatParameter(self.clientID, handle, 15, vrep.simx_opmode_blocking)
        _, bb_min_y = vrep.simxGetObjectFloatParameter(self.clientID, handle, 16, vrep.simx_opmode_blocking)
        _, bb_min_z = vrep.simxGetObjectFloatParameter(self.clientID, handle, 17, vrep.simx_opmode_blocking)
        
        
        _, bb_max_x = vrep.simxGetObjectFloatParameter(self.clientID, handle, 18, vrep.simx_opmode_blocking)
        _, bb_max_y = vrep.simxGetObjectFloatParameter(self.clientID, handle, 19, vrep.simx_opmode_blocking)
        _, bb_max_z = vrep.simxGetObjectFloatParameter(self.clientID, handle, 20, vrep.simx_opmode_blocking)

        bb = {
            'x': np.array([bb_min_x, bb_max_x]) + pos[0],
            'y': np.array([bb_min_y, bb_max_y]) + pos[1],
            'z': np.array([bb_min_z, bb_max_z]) + pos[2],
        }

        return bb
        

    def get_pose(self, handle):
        rct = 1
        target_pos = np.zeros(3)
        while rct != 0 or np.linalg.norm(target_pos) < 0.001:    
            rct, target_pos = vrep.simxGetObjectPosition(self.clientID, handle, self.world_frame_handle, vrep.simx_opmode_buffer)
            rcq, target_quat = vrep.simxGetObjectQuaternion(self.clientID, handle, self.world_frame_handle, vrep.simx_opmode_buffer)

        return np.array(target_pos), np.array(target_quat)

    def get_velocity(self, handle):
        rct, linear_vel, angular_vel = vrep.simxGetObjectVelocity(self.clientID, handle,  vrep.simx_opmode_buffer)
        
        return np.array(linear_vel), np.array(angular_vel)
        
    def set_pose(self, pos, quat, handle):
        vrep.simxSetObjectPosition(self.clientID, handle, self.world_frame_handle, pos, vrep.simx_opmode_oneshot)
        vrep.simxSetObjectQuaternion(self.clientID, handle, self.world_frame_handle, quat, vrep.simx_opmode_oneshot)


    def update_all_info(self):
        rc, sim_time = vrep.simxGetFloatSignal(self.clientID, "sim_time", vrep.simx_opmode_buffer)
        
        #### ee_target pose ####
        _, pos = vrep.simxGetObjectPosition(self.clientID, self.ee_target_handle, self.world_frame_handle, vrep.simx_opmode_buffer)
        _, quat = vrep.simxGetObjectQuaternion(self.clientID, self.ee_target_handle, self.world_frame_handle, vrep.simx_opmode_buffer)
        # ee_target_pose = {'pos': self.ee_target_pose[:3], 'quat': self.ee_target_pose[3:]}
        ee_target_pose = {'pos': np.array(pos), 'quat': np.array(quat)}
        
        #### ee_target vel ####
        linear_vel, angular_vel = self.get_velocity(self.ee_target_handle)
        ee_target_vel = {'linear': linear_vel, 'angular': angular_vel}
        
        
        #### retrieve object positions ####
        object_poses = self.get_object_pose()
        
        self.all_info = object_poses
        self.all_info['ee_target_pose'] = ee_target_pose
        self.all_info['ee_target_vel'] = ee_target_vel
        self.all_info['motion_bound'] = self.motion_bound
        self.all_info['sample_region_bounds'] = self.sample_region_bounds
        self.all_info['executed_action'] = self.action_dict
        self.all_info['info_update_time'] = sim_time

    def get_info(self):
        return self.all_info

    def get_state(self, all_info):
        self.update_all_info()
        if self.HotdogEnv_config.get('get_state'):
            return self.HotdogEnv_config.get('get_state')(all_info)
        else:
            return np.array([0])


    def get_reward(self, state=None, action=None, next_state=None, all_info={}):
        if self.HotdogEnv_config.get('get_reward'):
            return self.HotdogEnv_config.get('get_reward')(state, action, next_state, all_info)
        else:
            return 0


    def is_done(self, state=None, action=None, next_state=None, all_info={}):
        if self.HotdogEnv_config.get('is_done'):
            return self.HotdogEnv_config.get('is_done')(state, action, next_state, all_info)
        else:
            return False

    
    def reset(self, **kwargs):
        #### clear signals ####
        vrep.simxClearIntegerSignal(self.clientID, "", vrep.simx_opmode_oneshot)
        vrep.simxClearFloatSignal(self.clientID, "", vrep.simx_opmode_oneshot)
        vrep.simxClearStringSignal(self.clientID, "", vrep.simx_opmode_oneshot)

        self.update_all_info()
        #### reset ee_target ####
        self.ee_target_pose = np.array([0.161, -0.335, 0.132, 0.7818126082420349, -0.6095061302185059, -0.06530048698186874, -0.11404870450496674])

        if self.HotdogEnv_config.get('reset') is None:
            new_pose = self.ee_target_pose
            self.set_pose(pos=new_pose[:3], quat=new_pose[3:], handle=self.ee_target_handle)
        else:
            reset_dict = self.HotdogEnv_config.get('reset')(self.all_info)
            for k, v in viewitems(reset_dict):
                if k == 'ee_target':
                    reset_object_handle = self.ee_target_handle
                elif k == 'hotdogplate':
                    reset_object_handle = self.object_handles['hotdogplate'+self.suffix]
                else:
                    raise ValueError('reset object not supported')
                self.set_pose(pos=v[:3], quat=v[3:], handle=reset_object_handle)
                
        
        if self.synchronous:
            self.synchronous_trigger()
            vrep.simxGetPingTime(self.clientID)
            
        self.update_all_info()
        
        return self.get_state(self.all_info)
        

    def get_object_pose(self):
        #### retrive object pose ####
        if self.object_handles is not None:
            object_pose = {}
            for obj_name, obj_handle in viewitems(self.object_handles):
                rc = 1
                while rc != 0:
                    rc, object_position = vrep.simxGetObjectPosition(self.clientID, obj_handle, self.world_frame_handle, vrep.simx_opmode_buffer)
                    rc, object_quat = vrep.simxGetObjectQuaternion(self.clientID, obj_handle, self.world_frame_handle, vrep.simx_opmode_buffer)
                    e = vrep.simxGetInMessageInfo(self.clientID, vrep.simx_headeroffset_server_state)

                if obj_name[-2] == "#":
                    object_pose[obj_name[:-2]] = {'pos': np.array(object_position), 'quat': np.array(object_quat)}
                else:
                    object_pose[obj_name] = {'pos': np.array(object_position), 'quat': np.array(object_quat)}
                    
            return object_pose
        else:
            return None

    def set_next_pose_from_velocity(self, linear_vel, angular_vel):

        # rc, ret_int, ret_float, ret_str, ret_buffer = vrep.simxCallScriptFunction(self.clientID, 'ee_target', vrep.sim_scripttype_childscript, 'set_linear_velocity', [], list(linear_vel), [], bytearray(), vrep.simx_opmode_oneshot)
        
        
        # rc, dt = vrep.simxGetFloatSignal(self.clientID, "sim_time_step", vrep.simx_opmode_buffer)
        dt = 0.05
        # #### linear ####
        curr_pos = self.all_info['ee_target_pose']['pos']
        next_pos = curr_pos + dt * linear_vel
        # self.ee_target_pose[:3] += np.array(linear_vel) * 0.05
        
        # #### angular ####
        # curr_quat = self.all_info['ee_target_pose']['quat']
        # next_quat = curr_quat
        next_quat = None
        
        vrep.simxSetObjectPosition(self.clientID, self.ee_target_handle, self.world_frame_handle, next_pos, vrep.simx_opmode_oneshot)
        # vrep.simxSetObjectQuaternion(self.clientID, self.ee_target_handle, self.world_frame_handle, next_quat, vrep.simx_opmode_oneshot)
        # time.sleep(0.05)
        
        return next_pos, next_quat
        
    def step(self, actions, axis=0):
        '''
        6 dim action space controls the linear and angular velocities of ee_target
        '''

        assert isinstance(actions, np.ndarray)
        if actions.ndim == 2:
            actions = actions.flatten()

        actions = actions * np.array(self.action_space['upper_bound'])
    
        # clip actions to limits
        if self.HotdogEnv_config.get('action_space') is not None:
            clipped_actions = np.clip(np.array(actions), self.action_space['lower_bound'], self.action_space['upper_bound'])
        else:
            clipped_actions = actions

     
        for _ in range(self.HotdogEnv_config.get('frameskip')):
            next_pos, next_quat = self.set_next_pose_from_velocity(linear_vel=clipped_actions[:3], angular_vel=clipped_actions[3:])

            # print('calculated:', next_pos)
            if self.synchronous:
                rc, sim_time = vrep.simxGetFloatSignal(self.clientID, "sim_time", vrep.simx_opmode_buffer)
                self.action_dict = {'timestamp': sim_time, 'actions': clipped_actions}

                self.synchronous_trigger()
                # make sure to get the freshest sensor readings 
                vrep.simxGetPingTime(self.clientID)
                self.update_all_info()
                #print('sensed:', self.all_info['ee_target_pose']['pos'])
                #print("-----------")
            else:
                raise ValueError('asynchronous mode not supported')
                
                

    @property
    def state_space(self):
        return self.HotdogEnv_config.get('state_space')

    @property
    def action_space(self):
        return self.HotdogEnv_config.get('action_space')

    def teleop(self, cmd):
        if cmd == 'w':
            action = np.array([0.,0.,1])
        elif cmd == 's':
            action = np.array([0.,0., -1])
        elif cmd == 'a':
            action = np.array([1,0., 0.])
        elif cmd == 'd':
            action = np.array([-1, 0., 0.])
        elif cmd == 'q':
            action = np.array([0., 1, 0.])
        elif cmd == 'e':
            action = np.array([0., -1, 0.])
        else:
            ation = np.array([0,0,0])
            
        self.step(action)
            
    def stop(self):
        vrep.simxStopSimulation(self.clientID, vrep.simx_opmode_oneshot)

    def pause(self):
        vrep.simxPauseSimulation(self.clientID, vrep.simx_opmode_oneshot)
    
    def close(self):
        vrep.simxGetObjectPosition(self.clientID, self.ee_target_handle, self.world_frame_handle, vrep.simx_opmode_discontinue)
        vrep.simxGetObjectQuaternion(self.clientID, self.ee_target_handle, self.world_frame_handle, vrep.simx_opmode_discontinue)
        vrep.simxGetObjectVelocity(self.clientID, self.ee_target_handle, vrep.simx_opmode_discontinue)

        for hn, h in viewitems(self.object_handles):
            vrep.simxGetObjectPosition(self.clientID, h, self.world_frame_handle, vrep.simx_opmode_discontinue)
            vrep.simxGetObjectQuaternion(self.clientID, h, self.world_frame_handle, vrep.simx_opmode_discontinue)

    def set_seed(self, seed):
        pass
            
if __name__ == "__main__":
    config = default_config
    cls = HotdogEnv(config=config)
    
    cls.reset()
    while True:
        actions = np.random.uniform(low=-1., high=1., size=6)
        # actions = np.array([0, 0.1, 0., 0., 0, 0])
        cls.step(actions)
        # cls.update_all_info()
        # print(cls.get_info()['ee_target_pose'])
       